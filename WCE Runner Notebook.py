# Databricks notebook source
# MAGIC %md 
# MAGIC # Weighting Controls Evaluation Software (Runner Notebook)
# MAGIC 
# MAGIC >This notebook is executed as the master or caller notebook of the Weighting Control Evaluation (WCE) software. The notebook calls five other individual modules that have to be executed sequentially. The caller notebook feeds the necessary parameters into those modules so the user no longer needs to go through running them manually. However, it is possible to run the modules manually for the validation or custom analysis purpose.  
# MAGIC 
# MAGIC **PURPOSE:**
# MAGIC >* To discover reportable qualified weighting controls, using tables from the media data lake, for a supplied date range and selected DMAs in LPM plus PPM or set meter plus PPM markets.
# MAGIC 
# MAGIC **What Notebook Does in a Nutshell:**
# MAGIC >* The notebook feeds the required parameters into **1_Person_Weight_Computation** Databricks notebook and retrieve the output which is a b1010 parquet file with person initial weights.
# MAGIC 
# MAGIC >* The notebook feeds the required parameters into **2_Effective_Sample_Size_Computation** Databricks notebook and retrieves the output which is a parquet file including the effective sample size of all potential weighting controls (except geography) per DMA, minorities/complements within a DMA and demos within a DMA. 
# MAGIC 
# MAGIC >* The notebook feeds the required parameters into **3_Sufficient_Sample_Size_Qualifier_Engine** Databricks notebook and retrieves the output which a parquet file including all qualified controls per DMA, minorities/complements within a DMA and demos within a DMA.
# MAGIC 
# MAGIC >* The notebook feeds the required parameters into **4_UE_Computation** Databricks notebook and retrieves the output which is a parquet file with all qualified controls (including geography) as well as UE per DMA, minorities/complements within a DMA and demos within a DMA.
# MAGIC 
# MAGIC >* The notebook feeds the required parameters into **5_d15_File_Creation** Databricks notebook and retrieves the output which is a clean reportable d15 file , written as a parquet file on S3. The last module also creates a table of all weighting controls whose UEs were not retrieved from the MDL.
# MAGIC 
# MAGIC **USER INPUTS:** 
# MAGIC > 1. **start_date**
# MAGIC >  - The beginning of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC > 2. **end_date**
# MAGIC >  - The ending of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC > 3. **sample_type** 
# MAGIC >  - Market type of the analysis, provided as a string, the acceptable market types are 'SET_METER+PPM' or 'LPM+PPM'. The software cannot be executed for LPM plus PPM and set meter plus ppm markets simultaneously.
# MAGIC > 4. **DMA codes**
# MAGIC >  - this should be a Python string of dmas, e.g. '501,602,703', '501'.
# MAGIC >  - lmp plus ppm market codes are '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC >  - set meter plus ppm market codes are '515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839'
# MAGIC > 5. **please_export**
# MAGIC >  - Provided as a string, set this varialbe to 'True' if you want to save the data in the Optional Overrides sections and 'False' otherwise.
# MAGIC > 6. **ue_table_measurement_period**
# MAGIC >  - The period for which UEs are pulled up from the MDL, provided as a string, in the following format **monthyy**
# MAGIC >  - The software only pulls up ues with the maximum load id.
# MAGIC >  - examples are 'Feb18' or 'May18'
# MAGIC > 7. **missing_ue_output_dir**
# MAGIC >  - The directory in which the software saves a Parquet file in which all weighting controls whose UEs were missing in the MDL table are communicated. 
# MAGIC > 8. **geography_file_directory**
# MAGIC >  - The directory of the geography group assignment file, with a csv format, provided as a string. 
# MAGIC > 9. **d15_file_output_dir**
# MAGIC >  - The directory in which the software saves the output as a parquet format.
# MAGIC 
# MAGIC **THE GEOGRAPHY FILE FORMAT**
# MAGIC > | column                    | data_type | example                            |
# MAGIC   |---------------------------|-----------|------------------------------------|
# MAGIC   | county_group              | bigint    | 1                                  |
# MAGIC   | dma_code                  | bigint    | 501                                |
# MAGIC   | control                   | string    | GEOGRAPHY or GEOGRAPHY_BLACK_RACE  |
# MAGIC   | county_code               | bigint    | 31059                              |
# MAGIC   
# MAGIC **THE OUTPUT FORMAT:**
# MAGIC > | col_name                   | data_type | ex_results                         |
# MAGIC   |----------------------------|-----------|------------------------------------|
# MAGIC   | DMA_ID                     | bigint    | 501                                |
# MAGIC   | WEIGHT_TYPE                | string    | PS WGT                             |
# MAGIC   | START_DATE                 | date      | 2018-03-01                         |
# MAGIC   | END_DATE                   | date      | 3000-12-31                         |
# MAGIC   | SEQ_ORDER                  | bigint    | 1                                  |
# MAGIC   | HH_PERSON                  | bigint    | H/P                                |
# MAGIC   | CHAR_SHORT_NAME            | string    | AGE_OF_HOUSEHOLDER_45-54           |
# MAGIC   | RIM                        | string    | AGE_OF_HEAD                        |
# MAGIC   | UE                         | int       | 1486270                            |
# MAGIC   | SSCM_KEY                   | string    | 38                                 |
# MAGIC  
# MAGIC 
# MAGIC **DATA SOURCES:**
# MAGIC > | schema               | table                                                        |
# MAGIC   |----------------------|--------------------------------------------------------------|
# MAGIC   | dsci_tam             | universe_estimates                                           |
# MAGIC   | dsci_tam             | household_characteristics                                    |
# MAGIC   | dsci_tam             | person_characteristics                                       |             
# MAGIC   | --------             | a CSV file including county groups assignment                |       
# MAGIC   
# MAGIC **AN EXAMPLE RUN:**
# MAGIC > <pre>
# MAGIC start_date                    = '2018-03-01'
# MAGIC end_date                      = '2018-03-07'
# MAGIC effective_date                = '2018-03-20'
# MAGIC sample_type                   = 'LPM+PPM'
# MAGIC dma_codes                     = '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC ue_table_measurement_period   = 'May18' 
# MAGIC geography_file_dir            = '/FileStore/tables/historical_geography_assignment2.csv'
# MAGIC please_export                 = 'True'
# MAGIC d15_file_output_dir           = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/d15_file.parquet'
# MAGIC missing_ue_output_dir         = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/controls_with_no_ues.parquet'
# MAGIC </pre>
# MAGIC 
# MAGIC **AN EXAMPLE OUTPUT:**
# MAGIC >* EXAMPLE OF D15 FILE
# MAGIC >> <pre>
# MAGIC <table class="confluenceTable">
# MAGIC <tbody>
# MAGIC <tr>
# MAGIC <td class="confluenceTd">DMA_ID</td>
# MAGIC <td class="confluenceTd">WEIGHT_TYPE</td>
# MAGIC <td class="confluenceTd">START_DATE</td>
# MAGIC <td class="confluenceTd">END_DATE</td>
# MAGIC <td class="confluenceTd">SEQ_ORDER</td>
# MAGIC <td class="confluenceTd">HH_PERSON</td>
# MAGIC <td class="confluenceTd">CHAR_SHORT_NAME</td>
# MAGIC <td class="confluenceTd">RIM</td>
# MAGIC <td class="confluenceTd">UE</td>
# MAGIC <td class="confluenceTd">SSCM_KEY</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd">501</td>
# MAGIC <td class="confluenceTd">PS WGT</td>
# MAGIC <td class="confluenceTd">3/1/2018</td>
# MAGIC <td class="confluenceTd">12/31/3000</td>
# MAGIC <td class="confluenceTd">1</td>
# MAGIC <td class="confluenceTd">H</td>
# MAGIC <td class="confluenceTd">ADS_YES</td>
# MAGIC <td class="confluenceTd">ADS</td>
# MAGIC <td class="confluenceTd">632560</td>
# MAGIC <td class="confluenceTd">38</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd">501</td>
# MAGIC <td class="confluenceTd">PS WGT</td>
# MAGIC <td class="confluenceTd">3/1/2018</td>
# MAGIC <td class="confluenceTd">12/31/3000</td>
# MAGIC <td class="confluenceTd">2</td>
# MAGIC <td class="confluenceTd">H</td>
# MAGIC <td class="confluenceTd">ADS_NO</td>
# MAGIC <td class="confluenceTd">ADS</td>
# MAGIC <td class="confluenceTd">6442190</td>
# MAGIC <td class="confluenceTd">38</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd">501</td>
# MAGIC <td class="confluenceTd">PS WGT</td>
# MAGIC <td class="confluenceTd">3/1/2018</td>
# MAGIC <td class="confluenceTd">12/31/3000</td>
# MAGIC <td class="confluenceTd">3</td>
# MAGIC <td class="confluenceTd">H</td>
# MAGIC <td class="confluenceTd">ADS_YES</td>
# MAGIC <td class="confluenceTd">ADS_HISPANIC</td>
# MAGIC <td class="confluenceTd">126000</td>
# MAGIC <td class="confluenceTd">38</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd">501</td>
# MAGIC <td class="confluenceTd">PS WGT</td>
# MAGIC <td class="confluenceTd">3/1/2018</td>
# MAGIC <td class="confluenceTd">12/31/3000</td>
# MAGIC <td class="confluenceTd">4</td>
# MAGIC <td class="confluenceTd">H</td>
# MAGIC <td class="confluenceTd">ADS_NO</td>
# MAGIC <td class="confluenceTd">ADS_HISPANIC</td>
# MAGIC <td class="confluenceTd">1286190</td>
# MAGIC <td class="confluenceTd">38</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd">501</td>
# MAGIC <td class="confluenceTd">PS WGT</td>
# MAGIC <td class="confluenceTd">3/1/2018</td>
# MAGIC <td class="confluenceTd">12/31/3000</td>
# MAGIC <td class="confluenceTd">5</td>
# MAGIC <td class="confluenceTd">H</td>
# MAGIC <td class="confluenceTd">ADS_YES</td>
# MAGIC <td class="confluenceTd">ADS_NON-HISPANIC</td>
# MAGIC <td class="confluenceTd">506560</td>
# MAGIC <td class="confluenceTd">38</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd">501</td>
# MAGIC <td class="confluenceTd">PS WGT</td>
# MAGIC <td class="confluenceTd">3/1/2018</td>
# MAGIC <td class="confluenceTd">12/31/3000</td>
# MAGIC <td class="confluenceTd">6</td>
# MAGIC <td class="confluenceTd">H</td>
# MAGIC <td class="confluenceTd">ADS_NO</td>
# MAGIC <td class="confluenceTd">ADS_NON-HISPANIC</td>
# MAGIC <td class="confluenceTd">5156000</td>
# MAGIC <td class="confluenceTd">38</td>
# MAGIC </tr>
# MAGIC </tbody>
# MAGIC </table>
# MAGIC </pre>
# MAGIC 
# MAGIC >* EXAMPLE OF CONTROLS WITH MISSING UEs
# MAGIC 
# MAGIC >> | dma | weighting_control      | level                      | universe_estimate |
# MAGIC    |-----|------------------------|----------------------------|-------------------|
# MAGIC    | 506 |	LANGUAGE	          | ENGLISH_DOMINANT	       | null              |
# MAGIC    | 506 |	LANGUAGE	          | SPANISH_DOMINANT_BILINGUAL | null              |
# MAGIC    | 533 |	LANGUAGE	          | ENGLISH_DOMINANT	       | null              |
# MAGIC    | 533 |	LANGUAGE	          | SPANISH_DOMINANT_BILINGUAL | null              |
# MAGIC    | 548 |	LANGUAGE	          | ENGLISH_DOMINANT	       | null              |
# MAGIC    | 548 |	LANGUAGE	          | SPANISH_DOMINANT	       | null              |
# MAGIC 
# MAGIC **CODE DEVELOPERS:**
# MAGIC >* MJ PAKDEL
# MAGIC >* JONATHAN OUEGNIN

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Inputs
# MAGIC 
# MAGIC - - - -
# MAGIC 
# MAGIC * Jump to Section:
# MAGIC   * [Module 1 - Person Weight Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1942844)
# MAGIC   * [Module 2 - Effective Sample Size Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943057)
# MAGIC   * [Module 3 - Sufficient Sample Size Qualifier Engine](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943058)
# MAGIC   * [Module 4 - UE Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943059)
# MAGIC   * [Module 5 - D15 File Creation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943060)

# COMMAND ----------

start_date                    = '2018-03-07'
end_date                      = '2018-03-14'
effective_date                = '2018-03-20'
sample_type                   = 'LPM+PPM' # SET_METER+PPM or LPM+PPM
dma_codes                     = '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751' # LPM+PPM
# dma_codes                     = '515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839' # SET_METER+PPM
please_export                 = 'True'
ue_table_measurement_period   = 'May18' 
# geography_file_dir            = '/FileStore/tables/historical_geography_assignment2.csv'
geography_file_dir            = "s3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/historical_geography_assignment3_nongeo.csv"
missing_ue_output_dir         = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/controls_with_no_ues_lpm_ppm_08102018.parquet'
d15_file_output_dir           = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/d15_file_lpm_ppm_08242018.parquet'

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Module 1 - Person Weight Computation
# MAGIC 
# MAGIC - - - -
# MAGIC 
# MAGIC * Jump to Section:
# MAGIC   * [Inputs](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943032)
# MAGIC   * [Module 2 - Effective Sample Size Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943057)
# MAGIC   * [Module 3 - Sufficient Sample Size Qualifier Engine](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943058)
# MAGIC   * [Module 4 - UE Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943059)
# MAGIC   * [Module 5 - D15 File Creation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943060)

# COMMAND ----------

results_notebook1 = dbutils.notebook.run('1_Person_Weight_Computation', 25000,
                     {'start_date': start_date,
                      'end_date': end_date,
                      'dma_codes': dma_codes,
                      'ue_table_measurement_period': ue_table_measurement_period,
                      'sample_type': sample_type,
                      'please_export': please_export})

print(results_notebook1)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Module 2 - Effective Sample Size Computation
# MAGIC 
# MAGIC - - - -
# MAGIC 
# MAGIC * Jump to Section:
# MAGIC   * [Inputs](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943032)
# MAGIC   * [Module 1 - Person Weight Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1942844)
# MAGIC   * [Module 3 - Sufficient Sample Size Qualifier Engine](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943058)
# MAGIC   * [Module 4 - UE Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943059)
# MAGIC   * [Module 5 - D15 File Creation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943060)

# COMMAND ----------

results_notebook2 = dbutils.notebook.run('2_Effective_Sample_Size_Computation', 25000,
                     {'start_date': start_date,
                      'end_date': end_date,
                      'sample_type': sample_type,
                      'dma_codes': dma_codes,
                      'ue_table_measurement_period': ue_table_measurement_period
                      })

print(results_notebook2)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Module 3 - Sufficient Sample Size Qualifier Engine
# MAGIC 
# MAGIC - - - -
# MAGIC 
# MAGIC * Jump to Section:
# MAGIC   * [Inputs](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943032)
# MAGIC   * [Module 1 - Person Weight Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1942844)
# MAGIC   * [Module 2 - Effective Sample Size Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943057)
# MAGIC   * [Module 3 - Sufficient Sample Size Qualifier Engine](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943058)
# MAGIC   * [Module 4 - UE Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943059)
# MAGIC   * [Module 5 - D15 File Creation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943060)

# COMMAND ----------

results_notebook3 = dbutils.notebook.run('3_Sufficient_Sample_Size_Qualifier_Engine', 25000,
                     {'start_date': start_date,
                      'end_date': end_date,
                      'sample_type': sample_type})

print(results_notebook3)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Module 4 - UE Computation
# MAGIC 
# MAGIC - - - -
# MAGIC 
# MAGIC * Jump to Section:
# MAGIC   * [Inputs](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943032)
# MAGIC   * [Module 1 - Person Weight Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1942844)
# MAGIC   * [Module 2 - Effective Sample Size Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943057)
# MAGIC   * [Module 3 - Sufficient Sample Size Qualifier Engine](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943058)
# MAGIC   * [Module 5 - D15 File Creation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943060)

# COMMAND ----------

results_notebook4 = dbutils.notebook.run('4_UE_Computation', 25000,
                     {'start_date': start_date,
                      'end_date': end_date,
                      'ue_table_measurement_period': ue_table_measurement_period,
                      'geography_file_dir': geography_file_dir,
                      'missing_ue_output_dir': missing_ue_output_dir,
                      'sample_type': sample_type})

print(results_notebook4)

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Module 5 - D15 File Creation
# MAGIC 
# MAGIC - - - -
# MAGIC 
# MAGIC * Jump to Section:
# MAGIC   * [Inputs](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943032)
# MAGIC   * [Module 1 - Person Weight Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1942844)
# MAGIC   * [Module 2 - Effective Sample Size Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943057)
# MAGIC   * [Module 3 - Sufficient Sample Size Qualifier Engine](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943058)
# MAGIC   * [Module 4 - UE Computation](https://nielsen-prod.cloud.databricks.com/#notebook/1631653/command/1943059)

# COMMAND ----------

results_notebook5 = dbutils.notebook.run('5_D15_File_Creation', 25000,
                     {'start_date': start_date,
                      'end_date': end_date,
                      'effective_date': effective_date,
                      'sample_type': sample_type,
                      'ue_table_measurement_period': ue_table_measurement_period,
                      'd15_file_output_dir': d15_file_output_dir})

print(results_notebook5)

# COMMAND ----------

wc = spark.read.parquet('s3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/d15_file_lpm_ppm_08242018.parquet')

# COMMAND ----------

display(wc.orderBy('WEIGHT_TYPE','DMA_ID','HH_PERSON','SEQ_ORDER'))
