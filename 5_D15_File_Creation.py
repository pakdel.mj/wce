# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # D15 FILE CREATION
# MAGIC ### Module # 5 
# MAGIC 
# MAGIC #### Purpose:
# MAGIC 
# MAGIC > * This module creates the final D15 file (post stratification and design weights) used by Data Science.
# MAGIC 
# MAGIC **USER INPUTS:**      
# MAGIC 
# MAGIC > 1. start_date - Start Date: **YYYY-mm-dd** 
# MAGIC > 2. end_date - End Date: **YYYY-mm-dd**
# MAGIC > 3. effective_date - Effective date for the Weighting Controls: **YYYY-mm-dd**
# MAGIC > 4. ue_table_measurement_period - UE table measurement period variable, as abbreviated month follwed by year: **monthyy**
# MAGIC > 5. d15_file_output_dir - Directory in which the weighting controls will be saved as a parquet file. String
# MAGIC 
# MAGIC **LOW-LEVEL EXPLANATION OF WHAT NOTEBOOK DOES:**
# MAGIC > This software can be broken into 3 parts.
# MAGIC 
# MAGIC > **Part 1: Create Design Weight File**
# MAGIC >>* Select DMA codes depending on sample types (LPM+PPM or SMM+PPM).
# MAGIC >>* Pulls data from the **dsci_tam.universe_estimate** for PPM Metro 1, PPM Metro 2, PPM Remainder market_break_description to create the design weights dataframe. 
# MAGIC >>* Save the Design Weighting Controls to s3 in the **d15_file_output_dir** directory. 
# MAGIC 
# MAGIC > **Part 2: Create Post Stratification Weight Dataframe**
# MAGIC 
# MAGIC >>* Import the weighting controls (output of the UE Computation Module).   
# MAGIC >>* Adding columns needed for the final D15 file:
# MAGIC     * WEIGHT TYPE 
# MAGIC     * START DATE
# MAGIC     * END DATE
# MAGIC     * HH/PERSON
# MAGIC     * SSCM KEY
# MAGIC >>* Order the **Rim** and **Char Short Name** columns in the order provided in the software specifications. 
# MAGIC >>>  Rim
# MAGIC       Origin
# MAGIC        Language
# MAGIC        Black
# MAGIC        Asian
# MAGIC        American Indian/American Native 
# MAGIC        Cable Plus ADS/Broadcast Only 
# MAGIC        Cable
# MAGIC        Digital Cable
# MAGIC        DVR
# MAGIC        Number of Sets
# MAGIC        ADS
# MAGIC        AOH
# MAGIC        Presence of Non-Adult
# MAGIC        HH Size
# MAGIC        Geography
# MAGIC        Hispanic Cable Plus ADS/Broadcast Only
# MAGIC        Non-Hispanic ADS Plus Broadcast Only
# MAGIC        Hispanic ADS
# MAGIC        Non-Hispanic ADS
# MAGIC        Hispanic AOH
# MAGIC        Non-Hispanic AOH
# MAGIC        Hispanic HH Size 
# MAGIC        Non-Hispanic HH Size 
# MAGIC        Hispanic Geography
# MAGIC        Non-Hispanic Geography
# MAGIC        Black Cable Plus ADS/Broadcast Only 
# MAGIC        Non-Black Cable Plus ADS/Broadcast Only 
# MAGIC        Black Cable
# MAGIC        Non-Black Cable
# MAGIC        Black ADS
# MAGIC        Non-Black ADS
# MAGIC        Black AOH
# MAGIC        Non-Black AOH
# MAGIC        Black HH Size 
# MAGIC        Non-Black HH Size
# MAGIC        Black Geography
# MAGIC        Non-Black Geography
# MAGIC        Persons
# MAGIC        Hispanic Persons
# MAGIC        Non-Hispanic Persons
# MAGIC        Black Persons
# MAGIC        Non-Black Persons
# MAGIC >>>  Char Short Name  
# MAGIC       Origin - Hispanic/Non-Hispanic
# MAGIC        Language - Non-Hispanic/Spanish Only/Mostly Spanish/Spanish Dominant/Spanish Dominant Bilingual/Spanish English Equal/English Dominant Bilingual/English Dominant/Mostly English/Only English
# MAGIC        Race - 1=Black/Non-Black, 2=Asian/Non-Asian, 3=AIAN/Non-AIAN 							
# MAGIC        Cable - 1= Cable+ADS/Broadcast, 2= No/Yes	
# MAGIC        ADS - Yes/No							
# MAGIC        AOH - <25/25-34/<35/35+/35-44/45-54/35-54/<55/55-64/55+/65+ 
# MAGIC        Presence of Non Adults - None/Only 0-11/Any 12-17/Any 0-17
# MAGIC        HH Size - 1/2/1-2/3/3+/4/3-4/5/5+/6+ 	
# MAGIC        DVR - Yes/No 	
# MAGIC        Measurable Sets - 1/2/2+/3/4+						
# MAGIC        Persons - C2-5/C6-11/C2-11/M12-17/F12-17/P2-17/T12-17/M18-24/M25-34/M18-34/M35-49/M50-54/M55-64/M50-64/M18+/M50+/M65+/F18-24/F25-34/F18-34/F35-49/F50-54/F55-64/F50-64/F18+/F50+/F65+
# MAGIC        
# MAGIC        
# MAGIC > **Part 3: Rename Rim and Char Short Name and save output**
# MAGIC >>* Import the 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/wce_names_mapping.csv' file created for this project to rename char short names.<br/ >
# MAGIC     The columns **sub_rim_wce** and **rim_dsci** matches the names in the weighting controls dataframe.<br/ >
# MAGIC     The columns **charshortname_appdev** and **rim_appdev** conatin the proper names needed for the D15.<br/ ><br/ >
# MAGIC     Example of rows in the 'wce_names_mapping.csv' file:
# MAGIC 
# MAGIC >> | type | sub_rim_wce | rim_dsci                | charshortname_appdev | rim_appdev                           |
# MAGIC    |------|-------------|-------------------------|----------------------|--------------------------------------|
# MAGIC    | H    |	ADS_YES     | ADS                     | ADS                  | ADS                                  |
# MAGIC    | H    | ADS_NO	    | ADS                     | ADS=N	             | ADS                                  |
# MAGIC    | H    |	ADS_YES	    | ADS_BLACK_RACE	      | RACE=BLK ADS	     | ADS WITHIN BLACK/NON-BLACK           |	
# MAGIC    | H    | ADS_NO	    | ADS_BLACK_RACE	      | RACE=BLK ADS=N       | ADS WITHIN BLACK/NON-BLACK           |	
# MAGIC    | H    |	ADS_YES	    | ADS_NON-BLACK_RACE      | RACE=NOBLK ADS       | ADS WITHIN BLACK/NON-BLACK           |
# MAGIC    | H    |	ADS_NO	    | ADS_NON-BLACK_RACE      | RACE=NOBLK ADS=N     | ADS WITHIN BLACK/NON-BLACK           |
# MAGIC    | P    | F55_64	    | PERSONS_HISPANIC	      | RACE=NOBLK F18-34    | PERSONS WITHIN HISPANIC/NON-HISPANIC |	
# MAGIC    | P    |	F18_34	    | PERSONS_NON-BLACK_RACE  | RACE=NOBLK ADS       | PERSONS WITHIN BLACK/NON-BLACK       |
# MAGIC <br/ >
# MAGIC >>* Join the 'wce_names_mapping.csv' to the Post stratification Weighting Controls data, select and rename appropiate columns.
# MAGIC >>* Save the Post Startification Weighting Controls to s3 in the **final_output_module5_ps** directory.
# MAGIC 
# MAGIC >>* **EXAMPLE D15 FILE**
# MAGIC 
# MAGIC >>* POST STRATIFICATION WEIGHTS  
# MAGIC 
# MAGIC >> | DMA ID | WEIGHT TYPE |	START DATE | END DATE   | SEQ ORDER | HH/PERSON | CHAR SHORT NAME | RIM                                  | SSCM KEY | UE      |
# MAGIC    |--------|-------------|------------|------------|-----------|-----------|-----------------|--------------------------------------|----------|---------|
# MAGIC    | 501    | PS WGT      |	2018-03-20 | 3000-12-31 | 1         | H         | HISPANIC        | ORIGIN                               | 38       | 1412190 |
# MAGIC    | 501    | PS WGT      |	2018-03-20 | 3000-12-31 | 2         | H         | NON-HISP        | ORIGIN                               | 38       | 5662560 |
# MAGIC    | 501    | PS WGT      |	2018-03-20 | 3000-12-31 | 35        | P         | HISPANIC M12-17 | PERSONS WITHIN HISPANIC/NON-HISPANIC | 38       | 201774  |
# MAGIC    | 501    | PS WGT      |	2018-03-20 | 3000-12-31 | 36        | P         | HISPANIC F12-17 | PERSONS WITHIN HISPANIC/NON-HISPANIC | 38       | 192294  |
# MAGIC    <br /> 
# MAGIC                                  
# MAGIC >>* DESIGN WEIGHTS   
# MAGIC 
# MAGIC >> | DMA ID | WEIGHT TYPE |	START DATE | END DATE   | SEQ ORDER | HH/PERSON | CHAR SHORT NAME | RIM           | SSCM KEY | UE      |
# MAGIC    |--------|-------------|------------|------------|-----------|-----------|-----------------|-------------- |----------|---------|
# MAGIC    | 501    | DESIGN      |	2018-03-20 | 3000-12-31 | 1         | H         | PPM METRO 1     | PPM GEOGRAPHY | 38       | 6238790 |
# MAGIC    | 501    | DESIGN      |	2018-03-20 | 3000-12-31 | 2         | H         | PPM REMAINDER   | PPM GEOGRAPHY | 38       | 835960  |
# MAGIC                              
# MAGIC                                  
# MAGIC >> <br />  
# MAGIC 
# MAGIC **Authors:**    
# MAGIC - MJ PAKDEL
# MAGIC - JONATHAN OUEGNIN

# COMMAND ----------

# MAGIC %md
# MAGIC ### Input

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################

# start_date = '2018-03-07'
# end_date = '2018-03-14'
# effective_date = '2018-03-20'
# sample_type = 'SET_METER+PPM'
# ue_table_measurement_period = 'May18'
# d15_file_output_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/d15.parquet'

# COMMAND ----------

##########################################################
#####    USER INPUTS (called in another notebook)    #####
##########################################################

start_date = str(dbutils.widgets.get('start_date'))
end_date = str(dbutils.widgets.get('end_date'))
effective_date = str(dbutils.widgets.get('effective_date'))
sample_type = str(dbutils.widgets.get('sample_type'))
ue_table_measurement_period = str(dbutils.widgets.get('ue_table_measurement_period'))
d15_file_output_dir = str(dbutils.widgets.get('d15_file_output_dir')) 

# COMMAND ----------

# MAGIC %md
# MAGIC ### Import Libraries and Functions

# COMMAND ----------

from pyspark.sql.functions import lit, when, row_number
from pyspark.sql.types import StructField, StringType, IntegerType, StructType
from pyspark.sql.window import Window
import pandas as pd
import numpy as np
import re
import datetime

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Validate Sample Type User Input

# COMMAND ----------

sample_type = sample_type.upper()

if (sample_type != "LPM+PPM") and (sample_type != "SET_METER+PPM"):
    dbutils.notebook.exit("Invalid Sample Type Input")

# COMMAND ----------

###############################################################################
#####    Create paths that will be used for input of the software
###############################################################################

# Directory path of module 4 dataframe result 
final_output_module4_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/weighting_controls_' + start_date.replace('-','_') + '_' + end_date.replace('-','_') + '_' + sample_type.replace('+','_') + '.parquet'

# COMMAND ----------

# MAGIC %md
# MAGIC ### Read Output of UE Module

# COMMAND ----------

wc_input = spark.read.parquet(final_output_module4_dir)
wc_input.createOrReplaceTempView('wc_input')
# wc_input.cache()
wc_input_pd = wc_input.toPandas()
sscm_key = 78 if sample_type == "SET_METER+PPM" else 3
# display(wc_input.orderBy(wc_input.dma, wc_input.weighting_control))

# COMMAND ----------

# MAGIC %md 
# MAGIC # Part 1: Create Design Weight File

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create the filters based on the sample type

# COMMAND ----------

########################################################
#####    Create a list with DMAs (LPM or SMM sample)
########################################################

dma_list = sorted(wc_input_pd.dma.unique())
dma_list = [ int(x) for x in dma_list ]

if sample_type == 'LPM+PPM':
    lpm_dma_codes = [501, 602, 820, 511, 539, 623, 517, 528, 819, 618, 512, 803, 753, 807, 508, 504, 506, 524, 613, 505, 609, 510, 862, 534, 751] # lpm market codes
    dma_codes = list(set(lpm_dma_codes).intersection(dma_list)) # Keep needed DMA
elif sample_type == 'SET_METER+PPM':
    sm_dma_codes = [515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839] # set meter market codes
    dma_codes = list(set(sm_dma_codes).intersection(dma_list))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create Design weighting controls

# COMMAND ----------

###############################################################################################
#####    Pulls PPM Metro 1, 2, and Remainder and UE from the dsci_tam.universe_estimates table
#####    Create Design weighting controls
###############################################################################################

sql_design_wgt = """
SELECT 
  geography_code AS DMA_ID,
  'DESIGN WGT' AS WEIGHT_TYPE,
  '{effective_date}' AS START_DATE,
  '3000-12-31' AS END_DATE,
  'X' AS SEQ_ORDER,
  'H' AS HH_PERSON,
  CASE
    WHEN market_break = 'PPM Metro 1' THEN 'PPM METRO 1'
    WHEN market_break = 'PPM Metro 2' THEN 'PPM METRO 2'
    WHEN market_break = 'PPM Non-Metro' THEN 'PPM REMAINDER'
  END AS CHAR_SHORT_NAME,
  'PPM GEOGRAPHY' AS RIM,
  {sscm_key} AS SSCM_KEY,
  universe_estimate AS UE,
  load_id
FROM 
    dsci_tam.universe_estimates 
WHERE 1=1 
  AND measurement_period = '{period}' 
  AND (market_break = 'PPM Metro 1' OR market_break = 'PPM Metro 2' OR market_break = 'PPM Non-Metro') 
  AND market_break_description = 'TV Household'
  AND geography_code IN ({dmas})
  AND geography = 'DMA'
""".format(period=ue_table_measurement_period, dmas=', '.join(str(x) for x in dma_codes), effective_date=effective_date, sscm_key=sscm_key)

wc_design_wgt = spark.sql(sql_design_wgt)

# Filter the result to the MAX load_id present in the table, which represents the latest data available
max_load_id_ds = max([x.load_id for x in wc_design_wgt.select('load_id').distinct().collect()])
wc_design_wgt = wc_design_wgt.filter(wc_design_wgt.load_id == max_load_id_ds).drop('load_id')

wc_design_wgt = wc_design_wgt.withColumn("SEQ_ORDER", row_number().over(Window.partitionBy("DMA_ID").orderBy("DMA_ID", "CHAR_SHORT_NAME"))).orderBy("DMA_ID", "CHAR_SHORT_NAME")
wc_design_pd = wc_design_wgt.toPandas()
# wc_design_wgt.write.parquet(final_output_module5_design, mode='overwrite')

# COMMAND ----------

# MAGIC %md 
# MAGIC # Part 2: Create Post Stratification Weight Dataframe

# COMMAND ----------

# MAGIC %md
# MAGIC ### Add Required Columns to Dataframe

# COMMAND ----------

################################################################################################
#####    This transform the data into a pandas dataframe, and adds columns to the input file:
#####    DMA column, Weight Type column, Date columns, Household/Person column, SSCM Key
################################################################################################

wc_input_pd['dma'] = wc_input_pd['dma'].astype(int)
wc_input_pd["WEIGHT_TYPE"] = "PS WGT"
wc_input_pd["START_DATE"] = effective_date 
wc_input_pd["END_DATE"] = "3000-12-31" 
wc_input_pd["HH_PERSON"] = "H"
wc_input_pd.loc[wc_input_pd.weighting_control.str.contains("PERSONS"),"HH_PERSON"] = "P"
wc_input_pd["SSCM_KEY"] = sscm_key

# COMMAND ----------

# MAGIC %md
# MAGIC ### Function to order Geography levels

# COMMAND ----------

#####################################################################################################
#####    This function orders the geography controls of the geography assignment file in
#####    in numerical order.
#####################################################################################################

def order_geo_levels(dma=501):
    """This function will create a sorted list of the geography weighting controls level per DMA."""
    
    # filter data to Geography contols only
    geo = wc_input_pd.loc[(wc_input_pd.dma == dma) & \
                          ((wc_input_pd.weighting_control == 'GEOGRAPHY') | \
                          (wc_input_pd.weighting_control == 'GEOGRAPHY WITHIN HISPANIC/NON-HISPANIC') | \
                          (wc_input_pd.weighting_control == 'GEOGRAPHY WITHIN BLACK/NON-BLACK'))\
                          ,['dma', 'weighting_control', 'level']]
    
    # create sorted list of geography contols using the lambda function to get the CG number
    get_cg_num = lambda x: re.search(u'\d{1,2}', str(x)).group()
    geo['cg_number'] = geo['level'].map(get_cg_num).astype(int)
    
    # create sorted list of geography contols using the lambda function to get the CG minority Complement (e.g, Non-Black vs. Black) or total DMA
    get_cd_min_yn = lambda x: re.search(u'([^\s]+)', str(x)).group()
    geo['cd_min_yn'] = geo['level'].map(get_cd_min_yn)
    
    def get_minority(x):
        if x == 'GEOGRAPHY':
            return('DMA')
        elif x == 'GEOGRAPHY WITHIN BLACK/NON-BLACK':
            return('BLACK')
        elif x == 'GEOGRAPHY WITHIN HISPANIC/NON-HISPANIC':
            return('HISPANIC')
          
    geo['cg_minority'] = geo['weighting_control'].map(get_minority)
    geo = geo.sort_values(['cg_minority','cd_min_yn','cg_number'])
    
    geo_level_list = geo['level'].tolist()
    
    return(geo_level_list)
 
  
def add_leading_zero(level):
    if 'CG ' in level:
        for county in re.findall(u'\d{4,5}', level):
            if len(county) < 5:
                level = level.replace(county, '0' + county)
        if len(re.search(u'\d{1,2}', level).group(0)) == 1:
            level = re.sub(r'(\d{1,2})', '0' + r'\1', level, count=1)        
        return(level)  
    else:
        return(level)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Created ordered lists for controls and levels

# COMMAND ----------

#####################################################################################################
#####    Creates ordered list for RIM and Level for both Household and Persons weighting controls
#####################################################################################################

order_controls = ['PPM GEOGRAPHY','PPM_METRO_CODE', 'ORIGIN', 'LANGUAGE', 'BLACK', 'ASIAN', 'AIAN', 'CABLE_PLUS', 'CABLE', 'ADS', 'DVR', 'MEASURABLE_SETS', 'AGE_OF_HEAD', 'PRESENCE_OF_NONADULTS', 'HOUSEHOLD_SIZE',\
                  'GEOGRAPHY', 'PERSONS', 'CABLE_PLUS_HISPANIC', 'CABLE_PLUS_NON-HISPANIC', 'ADS_HISPANIC', 'ADS_NON-HISPANIC', 'AGE_OF_HEAD_HISPANIC', 'AGE_OF_HEAD_NON-HISPANIC',\
                  'HOUSEHOLD_SIZE_HISPANIC', 'HOUSEHOLD_SIZE_NON-HISPANIC', 'GEOGRAPHY WITHIN HISPANIC/NON-HISPANIC', 'PERSONS_HISPANIC', 'PERSONS_NON-HISPANIC','CABLE_PLUS_BLACK_RACE',\
                  'CABLE_PLUS_NON-BLACK_RACE', 'ADS_BLACK_RACE', 'ADS_NON-BLACK_RACE', 'AGE_OF_HEAD_BLACK_RACE', 'AGE_OF_HEAD_NON-BLACK_RACE', 'HOUSEHOLD_SIZE_BLACK_RACE',\
                  'HOUSEHOLD_SIZE_NON-BLACK_RACE', 'GEOGRAPHY WITHIN BLACK/NON-BLACK', 'PERSONS_BLACK_RACE', 'PERSONS_NON-BLACK_RACE']

order_levels_h = ['HISPANIC', 'NON-HISPANIC', 'SPANISH_ONLY', 'MOSTLY_SPANISH', 'SPANISH_DOMINANT', 'SPANISH_DOMINANT_BILINGUAL', 'SPANISH_ENGLISH_EQUAL', 'ENGLISH_DOMINANT_BILINGUAL', 'ENGLISH_DOMINANT',\
                'MOSTLY_ENGLISH', 'ENGLISH_ONLY', 'BLACK_RACE', 'NON-BLACK_RACE', 'ASIAN_RACE', 'NON-ASIAN_RACE', 'AIAN_RACE', 'NON-AIAN_RACE', 'CABLE_PLUS_YES', 'BROADCAST_ONLY', 'CABLE_NO', 'CABLE_YES',\
                'ADS_YES', 'ADS_NO', 'AGE_OF_HOUSEHOLDER_<25', 'AGE_OF_HOUSEHOLDER_25-34', 'AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35+', 'AGE_OF_HOUSEHOLDER_35-44', 'AGE_OF_HOUSEHOLDER_45-54',\
                'AGE_OF_HOUSEHOLDER_35-54', 'AGE_OF_HOUSEHOLDER_<55', 'AGE_OF_HOUSEHOLDER_55-64', 'AGE_OF_HOUSEHOLDER_55+', 'AGE_OF_HOUSEHOLDER_65+', 'PRESENCE_OF_NONADULTS_NONE_<18',\
                'PRESENCE_OF_NONADULTS_ONLY_<12', 'PRESENCE_OF_NONADULTS_ANY_12-17', 'PRESENCE_OF_NONADULTS_ANY_CHILD', '1_PERSON', '2_PERSONS', '1-2_PERSONS', '3_PERSONS', '3+_PERSONS', '4_PERSONS',\
                '3-4_PERSONS', '5_PERSONS', '5+_PERSONS', '6+_PERSONS', 'DVR_YES', 'DVR_NO', '1_SET', '2_SETS', '2+_SETS', '3_SETS', '4+_SETS', 'PPM METRO 1', 'PPM METRO 2', 'PPM REMAINDER']

order_levels_p = ['C2_5', 'C6_11', 'C2_11', 'M12_17', 'F12_17', 'P2_17', 'P12_17', 'M18_24', 'M25_34', 'M18_34', 'M35_49', 'M18_49', 'M50_54', 'M55_64', 'M50_64', 'M18_999', 'M50_999', 'M65_999', 'F18_24',\
                  'F25_34', 'F18_34', 'F35_49', 'F18_49', 'F50_54', 'F55_64', 'F50_64', 'F18_999', 'F50_999', 'F65_999']

# COMMAND ----------

# MAGIC %md
# MAGIC ### Loop over each DMA to create weighting controls

# COMMAND ----------

#####################################################################################################
#####    Create and order a Household and Person dataframe per DMA 
#####    Join both Household and Persons dataframes per DMA 
#####    Union every DMA dataframe
#####################################################################################################

wc_final_ps = pd.DataFrame()

"""This loop creates a sorted HH weighting controls dataset and a sorted Person weighting controls dataset per dma. 
The sequence order gets added and both datasets are then concatenated."""

for dma in dma_list:
  
    wc_ppm_h = wc_design_pd.loc[wc_design_pd.DMA_ID == dma,:]
    wc_ppm_h['WEIGHT_TYPE'] = "PS WGT"
    wc_input_pd_h = wc_input_pd.loc[(wc_input_pd.HH_PERSON == "H") & (wc_input_pd.dma == dma),:]
    wc_ppm_h = wc_ppm_h[['DMA_ID', 'RIM', 'CHAR_SHORT_NAME', 'UE', 'WEIGHT_TYPE', 'START_DATE', 'END_DATE', 'HH_PERSON', 'SSCM_KEY']]
    wc_ppm_h.columns = wc_input_pd_h.columns
    wc_input_pd_h = wc_ppm_h.append(wc_input_pd_h)
    wc_input_pd_h.loc[:,'weighting_control'] = pd.Categorical(wc_input_pd_h['weighting_control'], order_controls)
    wc_input_pd_h.loc[:,'level'] = pd.Categorical(wc_input_pd_h['level'], order_levels_h + order_geo_levels(dma=dma))
    wc_input_pd_h = wc_input_pd_h.sort_values(['dma', 'weighting_control', 'level']).reset_index(drop = True)
    wc_input_pd_h['SEQ_ORDER'] = wc_input_pd_h.index + 1
    wc_input_pd_h['level'] = wc_input_pd_h['level'].apply(add_leading_zero)

    wc_input_pd_p = wc_input_pd.loc[(wc_input_pd["HH_PERSON"] == "P") & (wc_input_pd.dma == dma),:]
    wc_input_pd_p.loc[:,'weighting_control'] = pd.Categorical(wc_input_pd_p['weighting_control'], order_controls)
    wc_input_pd_p.loc[:,'level'] = pd.Categorical(wc_input_pd_p['level'], order_levels_p)
    wc_input_pd_p = wc_input_pd_p.sort_values(['dma', 'weighting_control','level']).reset_index(drop = True)
    wc_input_pd_p['SEQ_ORDER'] = wc_input_pd_p.index + 1
    wc_input_pd_p['level'] = wc_input_pd_p['level'].apply(add_leading_zero)
    
    wc_input_dma = wc_input_pd_h.append(wc_input_pd_p)

    wc_final_ps = wc_final_ps.append(wc_input_dma)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Convert Pandas Dataframe to PySpark Dataframe

# COMMAND ----------

########################################################
#####    Convert Pandas dataframe to PySpark dataframe 
########################################################

df_schema = StructType([StructField('dma', IntegerType(), True),
                        StructField('weighting_control', StringType(), True),
                        StructField('level', StringType(), True),
                        StructField('universe_estimate', IntegerType(), True),
                        StructField('WEIGHT_TYPE', StringType(), True),
                        StructField('START_DATE', StringType(), True),
                        StructField('END_DATE', StringType(), True),
                        StructField('HH_PERSON', StringType(), True),
                        StructField('SSCM_KEY', IntegerType(), True),
                        StructField('SEQ_ORDER', IntegerType(), True)])

final_output_ps = sqlContext.createDataFrame(wc_final_ps, schema=df_schema)
final_output_ps = final_output_ps.select("dma", "WEIGHT_TYPE", "START_DATE", "END_DATE", "SEQ_ORDER","HH_PERSON", "level",\
                                         "weighting_control", "SSCM_KEY", "universe_estimate").withColumnRenamed("dma", "DMA_ID")\
                                         .withColumnRenamed("level", "CHAR_SHORT_NAME").withColumnRenamed("weighting_control", "RIM")\
                                         .withColumnRenamed("universe_estimate", "UE")
    
# display(final_output_ps)

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 3: Rename Rim and Char Short Name and save output

# COMMAND ----------

# MAGIC %md
# MAGIC ### Load renaming table from s3

# COMMAND ----------

################################################################################
#####    Load renaming table from s3 directory
################################################################################

name_df = spark.read.csv('s3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/wce_names_mapping.csv', header=True, inferSchema=True)
name_df.createOrReplaceTempView('name_df')
name_df.cache()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Rename Char Short Name and Rim

# COMMAND ----------

############################################################################################
#####    Join renaming table to weighting controls dataframe
#####    Rename the char short name and rim column using the values from the renaming table
############################################################################################



final_output_ps_renamed = final_output_ps.join(name_df, [final_output_ps.CHAR_SHORT_NAME == name_df.sub_rim_wce,\
                                                         final_output_ps.RIM == name_df.rim_dsci, \
                                                         final_output_ps.HH_PERSON == name_df.type], how='left')

final_output_ps_renamed = final_output_ps_renamed.withColumn('CHAR_SHORT_NAME',\
                               when(~final_output_ps_renamed.RIM.contains('GEOGRAPHY'), final_output_ps_renamed.charshortname_appdev)\
                                   .otherwise(final_output_ps_renamed.CHAR_SHORT_NAME))\
                               .withColumn('RIM',when(~final_output_ps_renamed.RIM.contains('GEOGRAPHY'), final_output_ps_renamed.rim_appdev)\
                                   .otherwise(final_output_ps_renamed.RIM))\
                               .select('DMA_ID', 'WEIGHT_TYPE', 'START_DATE', 'END_DATE', 'SEQ_ORDER',\
                                       'HH_PERSON',	'CHAR_SHORT_NAME', 'RIM', 'SSCM_KEY', 'UE')

# final_output_ps_renamed.write.parquet(final_output_module5_ps, mode='overwrite')
# display(final_output_ps_renamed)

# COMMAND ----------

# Combine PS and Design Weighing Controls
final_wc = final_output_ps_renamed.union(wc_design_wgt).orderBy("WEIGHT_TYPE","DMA_ID", "CHAR_SHORT_NAME")
final_wc.write.parquet(d15_file_output_dir, mode='overwrite')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Notebook Caller Output

# COMMAND ----------

dbutils.notebook.exit("Weighting Controls path: " + d15_file_output_dir)
