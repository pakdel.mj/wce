# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Effective Sample Size Computation 
# MAGIC >This notebook has to be executed as the second module of Weighting Control Evaluation (WCE) software. The effective sample sizes for each control/level combinations that are created at the end of this module are needed in module **Sufficient Sample Size Qualifier Engine** of WCE tool.
# MAGIC 
# MAGIC **Purpose:**
# MAGIC 
# MAGIC > * To compute the weighting controls effective sample size, using tables from the media data lake, for a supplied date range, selected DMAs and sample type. 
# MAGIC 
# MAGIC **LOW-LEVEL EXPLANATION OF WHAT NOTEBOOK DOES:**
# MAGIC >This software can be broken into 5 sub-modules.
# MAGIC 
# MAGIC > **Part 1: Read Household and Person Characteristics**
# MAGIC 
# MAGIC 
# MAGIC >>* Purpose of this part is to read household characteristics data from the TGB2 views (dsci_tam.household_characteristics) and person characteristics with intial weights that are created by  Person Weight Computation module.  
# MAGIC >>* for TGB2 views BBO homes are removed. Also, only keep TAM intab homes passing the set edit (LOCAL_DAILY_STATUS_IND in (1,3)) and intab PPM homes (LOCAL_DAILY_STATUS_IND in (4))
# MAGIC >>* Filteration used for the set_meter markets is 
# MAGIC 
# MAGIC >><pre>  
# MAGIC           (
# MAGIC       (npm_sample_ind='Y' and smm_sample_ind='N' and national_daily_status_ind in (1,3))
# MAGIC       or ((smm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4)))
# MAGIC           )</pre>
# MAGIC           
# MAGIC >>* Filteration used for the lpm markets is 
# MAGIC 
# MAGIC >><pre> ((lpm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4))) </pre>
# MAGIC 
# MAGIC 
# MAGIC > **Part 2: Compute Design Weights**
# MAGIC >>* Purpose of this part is to compute intab design weights for the households. 
# MAGIC >>* Aggregate data from TGB2 view and compute sample size per intab_date/dma_code/ppm_metro_code
# MAGIC >>* Pull up UEs from the dsci_tam.universe_estimates table for dma_code/ppm_metro_code combinations. Keep only UEs for the households without BBO and the measurement_period of interest. Also, keep only UEs with the highest load_id
# MAGIC >>* Join sample size table with the UE table by dma_code and ppm_metro_code. Intab design weight is UE divided by sample size and it is computed per intab_date/dma_code/ppm_metro_code.
# MAGIC 
# MAGIC > **Part 3: Transform Household Characteristics and Add the Column Weight**
# MAGIC 
# MAGIC >>* Purpose of this part is to implement the required data preparation before computing effective sample size at the fourth part of the notebook. Required data preparation on B1010 was already done in Person Weight Computation module. So this sub-module only implements TGB2 file's required data preparation
# MAGIC >>* The code creates labels for the weighting controls according to the weighting control specification documentation. All potential labels are mentioned in sub-module 4
# MAGIC >>* The transformed TGB2 table is then joined with the intab design weights that are created in sub-module 2 by intab_date,dma_code, and ppm_metro_code.  
# MAGIC 
# MAGIC > **Part 4: Calculate Effective Sample Size**
# MAGIC 
# MAGIC >>* Purpose of this part is to compute the effective sample size for all potential weighting controls. 
# MAGIC >>* In order to compute effective sample size for all controls in an automated way without excessive hard coding, the software creates the following Python dictionaries:  
# MAGIC 
# MAGIC >>| Dictionary Name            | Key                         | Value                                                                        |
# MAGIC   |- --------------------------|-----------------------------|------------------------------------------------------------------------------|
# MAGIC   | total_dma_controls         | DMA Controls                | Nested dictionary for each level                                             |
# MAGIC   | total_dma_derived_controls | Derived Controls            | Nested dictionary for each level including a Python list of derived controls |
# MAGIC   | minority_controls          | Minority Controls           | Python List of levels for minorities                                         |
# MAGIC   | minority_dict              | Black Ind/Origin Ind        | Python List of Black/Origin Indicators levels                                |
# MAGIC   | demo_controls              | Demographics Controls       | Python List of demographics levels                                           |           
# MAGIC >> ***total_dma_controls***
# MAGIC >>> The keys of this dictionary are potential controls for each DMA, the values of the dictionary are potential levels for each key. 
# MAGIC 
# MAGIC >>>| Market Break        | Controls                     | Levels                                                            |
# MAGIC   |----------------------------------------------------|-------------------------------------------------------------------|
# MAGIC   | DMA                 | ORIGIN                       | HISPANIC/NON-HISPANIC                                             |
# MAGIC   | DMA                 | BLACK                        | BLACK RACE/NON-BLACKRACE                                          |
# MAGIC   | DMA                 | ASIAN                        | ASIAN RACE/NON-ASIAN RACE                                         |
# MAGIC   | DMA                 | AIAN                         | AIAN RACE/NON-AIAN RACE                                           |
# MAGIC   | DMA                 | ADS                          | ADS YES/ADS NO                                                    |
# MAGIC   | DMA                 | DVR                          | DVR YES/DVR NO                                                    |  
# MAGIC   | DMA                 | CABLE                        | CABLE YES/CABLE NO/CABLE PLUS/BROADCAST ONLY                      |
# MAGIC   | DMA                 | CABLE_PLUS                   | CABLE PLUS/BROADCAST ONLY                                         |
# MAGIC   | DMA                 | MEASUREABLE SETS             | 1 SET/2 SETS/3 SETS/4+ SETS                                       |  
# MAGIC   | DMA                 | OPERABLE SETS                | 1 SET/2+ SETS                                                     | 
# MAGIC   | DMA                 | AGE OF HEAD                  | AGE OF HOUSEHOLDER <25/25-34/35-44/45-54/55-64/65+                | 
# MAGIC   | DMA                 | HOUSEHOLD SIZE               | 1 PERSON/2 PERSONS/3 PERSONS/4 PERSONS/5 PERSONS/6+ PERSONS       | 
# MAGIC   | DMA                 | LANGUAGE                     | NON-HISPANIC/OS/MS/OE/ME/SE                                       |
# MAGIC   | DMA                 | PRESENCE OF NONADULTS        | PRESENCE OF NONADULTS NONE <18/ONLY 0-11/ANY 12-17                |
# MAGIC   | DMA                 | PPM METRO CODE               | PPM Metro 1/PPM Metro 2/PPM Non-Metro                             |
# MAGIC    
# MAGIC >> ***minority_controls***
# MAGIC >>> The keys of this dictionary are potential controls for minorities/complements within a DMA, the values of the dictionary are potential levels for each key.
# MAGIC 
# MAGIC >>>| Market Break        | Controls                     | Levels                                                            |
# MAGIC   |----------------------------------------------------|-------------------------------------------------------------------|
# MAGIC   | DMA                 | AGE OF HEAD                  | AGE OF HOUSEHOLDER <25/25-34/35-44/45-54/55-64/65+                |  
# MAGIC   | DMA                 | HOUSEHOLD SIZE               | 1 PERSON/2 PERSONS/3 PERSONS/4 PERSONS/5 PERSONS/6+ PERSONS       | 
# MAGIC   | DMA                 | ADS                          | ADS YES/ADS NO                                                    |
# MAGIC   | DMA                 | CABLE                        | CABLE YES/CABLE NO                                                |
# MAGIC   | DMA                 | CABLE_PLUS                   | CABLE PLUS/BROADCAST ONLY                                         |
# MAGIC 
# MAGIC >> ***total_dma_derived_controls***
# MAGIC >>> The keys of this dictionary are potential derived controls for each DMA, the values of the dictionary are potential levels for each key. Remeber that the same dictionary is also used as the potential derived controls for minorities/complements within a DMA.
# MAGIC 
# MAGIC >>>| Market Break        | Controls                     | Levels                                                            |
# MAGIC   |----------------------------------------------------|-------------------------------------------------------------------|
# MAGIC   | DMA                 | AGE OF HEAD                  | AGE OF HOUSEHOLDER <35/35+/<55/55+/35_54                          | 
# MAGIC   | DMA                 | HOUSEHOLD SIZE               | 1_2 PERSON/3_4 PERSONS/3+ PERSONS/5+ PERSONS                      | 
# MAGIC   | DMA                 | LANGUAGE                     | S DOMINANT/E Dominant/S DOMINANT BILINGUAL/E DOMINANT BILINGUAL   |
# MAGIC   | DMA                 | PRESENCE OF NONADULTS        | ANY_CHILD                                                         |                              
# MAGIC   | DMA                 | MEASUREABLE SETS             | 2+ SETS                                                           |  
# MAGIC   | DMA                 | PPM METRO CODE               | PPM Metro                                                         |
# MAGIC >> ***minority_dict***
# MAGIC >>> The keys of this dictionary are BLACK or ORIGIN indicator, the values of dictionary are potential levels for the keys.
# MAGIC 
# MAGIC >>>| Market Break        | key                         | value                                                             |
# MAGIC   |----------------------------------------------------|-------------------------------------------------------------------|
# MAGIC   | Minority/Complement | BLACK                        | BLACK_RACE/NON-BLACK_RACE                                         | 
# MAGIC   | Minority/Complement | ORIGIN                       | HISPANIC/NON-HISPANIC                                             | 
# MAGIC 
# MAGIC >> ***demo_controls***
# MAGIC >>> The only key of this dictionary is the person, the values of the dictionary are all potential demo buckets. 
# MAGIC 
# MAGIC >>>| Market Break        | Controls                     | Levels                                                            |
# MAGIC   |----------------------------------------------------|-------------------------------------------------------------------|
# MAGIC   | DMA PERSONS         | PERSONS                      | C2-5, C6-11,M12-17,M18-24,M25-34,M35-49,M50-54,M55_64,M65+        | 
# MAGIC   |                     |                              |            ,F12-17,F18-24,F25-34,F35-49,F50-54,F55_64,F65+        | 
# MAGIC   
# MAGIC >>* The 5 dictionaries defined above are used to compute the effective sample size of all potential weighting controls for each DMA, minority/complements within a DMA and demo buckets. The software created the following 5 datasets.                
# MAGIC >>- **tgb2_agg**: has effective sample size for all controls/levels in total_dma_controls dictionary using TGB2 dataset 
# MAGIC >>- **tgb2_agg_der**: has effective sample size for all controls/levels in total_dma_derived_controls dictionary using TGB2 dataset
# MAGIC >>- **tgb2_agg_minority**: has effective sample size for all controls/levels in minority_controls and minority_dict dictionaries using TGB2 dataset
# MAGIC >>- **tgb2_agg_der_minority**: has effective sample size for all controls/levels in total_dma_derived_controls and minority_dict dictionaries using TGB2 dataset
# MAGIC >>- **b1010_agg_minority**: has effective sample size for all controls/levels in demo_controls and minority_dict dictionaries using B1010 dataset
# MAGIC 
# MAGIC > **Part 5: Find Controls with Missing Levels, Create them with Effective Sample Size of 0 and Saving the Output to S3**
# MAGIC 
# MAGIC >>* Purpose of this part is to find all weighting controls with missing levels per DMA in each of the five dataframes created in sub-module4. For example dma_code 515 might not have any household with the age of householder less than 25. That level will be missing for the age of householder weighting control for dma_code 515.     
# MAGIC >>* Each missing weighting control level is added and is given an **effective sample size of 0**. 
# MAGIC >>* After finding and creating all missing levels in each of the mentioned 5 data sets, the data sets are joined (union all) together and the final super dataset is saved as a parquet file to s3. 
# MAGIC              
# MAGIC 
# MAGIC **USER INPUTS:** 
# MAGIC > 1. **start_date**
# MAGIC >  - The beginning of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC > 2. **end_date**
# MAGIC >  - The ending of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC > 3. **sample_type** 
# MAGIC >  - Market type of the analysis, provided as a string, the acceptable market types are 'SET_METER+PPM' or 'LPM+PPM'. The software cannot be executed for LPM plus PPM and set meter plus ppm markets simultaneously.
# MAGIC > 4. **DMA codes**
# MAGIC >  - this should be a Python string of dmas, e.g. '501,602,703', '501'.
# MAGIC >  - lmp plus ppm market codes are '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC >  - set meter plus ppm market codes are '515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839'
# MAGIC > 5. **ue_table_measurement_period**
# MAGIC >  - The period for which UEs are pulled up from the MDL, provided as a string, in the following format **monthyy**
# MAGIC >  - The software only pulls up ues with the maximum load id.
# MAGIC >  - examples are 'Feb18' or 'May18'
# MAGIC > 5. **input_b1010_dir**
# MAGIC >  - The directory of the person characteristics file with weights, saved to S3 with a Parquet format, provided as a string.
# MAGIC >  - This file is the outputed by Persosn Weight Computation module of WCE tool.
# MAGIC > 6. **please_export**
# MAGIC >  - Provided as a string, set this varialbe to 'True' if you want to save the data in the Optional Overrides sections and 'False' otherwise. 
# MAGIC > 7. **override_dir**
# MAGIC >  - the directory in which the software saves the output with a parquet fomat
# MAGIC  
# MAGIC **THE OUTPUT FORMAT:**
# MAGIC > | col_name                   | data_type | ex_results                            |
# MAGIC   |----------------------------|-----------|---------------------------------------|
# MAGIC   | dma_code                   | bigint    | 501                                   |
# MAGIC   | control                    | string    | ADS                                   |
# MAGIC   | parameter                  | string    | ADS_YES/ADS_NO                        |
# MAGIC   | effective_sample_size      | bigint    | 213.66                                |
# MAGIC   
# MAGIC  
# MAGIC 
# MAGIC **DATA SOURCES:**
# MAGIC > | schema               | table                                                                           |
# MAGIC   |----------------------|---------------------------------------------------------- ----------------------|
# MAGIC   | dsci_tam             | universe_estimates                                                              |
# MAGIC   | dsci_tam             | household_characteristics                                                       |
# MAGIC   | dsci_tam             | person_characteristics                                                          |
# MAGIC   | --------             | a Parquet file that is created by Person Weight Computation Module of WCE tool  |
# MAGIC 
# MAGIC   
# MAGIC **AN EXAMPLE RUN:**
# MAGIC > <pre>
# MAGIC start_date                    = '2018-03-01'
# MAGIC end_date                      = '2018-03-02'
# MAGIC sample_type                   = 'LPM+PPM'
# MAGIC dma_codes                     = '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC ue_table_measurement_period   = 'May18' 
# MAGIC input_b1010_dir               = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/person_initial_weights.20180301_20180302.parquet' 
# MAGIC please_export                 = 'True'
# MAGIC override_dir                  = '''s3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/person_initial_weights.20180301_20180302.parquet'''
# MAGIC </pre>
# MAGIC 
# MAGIC **AN EXAMPLE OUTPUT:**
# MAGIC > <pre>
# MAGIC <table class="confluenceTable">
# MAGIC <tbody>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">dma_code</td>
# MAGIC <td class="confluenceTd mceSelected">control</td>
# MAGIC <td class="confluenceTd mceSelected">parameter</td>
# MAGIC <td class="confluenceTd mceSelected">effective_sample_size</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_YES</td>
# MAGIC <td class="confluenceTd mceSelected">213.66</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NO</td>
# MAGIC <td class="confluenceTd mceSelected">2628.98</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NO</td>
# MAGIC <td class="confluenceTd mceSelected">559.67</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_YES</td>
# MAGIC <td class="confluenceTd mceSelected">38.76</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NO</td>
# MAGIC <td class="confluenceTd mceSelected">637.82</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_YES</td>
# MAGIC <td class="confluenceTd mceSelected">65.29</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NON-BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NO</td>
# MAGIC <td class="confluenceTd mceSelected">2077.55</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NON-BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_YES</td>
# MAGIC <td class="confluenceTd mceSelected">175.47</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NON-HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NO</td>
# MAGIC <td class="confluenceTd mceSelected">1997.77</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_NON-HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">ADS_YES</td>
# MAGIC <td class="confluenceTd mceSelected">153.22</td>
# MAGIC </tr>
# MAGIC </tbody>
# MAGIC </table>
# MAGIC </pre>
# MAGIC 
# MAGIC **CODE DEVELOPERS:**
# MAGIC >* MJ PAKDEL
# MAGIC >* JONATHAN OUEGNIN

# COMMAND ----------

# MAGIC %md
# MAGIC ### Import Libraries and Functions

# COMMAND ----------

from pyspark.sql.functions import when
from pyspark.sql.types import StructField, StringType, StructType
import pandas as pd

# COMMAND ----------

# MAGIC %md
# MAGIC ### Define Parameters to Run the Cookbooks and  Final Output

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################

start_date                    = '2018-03-01'
end_date                      = '2018-03-01'
sample_type                   = 'SET_METER+PPM'
# dma_codes                     = '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
dma_codes                     = '515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839'
ue_table_measurement_period   = 'May18' 
input_b1010_dir               = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/person_initial_weights_2018_03_01_2018_03_02.parquet' # b1010 output file
please_export                 = 'True'

# COMMAND ----------

##########################################################
#####    USER INPUTS (called in another notebook)    #####
##########################################################

start_date                        = str(dbutils.widgets.get('start_date'))
end_date                          = str(dbutils.widgets.get('end_date'))
sample_type                       = str(dbutils.widgets.get('sample_type'))
dma_codes                         = str(dbutils.widgets.get('dma_codes'))
ue_table_measurement_period       = str(dbutils.widgets.get('ue_table_measurement_period'))

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Validate Sample Type User Input

# COMMAND ----------

sample_type = sample_type.upper()

if (sample_type != "LPM+PPM") and (sample_type != "SET_METER+PPM"):
    dbutils.notebook.exit("Invalid Sample Type Input")

# COMMAND ----------

# Directory path of module 1 dataframe result 
input_b1010_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/person_initial_weights_' + start_date.replace('-','_') + '_' + end_date.replace('-','_') + '_' + sample_type.replace('+','_') + '.parquet' 

# Directory path for module output 
final_output_module1_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/effective_sample_size_data_' + start_date.replace('-','_') + '_' + end_date.replace('-','_') + '_' + sample_type.replace('+','_') + '.parquet'

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create the filters based on the sample type

# COMMAND ----------

###################################################################################################################
#####    Depending on the sample_type, here some filters are created to pull data from TGB2 and B1010    #########
###################################################################################################################

if sample_type == 'SET_METER+PPM':
    sample_type = '''(
      (npm_sample_ind='Y' and smm_sample_ind='N' and national_daily_status_ind in (1,3))
      or ((smm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4)))
      )'''
  
elif sample_type == 'LPM+PPM':
    sample_type="((lpm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4)))"

cached_tables = []    

sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy") 

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 1: Read Household and Person Characteristics 

# COMMAND ----------

# MAGIC %md
# MAGIC ### Pull up TGB2 Household Characteristics View

# COMMAND ----------

###################################################################################################################
#####    Pull up data from TGB2 view (dsci_tam.household_characteristics)                                    ######
#####    BBO homes are removed                                                                               ###### 
###################################################################################################################

s = '''
SELECT 
    DMA_CODE,INTAB_DATE,HOUSEHOLD_ID,lpm_sample_ind,ppm_sample_ind,smm_sample_ind,
    HOH_ORIGIN_CODE,HOH_AGE,CENSUS_NUM_OF_PERSONS,CHILDREN_LESS_THAN_12_IND,
    CHILDREN_12_TO_17_IND,CHILDREN_LESS_THAN_18_IND,LANGUAGE_CLASS_CODE,HH_BLACK_IND,HH_ASIAN_IND,
    HH_AMERICAN_INDIAN_IND,HH_ADS_IND,NUMBER_OF_VIEWABLE_SETS,TOTAL_NUMBER_OF_OPERABLE_SETS,HH_DVR_IND,CABLE_IND,HH_CABLE_PLUS_IND,NMR_STCTY,
    CASE WHEN PPM_METRO_CODE=1 THEN 'PPM Metro 1'
        WHEN PPM_METRO_CODE=2 THEN 'PPM Metro 2'
        ELSE 'PPM Non-Metro' END AS PPM_METRO_CODE
FROM 
    dsci_tam.household_characteristics 
WHERE 1=1
    AND intab_date between '{start_date}' and '{end_date}'
    AND dma_code in ({dma_codes})
    AND {sample_type}
    AND HH_BROADBAND_ONLY_IND != 'Y'
'''.format(dma_codes=dma_codes,start_date=start_date,end_date=end_date,sample_type=sample_type)
tgb2_file = spark.sql(s)
tgb2_file.createOrReplaceTempView('tgb2_file')
tgb2_file.cache()
cached_tables.append('tgb2_file')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Pull up B1010 Persons with Initial Person Weights from Person Weight Computation Module

# COMMAND ----------

b1010_file_w = sqlContext.read.parquet(input_b1010_dir)
b1010_file_w=b1010_file_w.withColumnRenamed("person_weight", "weight")
b1010_file_w.createOrReplaceTempView('b1010_file_w')
b1010_file_w.cache()
b1010_file_w.count()
cached_tables.append('b1010_file_w')

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 2: Compute Design Weights

# COMMAND ----------

# MAGIC %md
# MAGIC ### Compute intab counts using TGB2 File

# COMMAND ----------

###################################################################################################################
#####    Aggregate data from TGB2 view and compute sample size per intab_date/dma_code/ppm_metro_code
###################################################################################################################

s = '''
SELECT
   INTAB_DATE,
   DMA_CODE,
   PPM_METRO_CODE,
   count(*) as intab_count
FROM
   tgb2_file
GROUP BY
   INTAB_DATE,DMA_CODE,PPM_METRO_CODE
ORDER BY
   INTAB_DATE,DMA_CODE,PPM_METRO_CODE
'''
intab_counts = spark.sql(s)
intab_counts.createOrReplaceTempView('intab_counts')
intab_counts.cache()
cached_tables.append('intab_counts')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Pull UE Table

# COMMAND ----------

###################################################################################################################
#####    Pull up UEs from the dsci_tam.universe_estimates table for dma_code/ppm_metro_code combinations. 
#####    Keep only UEs for the households without BBO and the measurement_period of interest. 
#####    Keep only UEs with the highest load_id.
###################################################################################################################

max_load_id_query = '''
SELECT
    MAX(load_id) AS max_load_id
FROM
    dsci_tam.universe_estimates
WHERE 
    measurement_period='{0}'
'''.format(ue_table_measurement_period)

max_load_id = spark.sql(max_load_id_query).collect()[0][0]

s = ''' 
SELECT 
   geography_code as dma_code,market_break,universe_estimate
FROM
   dsci_tam.universe_estimates 
WHERE 
   measurement_period='{ue_period}'
   AND ue_type='H'
   AND load_id={max_load_id}
   AND ((market_break="PPM Metro 1" AND market_break_description="TV Household") OR 
       (market_break="PPM Metro 2" AND market_break_description="TV Household") OR 
       (market_break="PPM Non-Metro" AND market_break_description="TV Household"))
ORDER BY
   dma_code,market_break
'''.format(ue_period=ue_table_measurement_period, max_load_id=max_load_id)

ue_table = spark.sql(s)
ue_table.createOrReplaceTempView("ue_table")
ue_table.cache()
cached_tables.append('ue_table')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Compute Intab Design Weight

# COMMAND ----------

###################################################################################################################
#####    Join sample size table with the UE table by dma_code and ppm_metro_code. 
#####    Intab design weight is UE divided by sample size and it is computed per intab_date/dma_code/ppm_metro_code
###################################################################################################################

s = '''
SELECT --count(*)
   A.*,B.universe_estimate,round(B.universe_estimate/A.intab_count,4) as weight
FROM
   intab_counts A
JOIN
   ue_table B
WHERE
   A.DMA_CODE=B.dma_code
   AND A.ppm_metro_code=B.market_break
ORDER BY
   A.DMA_CODE,A.PPM_METRO_CODE
'''

calculated_design_weight = spark.sql(s).coalesce(1)
calculated_design_weight.createOrReplaceTempView('calculated_design_weight')
calculated_design_weight.cache()
calculated_design_weight.count()
cached_tables.append('calculated_design_weight')

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 3: Transform Household Characteristics and Add the Column Weight

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create New columns for TGB2

# COMMAND ----------

###################################################################################################################
#####    Transform TGB@ dataframe by adding column needed to compute effective sample size only 
#####    (characteristics, PPM Metro), weights)
#####    Intab design weight is UE divided by sample size and it is computed per intab_date/dma_code/ppm_metro_code
###################################################################################################################

s = '''
SELECT 
    a.INTAB_DATE,a.dma_code,a.HOUSEHOLD_ID,
    CASE 
        WHEN a.HOH_ORIGIN_CODE = '1' THEN 'NON-HISPANIC'
        ELSE 'HISPANIC' 
    END AS ORIGIN,
    CASE 
        WHEN CAST(a.HOH_AGE AS int) < 25 THEN 'AGE_OF_HOUSEHOLDER_<25'
        WHEN CAST(a.HOH_AGE AS int) >= 25 AND CAST(a.HOH_AGE AS int) <= 34 THEN 'AGE_OF_HOUSEHOLDER_25-34' 
        WHEN CAST(a.HOH_AGE AS int) >= 35 AND CAST(a.HOH_AGE AS int) <= 44 THEN 'AGE_OF_HOUSEHOLDER_35-44'
        WHEN CAST(a.HOH_AGE AS int) >= 45 AND CAST(a.HOH_AGE AS int) <= 54 THEN 'AGE_OF_HOUSEHOLDER_45-54'
        WHEN CAST(a.HOH_AGE AS int) >= 55 AND CAST(a.HOH_AGE AS int) <= 64 THEN 'AGE_OF_HOUSEHOLDER_55-64'
        WHEN CAST(a.HOH_AGE AS int) >= 65 THEN 'AGE_OF_HOUSEHOLDER_65+'
    END AS AGE_OF_HEAD,
    CASE 
        WHEN a.HOH_ORIGIN_CODE = '1' THEN 'NON-HISPANIC'
        WHEN a.HOH_ORIGIN_CODE = '2' AND a.LANGUAGE_CLASS_CODE = '1' THEN 'SPANISH_ONLY'
        WHEN a.HOH_ORIGIN_CODE = '2' AND a.LANGUAGE_CLASS_CODE = '2' THEN 'ENGLISH_ONLY'
        WHEN a.HOH_ORIGIN_CODE = '2' AND a.LANGUAGE_CLASS_CODE = '3' THEN 'MOSTLY_SPANISH'
        WHEN a.HOH_ORIGIN_CODE = '2' AND a.LANGUAGE_CLASS_CODE = '4' THEN 'MOSTLY_ENGLISH'
        WHEN a.HOH_ORIGIN_CODE = '2' AND a.LANGUAGE_CLASS_CODE = '5' THEN 'SPANISH_ENGLISH_EQUAL'
    END AS LANGUAGE,
    CASE 
        WHEN a.CENSUS_NUM_OF_PERSONS = '1' THEN '1_PERSON'
        WHEN a.CENSUS_NUM_OF_PERSONS = '2' THEN '2_PERSONS'
        WHEN a.CENSUS_NUM_OF_PERSONS = '3' THEN '3_PERSONS'
        WHEN a.CENSUS_NUM_OF_PERSONS = '4' THEN '4_PERSONS'
        WHEN a.CENSUS_NUM_OF_PERSONS = '5' THEN '5_PERSONS'
        ELSE '6+_PERSONS'
    END AS HOUSEHOLD_SIZE,
    CASE
        WHEN a.CHILDREN_LESS_THAN_18_IND = 'N' THEN 'PRESENCE_OF_NONADULTS_NONE_<18'
        WHEN a.CHILDREN_LESS_THAN_12_IND = 'Y' AND a.CHILDREN_12_TO_17_IND = 'N' THEN 'PRESENCE_OF_NONADULTS_ONLY_<12'
        WHEN a.CHILDREN_12_TO_17_IND = 'Y' THEN 'PRESENCE_OF_NONADULTS_ANY_12-17'
    END AS PRESENCE_OF_NONADULTS,
    CASE
        WHEN a.NUMBER_OF_VIEWABLE_SETS = '1' THEN '1_SET'
        WHEN a.NUMBER_OF_VIEWABLE_SETS = '2' THEN '2_SETS'
        WHEN a.NUMBER_OF_VIEWABLE_SETS = '3' THEN '3_SETS'
        ELSE '4+_SETS'
    END AS MEASURABLE_SETS,
    CASE
        WHEN a.TOTAL_NUMBER_OF_OPERABLE_SETS = '1' THEN '1_SET'
        ELSE '2+_SETS'
    END AS OPERABLE_SETS,
    CASE
        WHEN a.HH_BLACK_IND = 'Y' THEN 'BLACK_RACE'
        ELSE 'NON-BLACK_RACE'
    END AS BLACK,
    CASE
        WHEN a.HH_ASIAN_IND = 'Y' THEN 'ASIAN_RACE'
        ELSE 'NON-ASIAN_RACE'
    END AS ASIAN,
    CASE
        WHEN a.HH_AMERICAN_INDIAN_IND = 'Y' THEN 'AIAN_RACE'
        ELSE 'NON-AIAN_RACE'
    END AS AIAN,
    CASE 
        WHEN a.HH_ADS_IND = 'Y' THEN 'ADS_YES'
        ELSE 'ADS_NO'
    END AS ADS,
    CASE 
        WHEN a.HH_DVR_IND = 'Y' THEN 'DVR_YES'
        ELSE 'DVR_NO'
    END AS DVR,
    CASE 
        WHEN a.CABLE_IND = 'Y' THEN 'CABLE_YES'
        ELSE 'CABLE_NO'
    END AS CABLE,
    CASE 
        WHEN a.HH_CABLE_PLUS_IND = 'Y' THEN 'CABLE_PLUS_YES'
        ELSE 'BROADCAST_ONLY'
    END AS CABLE_PLUS,
    a.NMR_STCTY,
    a.LPM_SAMPLE_IND,
    a.SMM_SAMPLE_IND,
    a.PPM_SAMPLE_IND,
    a.PPM_METRO_CODE,
    b.weight
FROM
    tgb2_file AS a
JOIN 
    calculated_design_weight AS b
ON 
    a.dma_code=b.dma_code
    AND a.PPM_METRO_CODE=b.PPM_METRO_CODE
    AND a.intab_date=b.intab_date
'''

tgb2_file_w = spark.sql(s).repartition(16)
sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")
tgb2_file_w = tgb2_file_w.drop('NMR_STCTY','LPM_SAMPLE_IND','SMM_SAMPLE_IND','PPM_SAMPLE_IND')
tgb2_file_w.createOrReplaceTempView('tgb2_file_w')
tgb2_file_w.cache()
tgb2_file_w.count()
cached_tables.append('tgb2_file_w')

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 4: Calculate Effective Sample Size 

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create Dictionaries

# COMMAND ----------

#####################################################################################################################
#####    Create 5 dictionaries used in order to compute the effective sample size of all potential weighting controls 
#####    for each DMA, minority/complements within a DMA and demo buckets.
#####################################################################################################################

######################
# Total DMA Controls #
######################

total_dma_controls = {
    'ORIGIN':  {
      'HISPANIC',
      'NON-HISPANIC'
    },
    'BLACK':  {
      'BLACK_RACE',
      'NON-BLACK_RACE'
    },
    'ASIAN':  {
      'ASIAN_RACE',
      'NON-ASIAN_RACE'
    },
    'AIAN':  {
      'AIAN_RACE',
      'NON-AIAN_RACE'
    },
    'ADS':  {
      'ADS_YES',
      'ADS_NO'
    },
    'DVR':  {
      'DVR_YES',
      'DVR_NO'
    },
    'CABLE':  {
      'CABLE_YES',
      'CABLE_NO'
    },
    'CABLE_PLUS':  {
      'CABLE_PLUS_YES',
      'BROADCAST_ONLY'
    },
    'MEASURABLE_SETS':  {
      '1_SET',
      '2_SETS',
      '3_SETS',
      '4+_SETS'
    },
    'OPERABLE_SETS':  {
      '1_SET',
      '2+_SETS'
    },
    'AGE_OF_HEAD': {
      'AGE_OF_HOUSEHOLDER_<25',
      'AGE_OF_HOUSEHOLDER_25-34',
      'AGE_OF_HOUSEHOLDER_35-44',
      'AGE_OF_HOUSEHOLDER_45-54',
      'AGE_OF_HOUSEHOLDER_55-64',
      'AGE_OF_HOUSEHOLDER_65+'
    },
    'HOUSEHOLD_SIZE': {
      '1_PERSON',
      '2_PERSONS',
      '3_PERSONS', 
      '4_PERSONS', 
      '5_PERSONS', 
      '6+_PERSONS'
    },
  
    'LANGUAGE': {
      'NON-HISPANIC',
      'SPANISH_ONLY',
      'ENGLISH_ONLY',
      'MOSTLY_SPANISH',
      'MOSTLY_ENGLISH',
      'SPANISH_ENGLISH_EQUAL'
    },
   
    'PRESENCE_OF_NONADULTS': {
      'PRESENCE_OF_NONADULTS_NONE_<18',
      'PRESENCE_OF_NONADULTS_ONLY_<12',
      'PRESENCE_OF_NONADULTS_ANY_12-17'
    },
  
    'PPM_METRO_CODE': {
      'PPM Metro 1',
      'PPM Metro 2',
      'PPM Non-Metro'
    }
}

#####################
# Minority Controls #
#####################

minority_controls = {
    'ADS':  {
      'ADS_YES',
      'ADS_NO'
    },
    'CABLE':  {
      'CABLE_YES',
      'CABLE_NO'
    },
    'CABLE_PLUS':  {
      'CABLE_PLUS_YES',
      'BROADCAST_ONLY'
    },
    'AGE_OF_HEAD': {
      'AGE_OF_HOUSEHOLDER_<25',
      'AGE_OF_HOUSEHOLDER_25-34',
      'AGE_OF_HOUSEHOLDER_35-44',
      'AGE_OF_HOUSEHOLDER_45-54',
      'AGE_OF_HOUSEHOLDER_55-64',
      'AGE_OF_HOUSEHOLDER_65+'
    },
    'HOUSEHOLD_SIZE': {
      '1_PERSON',
      '2_PERSONS',
      '3_PERSONS', 
      '4_PERSONS', 
      '5_PERSONS', 
      '6+_PERSONS'
    }
  
  
}

##############################
# Total DMA Derived Controls #
##############################

total_dma_derived_controls = {
    'AGE_OF_HEAD': {
        'AGE_OF_HOUSEHOLDER_<35': ['AGE_OF_HOUSEHOLDER_<25','AGE_OF_HOUSEHOLDER_25-34'],
        'AGE_OF_HOUSEHOLDER_35+': ['AGE_OF_HOUSEHOLDER_35-44','AGE_OF_HOUSEHOLDER_45-54','AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+'],
        'AGE_OF_HOUSEHOLDER_<55': ['AGE_OF_HOUSEHOLDER_<25', 'AGE_OF_HOUSEHOLDER_25-34','AGE_OF_HOUSEHOLDER_35-44','AGE_OF_HOUSEHOLDER_45-54'],
        'AGE_OF_HOUSEHOLDER_55+': ['AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+'],
        'AGE_OF_HOUSEHOLDER_35-54': ['AGE_OF_HOUSEHOLDER_35-44','AGE_OF_HOUSEHOLDER_45-54']
    },
    'HOUSEHOLD_SIZE': {
        '1-2_PERSONS': ['1_PERSON','2_PERSONS'],
        '3-4_PERSONS': ['3_PERSONS', '4_PERSONS'],
        '3+_PERSONS': ['3_PERSONS', '4_PERSONS', '5_PERSONS', '6+_PERSONS'],
        '5+_PERSONS': ['5_PERSONS', '6+_PERSONS']
    },
    'LANGUAGE': {
        'SPANISH_DOMINANT': ['SPANISH_ONLY','MOSTLY_SPANISH'],
        'ENGLISH_DOMINANT': ['ENGLISH_ONLY','MOSTLY_ENGLISH'],
        'ENGLISH_DOMINANT_BILINGUAL': ['ENGLISH_ONLY','MOSTLY_ENGLISH','SPANISH_ENGLISH_EQUAL'],
        'SPANISH_DOMINANT_BILINGUAL': ['SPANISH_ONLY','MOSTLY_SPANISH','SPANISH_ENGLISH_EQUAL']
    },
  'PRESENCE_OF_NONADULTS': {
    'ANY_CHILD':['PRESENCE_OF_NONADULTS_ONLY_<12','PRESENCE_OF_NONADULTS_ANY_12-17']
  },
  'MEASURABLE_SETS': {
    '2+_SETS': ['2_SETS','3_SETS','4+_SETS']
  },
  'PPM_METRO_CODE': {
     'PPM Metro':['PPM Metro 1','PPM Metro 2']
  }
  
}

#######################
# Minority Dictionary #
#######################

minority_dict = {'BLACK': ['BLACK_RACE','NON-BLACK_RACE'],
              'ORIGIN': ['HISPANIC','NON-HISPANIC']}

########################
# Demographic Controls #
########################

demo_controls = {'PERSONS':['C2_5','C6_11','C2_11','P2_17','P12_17',
                          'M12_17','M18_24','M25_34','M35_49','M50_54','M55_64','M65_999','M18_34','M18_49','M50_999','M50_64','M18_999',
                          'F12_17','F18_24','F25_34','F35_49','F50_54','F55_64','F65_999','F18_34','F18_49','F50_999','F50_64','F18_999']}

# COMMAND ----------

# MAGIC %md
# MAGIC ### Calculate Effective Sample Size for Total DMA Controls

# COMMAND ----------

#####################################################################################
#####  Create query to compute effective sample size for Total DMA              #####
##### Compute effective sample size for Total DMA                               #####
#####################################################################################

# Create string containing the SQL query used in the loop 
tgb2_agg_query_part = '''
SELECT 
    dma_code,
    '{control}' AS control,
    parameter,
    ROUND(mean(PESS),2) AS effective_sample_size
FROM
    (
    SELECT
        INTAB_DATE,
        dma_code,
        {control} AS parameter,
        ((sum(weight)*sum(weight))/sum(weight*weight)) AS PESS
    FROM 
        {table} 
    GROUP BY
        INTAB_DATE,dma_code,parameter
     )
GROUP BY
     dma_code,control,parameter
     
UNION ALL  
'''
# Create empty SQL query
tgb2_agg_query=''

# Loop over the dictionary and consolidate query parts 
for key in total_dma_controls.keys():
  
  tgb2_agg_query+=tgb2_agg_query_part.format(control=key,table='tgb2_file_w')      

tgb2_agg_query = tgb2_agg_query.rstrip('UNION ALL\n    ')
tgb2_agg = spark.sql(tgb2_agg_query).repartition(1)
tgb2_agg.createOrReplaceTempView('tgb2_agg')
tgb2_agg.cache()
cached_tables.append('tgb2_agg')

tgb2_agg.write.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/tgb2.agg.parquet', mode='overwrite')
tgb2_agg = spark.read.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/tgb2.agg.parquet')
tgb2_agg_pd = tgb2_agg.toPandas()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Calculate Effective Sample Size for Minorities Controls

# COMMAND ----------

#####################################################################################
#####  Create query to compute effective sample size for Minorities             #####
##### Compute effective sample size for Minorities                              #####
#####################################################################################

# Create string containing the SQL query used in the loop 
tgb2_agg_minority_query_part = '''
SELECT 
    dma_code,
    concat('{control}','_{minority_level}') AS control,
    parameter,
    ROUND(mean(PESS),2) AS effective_sample_size
FROM
    (
    SELECT
        INTAB_DATE,
        dma_code, 
        {control} AS parameter,
        ((sum(weight)*sum(weight))/sum(weight*weight)) AS PESS
    FROM 
        {table} 
    WHERE
        1=1 AND
        {minority} = '{minority_level}'
    GROUP BY
        INTAB_DATE,dma_code,parameter
     ) 
GROUP BY
     dma_code,control,parameter
  
UNION ALL  
'''

# Create empty SQL query
tgb2_agg_query_minority = ''

# Loop over the dictionaries and consolidate query parts 
for key in minority_controls.keys():

  for minority, minority_level in minority_dict.items():

    for index_m,i in enumerate(minority_level):

      tgb2_agg_query_minority += tgb2_agg_minority_query_part\
      .format(control=key,minority=minority,minority_level=minority_level[index_m],table='tgb2_file_w')

tgb2_agg_query_minority = tgb2_agg_query_minority.rstrip('UNION ALL\n    ')

tgb2_agg_minority = spark.sql(tgb2_agg_query_minority).repartition(1)
tgb2_agg_minority.createOrReplaceTempView('tgb2_agg_minority')
tgb2_agg_minority.cache()
cached_tables.append('tgb2_agg_minority')

tgb2_agg_minority.write.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/tgb2.agg.minority.parquet', mode='overwrite')
tgb2_agg_minority = spark.read.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/tgb2.agg.minority.parquet')
tgb2_agg_minority_pd = tgb2_agg_minority.toPandas()

# COMMAND ----------

##########################################################################################
#####  Create query to compute effective sample size for Total DMA Derived Controls  #####
#####  Compute effective sample size for Total DMA Derived Controls                  #####
##########################################################################################

# Create string containing the SQL query used in the loop 
tgb2_agg_der_query_part = '''
SELECT 
    dma_code,
    '{control}' AS control,
    '{derived_level}' AS parameter,
    ROUND(mean(PESS),2) AS effective_sample_size
FROM
    (
    SELECT
        INTAB_DATE,
        dma_code,
        '{derived_level}' AS parameter,
        ((sum(weight)*sum(weight))/sum(weight*weight)) AS PESS
    FROM 
        {table}
    WHERE 1=1 
      AND {control} IN ('{levels}')
    GROUP BY
        INTAB_DATE,dma_code,parameter
     )
GROUP BY
     dma_code,control,parameter
  
UNION ALL  
'''

# Create empty SQL query
tgb2_agg_der_query = ''

# Loop over the dictionary and consolidate query parts 
for key, value in total_dma_derived_controls.items():

  for new_level, level in value.items():
    
    level_breaks = "', '".join(level)
    
    tgb2_agg_der_query += tgb2_agg_der_query_part\
    .format(control=key,derived_level=new_level,levels=level_breaks,table='tgb2_file_w')
      
tgb2_agg_der_query = tgb2_agg_der_query.rstrip('UNION ALL\n    ')

tgb2_agg_der = spark.sql(tgb2_agg_der_query).coalesce(1)
tgb2_agg_der.createOrReplaceTempView('tgb2_agg_der')
tgb2_agg_der.cache()
cached_tables.append('tgb2_agg_der')

tgb2_agg_der.write.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/tgb2.agg.der.parquet', mode='overwrite')
tgb2_agg_der = spark.read.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/tgb2.agg.der.parquet')
tgb2_agg_der_pd = tgb2_agg_der.toPandas()

# COMMAND ----------

# Create schema for empty dataframes used to save the reuslts of the queries. The schema is encoded in a string
schemaString = "dma_code control parameter effective_sample_size"
fields = [StructField(field_name, StringType(), True) for field_name in schemaString.split()]
schema = StructType(fields)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Calculate Effective Sample Size for Minorities Derived Controls

# COMMAND ----------

##########################################################################################
#####  Create query to compute effective sample size for Minorities Derived Controls #####
#####  Compute effective sample size for Minorities Derived Controls                 #####
#####  Convert Pyspark dataframe to Pandas dataframe                                 #####
##########################################################################################

# Create empty dataframe to save results
tgb2_agg_der_minority = sqlContext.createDataFrame(sc.emptyRDD(),schema) 

# Create string containing the SQL query used in the loop 
tgb2_agg_der_minority_query_part = '''
SELECT 
    dma_code,
    concat('{control}','_{minority_level}') AS control,
    '{derived_level}' AS parameter,
    ROUND(mean(PESS),2) AS effective_sample_size
FROM
    (
    SELECT
        INTAB_DATE,
        dma_code,
        '{derived_level}' AS parameter,
        ((sum(weight)*sum(weight))/sum(weight*weight)) AS PESS
    FROM 
        {table}
    WHERE 1=1  
      AND {control} IN ('{levels}') 
      AND {minority} = '{minority_level}'
    GROUP BY
        INTAB_DATE,dma_code,parameter
     ) 
GROUP BY
     dma_code,control,parameter
'''

# Create empty SQL query
tgb2_agg_der_minority_query = ''

# Loop over the dictionaries and consolidate query parts and append result to empty dataframe
for key, value in total_dma_derived_controls.items():
  
  for minority, minority_level in minority_dict.items():
    
    for index_m,i in enumerate(minority_level): 
    
      for new_level, level in value.items():
      
        level_breaks = "', '".join(level)
          
        tgb2_agg_der_minority_query = tgb2_agg_der_minority_query_part\
          .format(control=key,derived_level=new_level,levels=level_breaks,minority=minority,\
          minority_level=minority_level[index_m],table='tgb2_file_w')
        tgb2_agg_der_minority_part = spark.sql(tgb2_agg_der_minority_query).repartition(1)
        tgb2_agg_der_minority = tgb2_agg_der_minority.union(tgb2_agg_der_minority_part).repartition(1)

tgb2_agg_der_minority.createOrReplaceTempView('tgb2_agg_der_minority')
tgb2_agg_der_minority.cache()
cached_tables.append('tgb2_agg_der_minority')

# COMMAND ----------

tgb2_agg_der_minority.write.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/tgb2.agg.der.minority.parquet', mode='overwrite')
tgb2_agg_der_minority = spark.read.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/tgb2.agg.der.minority.parquet')
tgb2_agg_der_minority_pd = tgb2_agg_der_minority.toPandas()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Calculate Effective Sample Size for Minorities/Complements Demos

# COMMAND ----------

##################################################################################################
#####  Create query to compute effective sample size for Persons Minorities Complements      #####
#####  Compute effective sample size for Persons Minorities Derived Controls                 #####
#####  Convert Pyspark dataframe to Pandas dataframe                                         #####
##################################################################################################

# Create empty dataframe to save results
b1010_agg_minority=sqlContext.createDataFrame(sc.emptyRDD(),schema) 

# Create string containing the SQL query used in the loop 
b1010_agg_minority_query_part='''
SELECT 
    DMA_CODE,
    concat('PERSONS_','{minority_level}') AS control,
    '{control}' AS parameter,
    ROUND(mean(PESS),2) AS effective_sample_size
FROM
    (
    SELECT
        INTAB_DATE,
        DMA_CODE,
        {control} AS parameter,
        ((sum(weight)*sum(weight))/sum(weight*weight)) AS PESS
    FROM 
        {table} 
    WHERE
        1=1 AND
        {minority} = '{minority_level}'
    GROUP BY
        INTAB_DATE,dma_code,{control}
     ) 
WHERE 
     parameter='Y'
GROUP BY
     DMA_CODE,control,parameter
'''

# Create empty SQL query
b1010_agg_query_minority=''

# Loop over the dictionaries and consolidate query parts and append result to empty dataframe
for key in demo_controls['PERSONS']:

  for minority, minority_level in minority_dict.items():

    for index_m,i in enumerate(minority_level):
        
      b1010_agg_query_minority = b1010_agg_minority_query_part\
        .format(control=key,minority=minority,minority_level=i,table='b1010_file_w')
      b1010_agg_minority_part=spark.sql(b1010_agg_query_minority).repartition(1)
      b1010_agg_minority=b1010_agg_minority.union(b1010_agg_minority_part).repartition(1)
        
b1010_agg_minority.createOrReplaceTempView('b1010_agg_minority')
b1010_agg_minority.cache()
cached_tables.append('b1010_agg_minority')

# COMMAND ----------

b1010_agg_minority.write.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/b1010.agg.minority.parquet', mode='overwrite')
b1010_agg_minority = spark.read.parquet('s3a://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/b1010.agg.minority.parquet')
b1010_agg_minority_pd = b1010_agg_minority.toPandas()

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 5: Find Controls with Missing Levels, Create them with Effective Sample Size of 0 and Saving the Output to S3

# COMMAND ----------

# MAGIC %md
# MAGIC ### Convert to Pandas Dataframe

# COMMAND ----------

# tgb2_agg_pd = tgb2_agg.toPandas()
# tgb2_agg_minority_pd = tgb2_agg_minority.toPandas()
# tgb2_agg_der_pd = tgb2_agg_der.toPandas()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Functions to Add Missing Rows

# COMMAND ----------

###############################################################################################
##### Function to fill original or derived levels that are missing at total dma level
##### Function to fill original or derived levels that are missing at minority/complement level
################################################################################################

def examine_missing_row_dmas(date_input,controls_dic):
    """
    This function checks that each total DMA weighting control is present in a given dataframe. 
    If the weighting control is present in the dataframe, the function appends the row to an 
    empty dataframe with its effective size value. If the weighting control is not present, the 
    function appends row with 0 as effective sample size.
    """
    input_df_with_zeros=pd.DataFrame()

    for dma in date_input.dma_code.unique():

      for control in controls_dic.keys():

        reduced_df = date_input[(date_input.dma_code==dma) & (date_input.control==control)]

        for val in controls_dic[control]:  

          if val not in reduced_df.parameter.as_matrix():

            reduced_df = reduced_df.append(pd.DataFrame([[dma,control,val,0.0]],columns=reduced_df.columns))

        input_df_with_zeros = pd.concat([input_df_with_zeros,reduced_df],axis=0)

    return(input_df_with_zeros)


def examine_missing_row_minorities(date_input,controls_dic):
    """
    This function checks that each minority weighting control is present in a given dataframe. 
    If the weighting control is present in the dataframe, the function appends the row to an 
    empty dataframe with its effective size value. If the weighting control is not present, 
    the function appends row with 0 as effective sample size.
    """

    input_df_with_zeros=pd.DataFrame()

    for minority, minority_level in minority_dict.items():

      for index_m,i in enumerate(minority_level):

          for dma in date_input.dma_code.unique():

            for control in controls_dic.keys():

              reduced_df = date_input[(date_input.dma_code==dma) & (date_input.control==control+'_'+i)]

              for val in controls_dic[control]:

                if val not in reduced_df.parameter.as_matrix():

                   reduced_df = reduced_df.append(pd.DataFrame([[dma,control+'_'+i,val,0.0]],columns=reduced_df.columns))

              input_df_with_zeros = pd.concat([input_df_with_zeros,reduced_df],axis=0)

    return(input_df_with_zeros)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create Final Output Dataframe

# COMMAND ----------

#filling missing rows of tgb2_agg_pd for all total_dma_controls
tgb2_agg_pd_final = examine_missing_row_dmas(tgb2_agg_pd,total_dma_controls)

#filling missing rows of tgb2_agg_der_pd for all total_dma_derived_controls
tgb2_agg_der_pd_final = examine_missing_row_dmas(tgb2_agg_der_pd,total_dma_derived_controls)

#filling missing rows of tgb2_agg_minority_pd for all minority_controls
tgb2_agg_minority_pd_final = examine_missing_row_minorities(tgb2_agg_minority_pd,minority_controls)

#filling missing rows of tgb2_agg_der_minority_pd for all total_dma_derived_controls
tgb2_agg_der_minority_pd_final = examine_missing_row_minorities(tgb2_agg_der_minority_pd,total_dma_derived_controls)

#filling missing rows of b1010_agg_minority_pd for all demo_controls
b1010_agg_minority_pd_final = examine_missing_row_minorities(b1010_agg_minority_pd,demo_controls)

# Concatenate all 5 dataframes
final_output_module1_pd = pd.concat([tgb2_agg_pd_final,tgb2_agg_der_pd_final,\
                                     tgb2_agg_minority_pd_final,tgb2_agg_der_minority_pd_final,\
                                     b1010_agg_minority_pd_final],axis=0)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Convert Output to Spark

# COMMAND ----------

final_output_module1 = sqlContext.createDataFrame(final_output_module1_pd,schema)
final_output_module1.createOrReplaceTempView('final_output_module1')
# display(final_output_module1.orderBy('dma_code','control'))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Write Output to S3

# COMMAND ----------

final_output_module1.write.parquet(final_output_module1_dir, mode='overwrite')

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Clean Work Space

# COMMAND ----------

for table in cached_tables:
    uncache_tbl_sql = "UNCACHE TABLE {}".format(table)
    spark.sql(uncache_tbl_sql)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Notebook Caller Output

# COMMAND ----------

dbutils.notebook.exit("Output of Effective Sample Size Computation Module path: " + final_output_module1_dir)
