# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # UE COMPUTATION ENGINE 
# MAGIC ### Module # 4
# MAGIC  
# MAGIC #### Purpose:
# MAGIC 
# MAGIC > * This module's goal is to compute the UEs of the collapsed weighting collapsed controls.
# MAGIC 
# MAGIC **UNDERLYING ASSUMPTIONS:**
# MAGIC 
# MAGIC 1- For every row in the UE table whose rim is the same as the ones in the rim list, there is a row with rim "Total".    
# MAGIC 2- For every row in the UE table whose subrim is the same as the ones in the subrim list, there is a row with subrim "TV Household".  
# MAGIC 
# MAGIC **USER INPUTS:**      
# MAGIC 
# MAGIC > 1. start_date - Start Date
# MAGIC > 2. end_date - End Date
# MAGIC > 3. ue_table_measurement_period - UE table measurement period variable, as abbreviated month follwed by year: **monthyy**
# MAGIC > 4. geography_file_dir - Directory of new geography assignment csv file
# MAGIC > 5. missing_ue_output_dir - Directory in which the software saves weighting controls with no UEs as a parquet file. 
# MAGIC 
# MAGIC **LOW-LEVEL EXPLANATION OF WHAT NOTEBOOK DOES:**
# MAGIC >This software can be broken into 5 sub-modules.
# MAGIC > Rim is equivalent to market_break
# MAGIC > Subrim is equivalent to market_break_description
# MAGIC 
# MAGIC > **Part 1: Import UE Data**
# MAGIC 
# MAGIC >>* Import the collapsed controls (output of the Sufficient Sample Size Qualifier Engine) and creates a list of DMAs present in the collapsed controls dataframe.   
# MAGIC >>* Import UE data from the **dsci_tam.universe_estimates** MDL table. Following are the filters applied to the data:
# MAGIC     * measurement_period 
# MAGIC     * DMA using the DMA list created from the collapsed controls
# MAGIC     * ue_type equal to "H" or "P" to remove BBO homes
# MAGIC     * market break needs to be equal to "Total", "Black", "Hispanic", "Asian", or "Am. Native"
# MAGIC     * geography equals "DMA"
# MAGIC >>* Finally the table is filtered to keep records from the maximum load_id for the measurement priod selected.
# MAGIC >>* The final dataframe is saved as the **ue_table_mdl** dataframe. 
# MAGIC 
# MAGIC 
# MAGIC > **Part 2: Compute Rim and Subrims UE Complements**
# MAGIC >>* Create SQL queries which computes the rim/market_break and subrim/market_break_description UE complements ("Non-Black", "Non-Asian", "Cable No", "ADS No", etc).
# MAGIC >>* There are two computations:<br /><br />           
# MAGIC     1. **UE Rim complements:** 
# MAGIC       * Subtracting each DMA rim UE to its Total UE 
# MAGIC       * Renaming the complement rims, e.g. "Black" becomes "Non-Black" and save the dataframe as **ue_table_non_rim**.
# MAGIC       * Union **ue_table_mdl** with **ue_table_non_rim** into the **ue_table0** dataframe, which now contains the UE data from the MDL and the Rim complements.<br /><br />
# MAGIC     2. **UE Subrims complements:** 
# MAGIC       * Subtracting each DMA subrim UE to its Total-TV Household UE
# MAGIC       * Renaming the complement subrims, e.g. "ADS Yes" becomes "No ADS Yes" and save the dataframe as **ue_table_non_subrim**.
# MAGIC       * Union **ue_table0** with **ue_table_non_subrim** into the **ue_table1** dataframe, which now contains the UE data from the MDL and the Rim and Subrim complements.<br />  
# MAGIC     
# MAGIC >>* The last step is to rename the market_break and market_break_description in the **ue_table1** to match the **collapsed controls** rims and subrims. 
# MAGIC >>* The result of this sub-module is saved in the **ue_table_renamed** dataframe. 
# MAGIC 
# MAGIC 
# MAGIC > **Part 3: Aggregate Derived UEs**
# MAGIC >>* This sub-module computes the derived UEs not present in the MDL table, e.g. "M12-17" equals the sum of UE from the subrim "M12-14" "M15-17". 
# MAGIC >>* Create a Python dictionnary with the following elements:
# MAGIC     * **Keys:** Name of the missing subrim UE 
# MAGIC     * **Values:** Names of subrims UEs that constite the new subrim 
# MAGIC >>* The sub_modules creates a query that will compute the derived UEs for the "Total", "Black", "Non-Black", "Asian" and "Non-Asian" rims present in the dictionnary.
# MAGIC >>* The sub_modules then creates queries that will compute the derived UEs for the "P2-17" and "P12-17" subrims. The queries are separated between Total DMA vs. Minority.
# MAGIC >>* The result of this sub-module is saved in the **ue_table_agg** dataframe.
# MAGIC 
# MAGIC 
# MAGIC > **Part 4: Collect PPM Metro 1 & 2 and PPM Remainder UEs from MDL**
# MAGIC >>* This sub-module collect the PPM UEs from the MDL table, e.g. "PPM Metro 1". 
# MAGIC >>* Import UE data from the **dsci_tam.universe_estimates** MDL table. Following are the filters applied to the data:
# MAGIC     * measurement_period 
# MAGIC     * DMA using the DMA list created from the collapsed controls
# MAGIC     * ue_type equal to "H" or "P" to remove BBO homes
# MAGIC     * market break needs to be equal to "PPM Metro 1", "PPM Metro 2", "PPM Non-Metro"
# MAGIC     * market break description needs to be equal to "TV Household"
# MAGIC     * geography equals "DMA"
# MAGIC >>* Finally the table is filtered to keep records from the maximum load_id for the measurement priod selected.
# MAGIC >>* The final dataframe is saved as the **ue_table_ppm** dataframe. 
# MAGIC 
# MAGIC 
# MAGIC > **Part 5: Create Geography UEs**
# MAGIC 
# MAGIC > This sub-module utilize the **geography assignment** file.
# MAGIC 
# MAGIC >>* **EXAMPLE GEOGRAPHY ASSIGNEMENT FILE**
# MAGIC 
# MAGIC >> | column                    | data_type | example                                |
# MAGIC    |---------------------------|-----------|----------------------------------------|
# MAGIC    | county_group              | bigint    | 1                                      |
# MAGIC    | dma_code                  | bigint    | 501                                    |
# MAGIC    | control                   | string    | GEOGRAPHY or GEOGRAPHY_BLACK_RACE ...  |
# MAGIC    | county_code               | bigint    | 31059                                  |                           
# MAGIC >> <br />  
# MAGIC 
# MAGIC >>* Import the new_geography_assignment file and creates a list of counties present in the geography assignment dataframe.
# MAGIC >>* Import UE data from the **dsci_tam.universe_estimates** MDL table. Following are the filters applied to the data:
# MAGIC     * measurement_period 
# MAGIC     * Counties using the county list created from the geography assignment file
# MAGIC     * ue_type equal to "H" or "P" to remove BBO homes
# MAGIC     * market break needs to be equal to "Total", "Black", or "Hispanic"
# MAGIC     * market break description needs to be equal to "TV Household"
# MAGIC     * geography equals "LC" for Local County
# MAGIC >>* Finally the table is filtered to keep records from the maximum load_id for the measurement priod selected.
# MAGIC >>* The final dataframe is saved as the **ue_table_mdl_lc** dataframe. 
# MAGIC >>* Compute Complement Rim    
# MAGIC     * Subtracting each County rim UE to its Total UE 
# MAGIC     * Renaming the complement rims, e.g. "Black" becomes "Non-Black" and save the dataframe as **ue_table_mdl_lc_non**.
# MAGIC >>* Union **ue_table_mdl_lc** with **ue_table_mdl_lc_non** into the **ue_table_mdl_lc** dataframe.
# MAGIC >>* Create a dataframe from the geography assignment file with four columns: 
# MAGIC 
# MAGIC >> | dma_code | county_group | control  | counties            |
# MAGIC    |----------|--------------|----------|---------------------|
# MAGIC    | 501      | 1            | Black    | 31059, 31081, 31103 |
# MAGIC    | 501      | 1            | Hispanic | 31059, 31081        |
# MAGIC    | 501      | 2            | Total    | 21125               |
# MAGIC >> <br /> 
# MAGIC 
# MAGIC >>* Compute UEs for county group and control present in the geography assignment file and save the results in **ue_table_cg**. 
# MAGIC   
# MAGIC   
# MAGIC > **Part 6: Union UEs and Join UEs to Collapsed Weighting Controls**
# MAGIC >>* Union all the UE tables into the **ue_table_final**.
# MAGIC >>* Join the UE table to the collapsed controls.
# MAGIC >>* Save the weighting controls that do not have any UE into the **missing_ue_output_dir** (most likely LANGUAGE controls).
# MAGIC >>* Finally, save the weighting controls that have UEs to the final output directory. 
# MAGIC 
# MAGIC 
# MAGIC **EXAMPLE OF OUTPUT WEIGHTING CONTROL FILE**
# MAGIC >> | dma | weighting_control      | level                    | universe_estimate |
# MAGIC    |-----|------------------------|--------------------------|-------------------|
# MAGIC    | 501 | ADS                    | ADS_YES                  | 632560            |
# MAGIC    | 501 | ADS                    | ADS_NO                   | 6442190           |
# MAGIC    | 501 | ADS_NON-HISPANIC       | ADS_YES                  | 506560            |
# MAGIC    | 501 | AGE_OF_HEAD_BLACK_RACE | AGE_OF_HOUSEHOLDER_45-54 | 277360            |
# MAGIC    | 501 | ASIAN	              | ASIAN_RACE	             | 646560            |
# MAGIC    | 501 | ASIAN	              | NON-ASIAN_RACE           | 6428190           |
# MAGIC    
# MAGIC    
# MAGIC **EXAMPLE OF OUTPUT MISSING UEs FILE**   
# MAGIC >> | dma | weighting_control      | level                      | universe_estimate |
# MAGIC    |-----|------------------------|----------------------------|-------------------|
# MAGIC    | 506 |	LANGUAGE	          | ENGLISH_DOMINANT	       | null              |
# MAGIC    | 506 |	LANGUAGE	          | SPANISH_DOMINANT_BILINGUAL | null              |
# MAGIC    | 533 |	LANGUAGE	          | ENGLISH_DOMINANT	       | null              |
# MAGIC    | 533 |	LANGUAGE	          | SPANISH_DOMINANT_BILINGUAL | null              |
# MAGIC    | 548 |	LANGUAGE	          | ENGLISH_DOMINANT	       | null              |
# MAGIC    | 548 |	LANGUAGE	          | SPANISH_DOMINANT	       | null              |
# MAGIC 
# MAGIC **Authors:**    
# MAGIC - MJ PAKDEL
# MAGIC - JONATHAN OUEGNIN

# COMMAND ----------

# MAGIC %md
# MAGIC ### Input

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################

# start_date = '2018-03-01'
# end_date = '2018-03-02'
# ue_table_measurement_period = 'May18'
# geography_file_dir = "/FileStore/tables/historical_geography_assignment_example.csv"
# geography_file_dir = '/FileStore/tables/historical_geography_assignment2.csv'
# geography_file_dir = "s3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/historical_geography_assignment3_nongeo.csv"
# sample_type = 'LPM+PPM'

# COMMAND ----------

##########################################################
#####    USER INPUTS (called in another notebook)    #####
##########################################################

start_date                        = str(dbutils.widgets.get('start_date'))
end_date                          = str(dbutils.widgets.get('end_date'))
ue_table_measurement_period       = str(dbutils.widgets.get('ue_table_measurement_period'))
geography_file_dir                = str(dbutils.widgets.get('geography_file_dir'))
sample_type                       = str(dbutils.widgets.get('sample_type'))
missing_ue_output_dir             = str(dbutils.widgets.get('missing_ue_output_dir'))

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Validate Sample Type User Input

# COMMAND ----------

sample_type = sample_type.upper()

if (sample_type != "LPM+PPM") and (sample_type != "SET_METER+PPM"):
    dbutils.notebook.exit("Invalid Sample Type Input")

# COMMAND ----------

# MAGIC %md
# MAGIC ### Import Libraries and Functions

# COMMAND ----------

from pyspark.sql.functions import collect_list, concat_ws, concat, lit, when, regexp_replace

# COMMAND ----------

# Read output of Sufficient Sample Size Qualifier Engine module
collapsed_controls_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/collapsed_controls_' + start_date.replace('-','_') + '_' + end_date.replace('-','_') + '_' + sample_type.replace('+','_') + '.parquet'

# Create path to save final output
final_output_module4_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/weighting_controls_' + start_date.replace('-','_') + '_' + end_date.replace('-','_') + '_' + sample_type.replace('+','_') + '.parquet'

cached_tables = []

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create list of controls and levels to compute

# COMMAND ----------

# List of dichotomous market breaks 
market_breaks_to_compute = ['Black', 'Hispanic', 'Am. Native', 'Asian']

# List of dichotomous market break descriptions
market_breaks_description_to_compute = ['ADS Yes', 'Cable Yes', 'DVR Yes', 'Number of Operable Sets 2+']

# COMMAND ----------

# MAGIC %md # Part 1: Import Data

# COMMAND ----------

# MAGIC %md
# MAGIC ### Import Weighting Controls

# COMMAND ----------

###############################################################################
##### Import weighting controls outputted by module 3
##### Create a dma_list of the DMAs present in the weighting controls
###############################################################################

collapsed_controls = sqlContext.read.parquet(collapsed_controls_dir)
collapsed_controls.createOrReplaceTempView('collapsed_controls')
collapsed_controls.cache()

cached_tables.append('collapsed_controls')

# Create DMA list
dma_list = [str(x.dma) for x in collapsed_controls.select('dma').distinct().collect()]

# COMMAND ----------

# MAGIC %md
# MAGIC ### Import UEs

# COMMAND ----------

#########################################################################################################################
##### Pull up UEs from the dsci_tam.universe_estimates table for Total, Black Hispanic and Am. Native market break ###### 
##### Keep only UEs for the households without BBO and the measurement_period and DMAs of interest                 ######          
##### Keep only UEs with the highest load_id.                                                                      ######
#########################################################################################################################

s = """
SELECT 
    geography_code,market_break,
    market_break_description,
    universe_estimate,load_id
FROM
     dsci_tam.universe_estimates 
WHERE 1=1
    AND measurement_period='{date}' 
    AND (ue_type='H' or ue_type='P')
    AND market_break IN ('Total', 'Black', 'Hispanic', 'Asian', 'Am. Native') 
    AND geography = 'DMA'
    AND geography_code IN ({dmas}) 
""".format(date=ue_table_measurement_period, dmas=", ".join(dma_list))

ue_table_mdl = spark.sql(s)

# Filter the result to the MAX load_id present in the table, which represents the latest data available
max_load_id = max([x.load_id for x in ue_table_mdl.select('load_id').distinct().collect()])
ue_table_mdl = ue_table_mdl.filter(ue_table_mdl.load_id == max_load_id).drop('load_id')

ue_table_mdl.createOrReplaceTempView('ue_table_mdl')
ue_table_mdl.cache()#.count()

cached_tables.append('ue_table_mdl')

# COMMAND ----------

# MAGIC %md # Part 2: Compute Rim and Subrims UE Complements

# COMMAND ----------

# MAGIC %md
# MAGIC ### Rims Computation

# COMMAND ----------

############################################
#### SQL Query to compute opposite Rims ####
############################################

s = """
SELECT
    A.geography_code,
    CONCAT('Non-', A.market_break) AS market_break,
    A.market_break_description,
    B.universe_estimate - A.universe_estimate AS universe_estimate  -- Computation
FROM
    ue_table_mdl A
JOIN
    (SELECT 
         * 
     FROM 
         ue_table_mdl 
     WHERE 1=1 
         AND market_break = 'Total') B
ON
    A.geography_code = B.geography_code
    AND A.market_break_description = B.market_break_description
WHERE 1=1
    AND A.market_break IN ('{rims}') 
""".format(rims="' ,'".join(market_breaks_to_compute))

ue_table_non_rim = spark.sql(s).coalesce(1)

ue_table_non_rim.createOrReplaceTempView('ue_table_non_rim')
ue_table_non_rim.cache()

cached_tables.append('ue_table_non_rim')

# COMMAND ----------

ue_table0 = ue_table_mdl.union(ue_table_non_rim)
ue_table0.createOrReplaceTempView('ue_table0')
ue_table0.cache()

cached_tables.append('ue_table0')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Subrims Computation

# COMMAND ----------

###############################################
#### SQL Query to compute opposite Subrims ####
###############################################

s = """
SELECT
    A.geography_code,
    A.market_break,
    CONCAT('No ', A.market_break_description) AS market_break_description,
    B.universe_estimate - A.universe_estimate AS universe_estimate
FROM
    ue_table0 A
JOIN
    (SELECT 
         * 
     FROM 
         ue_table0 
     WHERE 1=1 
         AND market_break_description = 'TV Household') B
ON
    A.geography_code = B.geography_code
    AND A.market_break = B.market_break
WHERE 1=1
    AND A.market_break_description IN ('{subrims}')
""".format(subrims="', '".join(market_breaks_description_to_compute))

ue_table_non_subrim = spark.sql(s)

ue_table_non_subrim.createOrReplaceTempView('ue_table_non_subrim')
ue_table_non_subrim.cache()#.count()

cached_tables.append('ue_table_non_subrim')

# COMMAND ----------

ue_table1 = ue_table0.union(ue_table_non_subrim).coalesce(1)
ue_table1.createOrReplaceTempView('ue_table1')
ue_table1.cache()

cached_tables.append('ue_table1')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Rename the UE Table Rim and Subrim

# COMMAND ----------

#######################################################
#### Rename rim and subrim in the UE table created ####
#######################################################

s = '''
  SELECT 
      geography_code,
      
      -------------------------------Market Break -------------------------------
      
      CASE 
        -------------------------------ASIAN-------------------------------
      
        WHEN market_break_description = 'TV Household' AND (market_break = 'Asian' OR market_break = 'Non-Asian') THEN 'ASIAN' 
      
        -------------------------------AMERICAN NATIVE-------------------------------
       
        WHEN market_break_description = 'TV Household' AND (market_break = 'Am. Native' OR market_break = 'Non-Am. Native') THEN 'AIAN' 
      
        -------------------------------BLACK-------------------------------
        
        WHEN market_break_description = 'TV Household' AND (market_break = 'Black' OR market_break = 'Non-Black') THEN 'BLACK' 
        
        -------------------------------ORIGIN-------------------------------
      
        WHEN market_break_description = 'TV Household' AND (market_break = 'Hispanic' OR market_break = 'Non-Hispanic') THEN 'ORIGIN'  
      
        -------------------------------PRESENCE OF NON-ADULTS-------------------------------
        WHEN market_break = 'Total' AND 
             (market_break_description = 'No Child <18' OR market_break_description = 'Child Only <12' OR
             market_break_description = 'Child Any 12-17' OR market_break_description = 'Child Only 12-17')
        THEN 'PRESENCE_OF_NONADULTS'
        -------------------------------LANGUAGE-------------------------------
        WHEN market_break = 'Total' AND 
             (market_break_description = 'Mostly English' OR market_break_description = 'Mostly Spanish' OR
             market_break_description = 'Only English' OR market_break_description = 'Only Spanish' OR
             market_break_description = 'Spanish and English' OR market_break_description = 'Non Hispanic') 
        THEN 'LANGUAGE'
        -------------------------------MEASURABLE SETS-------------------------------
        WHEN market_break = 'Total' AND 
             (market_break_description = 'Measured Sets 1' OR market_break_description = 'Measured Sets 2' OR
             market_break_description = 'Measured Sets 3' OR market_break_description = 'Measured Sets 4+')
        THEN 'MEASURABLE_SETS'
        -------------------------------OPERABLE SETS-------------------------------
        WHEN market_break = 'Total' AND 
             (market_break_description = 'Number of Operable Sets 2+' OR market_break_description = 'No Number of Operable Sets 2+')
        THEN 'OPERABLE_SETS'
        -------------------------------DVR-------------------------------
        WHEN market_break = 'Total' AND 
             (market_break_description = 'DVR Yes' OR market_break_description = 'No DVR Yes')
        THEN 'DVR'
        -------------------------------AGE OF HEAD-------------------------------
        WHEN market_break = 'Black' AND 
             (market_break_description = 'AOH <25' OR market_break_description = 'AOH 25-34' OR 
             market_break_description = 'AOH 35-44' OR market_break_description = 'AOH 45-54' OR 
             market_break_description = 'AOH 55-64' OR market_break_description = 'AOH 65+')
        THEN 'AGE_OF_HEAD_BLACK_RACE'
        WHEN market_break = 'Non-Black' AND 
             (market_break_description = 'AOH <25' OR market_break_description = 'AOH 25-34' OR 
             market_break_description = 'AOH 35-44' OR market_break_description = 'AOH 45-54' OR 
             market_break_description = 'AOH 55-64' OR market_break_description = 'AOH 65+')
        THEN 'AGE_OF_HEAD_NON-BLACK_RACE'
        WHEN market_break = 'Hispanic' AND 
             (market_break_description = 'AOH <25' OR market_break_description = 'AOH 25-34' OR 
             market_break_description = 'AOH 35-44' OR market_break_description = 'AOH 45-54' OR 
             market_break_description = 'AOH 55-64' OR market_break_description = 'AOH 65+')
        THEN 'AGE_OF_HEAD_HISPANIC'
        WHEN market_break = 'Non-Hispanic' AND 
             (market_break_description = 'AOH <25' OR market_break_description = 'AOH 25-34' OR 
             market_break_description = 'AOH 35-44' OR market_break_description = 'AOH 45-54' OR 
             market_break_description = 'AOH 55-64' OR market_break_description = 'AOH 65+')
        THEN 'AGE_OF_HEAD_NON-HISPANIC'
        WHEN market_break = 'Total' AND 
             (market_break_description = 'AOH <25' OR market_break_description = 'AOH 25-34' OR 
             market_break_description = 'AOH 35-44' OR market_break_description = 'AOH 45-54' OR 
             market_break_description = 'AOH 55-64' OR market_break_description = 'AOH 65+')
        THEN 'AGE_OF_HEAD'
        -------------------------------HOUSEHOLD SIZE-------------------------------
        WHEN market_break = 'Black' AND
             (market_break_description = 'HH Size 1' OR market_break_description = 'HH Size 2' OR 
             market_break_description = 'HH Size 3' OR market_break_description = 'HH Size 4' OR
             market_break_description = 'HH Size 5' OR market_break_description = 'HH Size 6+')
        THEN 'HOUSEHOLD_SIZE_BLACK_RACE'
        WHEN market_break = 'Non-Black' AND 
             (market_break_description = 'HH Size 1' OR market_break_description = 'HH Size 2' OR
             market_break_description = 'HH Size 3' OR market_break_description = 'HH Size 4' OR
             market_break_description = 'HH Size 5' OR market_break_description = 'HH Size 6+')
        THEN 'HOUSEHOLD_SIZE_NON-BLACK_RACE'
        WHEN market_break = 'Hispanic' AND 
             (market_break_description = 'HH Size 1' OR market_break_description = 'HH Size 2' OR
             market_break_description = 'HH Size 3' OR market_break_description = 'HH Size 4' OR
             market_break_description = 'HH Size 5' OR market_break_description = 'HH Size 6+')
        THEN 'HOUSEHOLD_SIZE_HISPANIC'
        WHEN market_break = 'Non-Hispanic' AND 
             (market_break_description = 'HH Size 1' OR market_break_description = 'HH Size 2' OR
             market_break_description = 'HH Size 3' OR market_break_description = 'HH Size 4' OR
             market_break_description = 'HH Size 5' OR market_break_description = 'HH Size 6+')
        THEN 'HOUSEHOLD_SIZE_NON-HISPANIC'
        WHEN market_break = 'Total' AND 
             (market_break_description = 'HH Size 1' OR market_break_description = 'HH Size 2' OR
             market_break_description = 'HH Size 3' OR market_break_description = 'HH Size 4' OR
             market_break_description = 'HH Size 5' OR market_break_description = 'HH Size 6+')
        THEN 'HOUSEHOLD_SIZE'
        -------------------------------CABLE PLUS-------------------------------
        WHEN market_break = 'Black' AND 
             (market_break_description = 'OTA' OR market_break_description = 'Cable and/or ADS Yes' OR
             market_break_description = 'Cable Yes' OR market_break_description = 'No Cable Yes') 
        THEN 'CABLE_PLUS_BLACK_RACE'
        WHEN market_break = 'Non-Black' AND
             (market_break_description = 'OTA' OR market_break_description = 'Cable and/or ADS Yes' OR
             market_break_description = 'Cable Yes' OR market_break_description = 'No Cable Yes') 
        THEN 'CABLE_PLUS_NON-BLACK_RACE'
        WHEN market_break = 'Hispanic' AND 
             (market_break_description = 'OTA' OR market_break_description = 'Cable and/or ADS Yes' OR
             market_break_description = 'Cable Yes' OR market_break_description = 'No Cable Yes')  
        THEN 'CABLE_PLUS_HISPANIC'
        WHEN market_break = 'Non-Hispanic' AND 
             (market_break_description = 'OTA' OR market_break_description = 'Cable and/or ADS Yes' OR
             market_break_description = 'Cable Yes' OR market_break_description = 'No Cable Yes')   
        THEN 'CABLE_PLUS_NON-HISPANIC'
        WHEN market_break = 'Total' AND
             (market_break_description = 'OTA' OR market_break_description = 'Cable and/or ADS Yes' OR
             market_break_description = 'Cable Yes' OR market_break_description = 'No Cable Yes') 
        THEN 'CABLE_PLUS'
        -------------------------------ADS-------------------------------
        WHEN market_break = 'Black' AND
             (market_break_description = 'ADS Yes' OR market_break_description = 'No ADS Yes')
        THEN 'ADS_BLACK_RACE'
        WHEN market_break = 'Non-Black' AND
             (market_break_description = 'ADS Yes' OR market_break_description = 'No ADS Yes') 
        THEN 'ADS_NON-BLACK_RACE'
        WHEN market_break = 'Hispanic' AND
             (market_break_description = 'ADS Yes' OR market_break_description = 'No ADS Yes')
        THEN 'ADS_HISPANIC'
        WHEN market_break = 'Non-Hispanic' AND
             (market_break_description = 'ADS Yes' OR market_break_description = 'No ADS Yes')
        THEN 'ADS_NON-HISPANIC'
        WHEN market_break = 'Total' AND
             (market_break_description = 'ADS Yes' OR market_break_description = 'No ADS Yes')
        THEN 'ADS'
        -------------------------------PERSONS-------------------------------  
        WHEN market_break = 'Black' AND 
             (market_break_description = 'Children 2-5' OR market_break_description = 'Children 6-11' OR 
             market_break_description = 'Women 12-14' OR market_break_description = 'Women 15-17' OR
             market_break_description = 'Women 18-20' OR market_break_description = 'Women 21-24' OR
             market_break_description = 'Women 25-34' OR market_break_description = 'Women 35-44' OR
             market_break_description = 'Women 45-49' OR market_break_description = 'Women 50-54' OR
             market_break_description = 'Women 55-64' OR market_break_description = 'Women 65+' OR
             market_break_description = 'Men 12-14' OR market_break_description = 'Men 15-17' OR
             market_break_description = 'Men 18-20' OR market_break_description = 'Men 21-24' OR
             market_break_description = 'Men 25-34' OR market_break_description = 'Men 35-44' OR
             market_break_description = 'Men 45-49' OR market_break_description = 'Men 50-54' OR
             market_break_description = 'Men 55-64' OR market_break_description = 'Men 65+') 
        THEN 'PERSONS_BLACK_RACE'
        WHEN market_break = 'Non-Black' AND 
             (market_break_description = 'Children 2-5' OR market_break_description = 'Children 6-11' OR 
             market_break_description = 'Women 12-14' OR market_break_description = 'Women 15-17' OR
             market_break_description = 'Women 18-20' OR market_break_description = 'Women 21-24' OR
             market_break_description = 'Women 25-34' OR market_break_description = 'Women 35-44' OR
             market_break_description = 'Women 45-49' OR market_break_description = 'Women 50-54' OR
             market_break_description = 'Women 55-64' OR market_break_description = 'Women 65+' OR
             market_break_description = 'Men 12-14' OR market_break_description = 'Men 15-17' OR
             market_break_description = 'Men 18-20' OR market_break_description = 'Men 21-24' OR
             market_break_description = 'Men 25-34' OR market_break_description = 'Men 35-44' OR
             market_break_description = 'Men 45-49' OR market_break_description = 'Men 50-54' OR
             market_break_description = 'Men 55-64' OR market_break_description = 'Men 65+') 
        THEN 'PERSONS_NON-BLACK_RACE'
         WHEN market_break = 'Hispanic' AND 
             (market_break_description = 'Children 2-5' OR market_break_description = 'Children 6-11' OR 
             market_break_description = 'Women 12-14' OR market_break_description = 'Women 15-17' OR
             market_break_description = 'Women 18-20' OR market_break_description = 'Women 21-24' OR
             market_break_description = 'Women 25-34' OR market_break_description = 'Women 35-44' OR
             market_break_description = 'Women 45-49' OR market_break_description = 'Women 50-54' OR
             market_break_description = 'Women 55-64' OR market_break_description = 'Women 65+' OR
             market_break_description = 'Men 12-14' OR market_break_description = 'Men 15-17' OR
             market_break_description = 'Men 18-20' OR market_break_description = 'Men 21-24' OR
             market_break_description = 'Men 25-34' OR market_break_description = 'Men 35-44' OR
             market_break_description = 'Men 45-49' OR market_break_description = 'Men 50-54' OR
             market_break_description = 'Men 55-64' OR market_break_description = 'Men 65+') 
        THEN 'PERSONS_HISPANIC'
        WHEN market_break = 'Non-Hispanic' AND 
             (market_break_description = 'Children 2-5' OR market_break_description = 'Children 6-11' OR 
             market_break_description = 'Women 12-14' OR market_break_description = 'Women 15-17' OR
             market_break_description = 'Women 18-20' OR market_break_description = 'Women 21-24' OR
             market_break_description = 'Women 25-34' OR market_break_description = 'Women 35-44' OR
             market_break_description = 'Women 45-49' OR market_break_description = 'Women 50-54' OR
             market_break_description = 'Women 55-64' OR market_break_description = 'Women 65+' OR
             market_break_description = 'Men 12-14' OR market_break_description = 'Men 15-17' OR
             market_break_description = 'Men 18-20' OR market_break_description = 'Men 21-24' OR
             market_break_description = 'Men 25-34' OR market_break_description = 'Men 35-44' OR
             market_break_description = 'Men 45-49' OR market_break_description = 'Men 50-54' OR
             market_break_description = 'Men 55-64' OR market_break_description = 'Men 65+') 
        THEN 'PERSONS_NON-HISPANIC'
        WHEN market_break = 'Total' AND 
             (market_break_description = 'Children 2-5' OR market_break_description = 'Children 6-11') 
        THEN 'PERSONS'
        WHEN market_break = 'Total' AND 
             (market_break_description = 'Women 12-14' OR market_break_description = 'Women 15-17' OR
             market_break_description = 'Women 18-20' OR market_break_description = 'Women 21-24' OR
             market_break_description = 'Women 25-34' OR market_break_description = 'Women 35-44' OR
             market_break_description = 'Women 45-49' OR market_break_description = 'Women 50-54' OR
             market_break_description = 'Women 55-64' OR market_break_description = 'Women 65+') 
        THEN 'PERSONS'
        WHEN market_break = 'Total' AND 
             (market_break_description = 'Men 12-14' OR market_break_description = 'Men 15-17' OR
             market_break_description = 'Men 18-20' OR market_break_description = 'Men 21-24' OR
             market_break_description = 'Men 25-34' OR market_break_description = 'Men 35-44' OR
             market_break_description = 'Men 45-49' OR market_break_description = 'Men 50-54' OR
             market_break_description = 'Men 55-64' OR market_break_description = 'Men 65+') 
        THEN 'PERSONS'
        
        --ELSE '_DELETE_'
        ELSE market_break
        
      END AS market_break,
      
      -------------------------------Market Break Description------------------------------- 
      
      CASE 
        WHEN market_break_description = 'HH Size 1' THEN '1_PERSON'
        WHEN market_break_description = 'HH Size 2' THEN '2_PERSONS'
        WHEN market_break_description = 'HH Size 3' THEN '3_PERSONS'
        WHEN market_break_description = 'HH Size 4' THEN '4_PERSONS'
        WHEN market_break_description = 'HH Size 5' THEN '5_PERSONS'
        WHEN market_break_description = 'HH Size 6+' THEN '6+_PERSONS'
        
        WHEN market_break_description = 'AOH <25' THEN 'AGE_OF_HOUSEHOLDER_<25'
        WHEN market_break_description = 'AOH 25-34' THEN 'AGE_OF_HOUSEHOLDER_25-34'
        WHEN market_break_description = 'AOH 35-44' THEN 'AGE_OF_HOUSEHOLDER_35-44'
        WHEN market_break_description = 'AOH 45-54' THEN 'AGE_OF_HOUSEHOLDER_45-54'
        WHEN market_break_description = 'AOH 55-64' THEN 'AGE_OF_HOUSEHOLDER_55-64'
        WHEN market_break_description = 'AOH 65+' THEN 'AGE_OF_HOUSEHOLDER_65+'
        
        WHEN market_break_description = 'Hispanic' THEN 'HISPANIC'
        WHEN market_break_description = 'Non Hispanic' THEN 'NON-HISPANIC'
        
        WHEN market_break_description = 'Cable Yes' THEN 'CABLE_YES'
        WHEN market_break_description = 'No Cable Yes' THEN 'CABLE_NO'
        WHEN market_break_description = 'OTA' THEN 'BROADCAST_ONLY'
        WHEN market_break_description = 'Cable and/or ADS Yes' THEN 'CABLE_PLUS_YES'

        WHEN market_break_description = 'DVR Yes' THEN 'DVR_YES'
        WHEN market_break_description = 'No DVR Yes' THEN 'DVR_NO'
        
        WHEN market_break_description = 'ADS Yes' THEN 'ADS_YES'
        WHEN market_break_description = 'No ADS Yes' THEN 'ADS_NO'        
        
        WHEN market_break_description = 'Children 2-5' THEN 'C2_5'
        WHEN market_break_description = 'Children 6-11' THEN 'C6_11'
        WHEN market_break_description = 'Women 12-14' THEN 'F12_14'
        WHEN market_break_description = 'Women 15-17' THEN 'F15_17'
        WHEN market_break_description = 'Women 18-20' THEN 'F18_20'
        WHEN market_break_description = 'Women 21-24' THEN 'F21_24'
        WHEN market_break_description = 'Women 25-34' THEN 'F25_34'
        WHEN market_break_description = 'Women 35-44' THEN 'F35_44'
        WHEN market_break_description = 'Women 45-49' THEN 'F45_49'
        WHEN market_break_description = 'Women 50-54' THEN 'F50_54'
        WHEN market_break_description = 'Women 55-64' THEN 'F55_64'
        WHEN market_break_description = 'Women 65+' THEN 'F65_999'
        WHEN market_break_description = 'Men 12-14' THEN 'M12_14'
        WHEN market_break_description = 'Men 15-17' THEN 'M15_17'
        WHEN market_break_description = 'Men 18-20' THEN 'M18_20'
        WHEN market_break_description = 'Men 21-24' THEN 'M21_24'
        WHEN market_break_description = 'Men 25-34' THEN 'M25_34'
        WHEN market_break_description = 'Men 35-44' THEN 'M35_44'
        WHEN market_break_description = 'Men 45-49' THEN 'M45_49'
        WHEN market_break_description = 'Men 50-54' THEN 'M50_54'
        WHEN market_break_description = 'Men 55-64' THEN 'M55_64'
        WHEN market_break_description = 'Men 65+' THEN 'M65_999'
        
        WHEN market_break_description = 'Measured Sets 1' THEN '1_SET'
        WHEN market_break_description = 'Measured Sets 2' THEN '2_SETS'
        WHEN market_break_description = 'Measured Sets 3' THEN '3_SETS'
        WHEN market_break_description = 'Measured Sets 4+' THEN '4+_SETS'
        
        WHEN market_break_description = 'Number of Operable Sets 2+' THEN '2+_SETS'
        WHEN market_break_description = 'No Number of Operable Sets 2+' THEN '1_SET'
        
        WHEN market_break_description = 'Non Hispanic' THEN 'NON-HISPANIC'
        WHEN market_break_description = 'Mostly English' THEN 'MOSTLY_ENGLISH'
        WHEN market_break_description = 'Mostly Spanish' THEN 'MOSTLY_SPANISH'
        WHEN market_break_description = 'Only English' THEN 'ENGLISH_ONLY'
        WHEN market_break_description = 'Only Spanish' THEN 'SPANISH_ONLY'
        WHEN market_break_description = 'Spanish and English' THEN 'SPANISH_ENGLISH_EQUAL'  
        
        WHEN market_break_description = 'No Child <18' THEN 'PRESENCE_OF_NONADULTS_NONE_<18'
        WHEN market_break_description = 'Child Only <12' THEN 'PRESENCE_OF_NONADULTS_ONLY_<12'
        WHEN market_break_description = 'Child Any 12-17' THEN 'PRESENCE_OF_NONADULTS_ANY_12-17'
        --WHEN market_break_description = 'Child Only 12-17' THEN 'PRESENCE_OF_NONADULTS_ANY_12-17'
        
        WHEN market_break_description = 'TV Household' AND market_break = 'Asian' THEN 'ASIAN_RACE'
        WHEN market_break_description = 'TV Household' AND market_break = 'Non-Asian' THEN 'NON-ASIAN_RACE'
        
        WHEN market_break_description = 'TV Household' AND market_break = 'Am. Native' THEN 'AIAN_RACE'
        WHEN market_break_description = 'TV Household' AND market_break = 'Non-Am. Native' THEN 'NON-AIAN_RACE'
        
        WHEN market_break_description = 'TV Household' AND market_break = 'Black' THEN 'BLACK_RACE'
        WHEN market_break_description = 'TV Household' AND market_break = 'Non-Black' THEN 'NON-BLACK_RACE'
        
        WHEN market_break_description = 'TV Household' AND market_break = 'Hispanic' THEN 'HISPANIC'
        WHEN market_break_description = 'TV Household' AND market_break = 'Non-Hispanic' THEN 'NON-HISPANIC'
        
        ELSE market_break_description
        
      END as market_break_description,  
      
      universe_estimate
  FROM 
      ue_table1
      
  UNION ALL
  
  SELECT 
      geography_code,
      'LANGUAGE' AS market_break,
      'HISPANIC' AS market_break_description,
      universe_estimate
  FROM
      ue_table1
  WHERE 1=1
      AND market_break = 'Hispanic'
      AND market_break_description = 'TV Household'
      
  '''

ue_table_renamed = spark.sql(s)
ue_table_renamed = ue_table_renamed.groupBy('geography_code', 'market_break', 'market_break_description')\
                                   .sum('universe_estimate').withColumnRenamed('sum(universe_estimate)','universe_estimate').coalesce(1)
ue_table_renamed.createOrReplaceTempView('ue_table_renamed')
ue_table_renamed.cache()

cached_tables.append('ue_table_renamed')

# COMMAND ----------

# MAGIC %md # Part 3: Aggregate Derived UEs

# COMMAND ----------

# MAGIC %md
# MAGIC ### Dictionary of Variables to Aggregate

# COMMAND ----------

#####################################################################################################
##### Create a Python dictionnary with the name of the missing subrim UE as the dictionnary keys, 
##### and the  names of the subrims UEs that constite the new subrim as the dictionnary values.
#####################################################################################################

ue_to_agg = {
        'AGE_OF_HOUSEHOLDER_<35': ['AGE_OF_HOUSEHOLDER_<25','AGE_OF_HOUSEHOLDER_25-34'],
        'AGE_OF_HOUSEHOLDER_35+': ['AGE_OF_HOUSEHOLDER_35-44','AGE_OF_HOUSEHOLDER_45-54','AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+'],
        'AGE_OF_HOUSEHOLDER_<55': ['AGE_OF_HOUSEHOLDER_<25', 'AGE_OF_HOUSEHOLDER_25-34','AGE_OF_HOUSEHOLDER_35-44','AGE_OF_HOUSEHOLDER_45-54'],
        'AGE_OF_HOUSEHOLDER_55+': ['AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+'],
        'AGE_OF_HOUSEHOLDER_35-54': ['AGE_OF_HOUSEHOLDER_35-44','AGE_OF_HOUSEHOLDER_45-54'],
  
        '1-2_PERSONS': ['1_PERSON','2_PERSONS'],
        '3-4_PERSONS': ['3_PERSONS', '4_PERSONS'],
        '3+_PERSONS': ['3_PERSONS', '4_PERSONS', '5_PERSONS', '6+_PERSONS'],
        '5+_PERSONS': ['5_PERSONS', '6+_PERSONS'],
          
        'PRESENCE_OF_NONADULTS_ANY_CHILD': ['PRESENCE_OF_NONADULTS_ONLY_<12','PRESENCE_OF_NONADULTS_ANY_12-17'],
        
        '2+_SETS': ['2_SETS','3_SETS','4+_SETS'],
  
        'SPANISH_DOMINANT': ['SPANISH_ONLY','MOSTLY_SPANISH'],
        'ENGLISH_DOMINANT': ['ENGLISH_ONLY','MOSTLY_ENGLISH'],
        'ENGLISH_DOMINANT_BILINGUAL': ['MOSTLY_ENGLISH','SPANISH_ENGLISH_EQUAL'],
        'SPANISH_DOMINANT_BILINGUAL': ['MOSTLY_SPANISH','SPANISH_ENGLISH_EQUAL'],
  
  
        # needed for minorities as well
        'C2_11': ['C2_5','C6_11'],
        'M12_17': ['M12_14','M15_17'],
        'M18_24': ['M18_20','M21_24'],
        'M35_49': ['M35_44','M45_49'],
        'M18_34': ['M18_20','M21_24','M25_34'],
        'M18_49': ['M12_14','M15_17','M18_20','M21_24','M25_34','M35_44','M45_49'],
        'M50_999': ['M50_54','M55_64','M65_999'],
        'M50_64': ['M50_54','M55_64'],
        'M18_999': ['M18_20','M21_24','M25_34','M35_44','M45_49','M50_54','M55_64','M65_999'],
        'F12_17': ['F12_14','F15_17'],
        'F18_24': ['F18_20','F21_24'],
        'F35_49': ['F35_44','F45_49'],
        'F18_34': ['F18_20','F21_24','F25_34'],
        'F18_49': ['F12_14','F15_17','F18_20','FF21_24','F25_34','F35_44','F45_49'],
        'F50_999': ['F50_54','F55_64','F65_999'],
        'F50_64': ['F50_54','F55_64'],
        'F18_999': ['F18_20','F21_24','F25_34','F35_44','F45_49','F50_54','F55_64','F65_999']
      }

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create Query for Derived UEs Aggregation

# COMMAND ----------

#####################################################################################################
##### The Next three cells are creating a query used to compute Derived UEs.
##### The computation is done at the Total DMA level and at the Minority level.
#####################################################################################################

# Query to compute derived UEs at Total DMA level
ue_table_agg_query_part = """
  SELECT 
      geography_code, market_break, 
      '{market_break_description}' as market_break_description, 
      sum(universe_estimate) as universe_estimate
  FROM
      {table}
  WHERE 1=1 
      AND market_break_description IN ('{all_levels}')
  GROUP BY 
      geography_code, market_break

  UNION ALL
"""

ue_table_agg_query = ''

for new_market_break_description, level in ue_to_agg.items():
  
    ue_table_agg_query += ue_table_agg_query_part.format(market_break_description=new_market_break_description,all_levels="', '".join(level),table='ue_table_renamed')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create Query for P2-17 & P12-17 in Total DMA

# COMMAND ----------

# Query to compute P2-17 & P12-17 UEs at Total DMA level
ue_to_agg_per = {
  'P2_17': ['C2_5','C6_11','F12_14','F15_17','M12_14','M15_17'],
  'P12_17': ['F12_14','F15_17','M12_14','M15_17']
}

sql_tot_part="""
    SELECT
        geography_code, 
        'PERSONS' AS market_break,'{m_break}' AS market_break_description,
        SUM(universe_estimate) AS universe_estimates
    FROM 
        ue_table_renamed 
    WHERE 1=1 
        AND market_break_description IN ('{break_desc}') 
        AND market_break IN ('PERSONS')
    GROUP BY 
        geography_code
      
    UNION ALL 
  """

for market_break,market_break_description in ue_to_agg_per.items():
  
  ue_table_agg_query += sql_tot_part.format(m_break=market_break,break_desc="', '".join(market_break_description))
  
# print(ue_table_agg_query)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create to Query for P2-17 & P12-17 in Minorities

# COMMAND ----------

market_break_list_min = ['PERSONS_BLACK_RACE','PERSONS_NON-BLACK_RACE','PERSONS_HISPANIC','PERSONS_NON-HISPANIC'] 

# Query to compute P2-17 & P12-17 UEs at the Minority level
sql_min_part = """
    SELECT
      geography_code, 
      market_break,'{m_break}' AS market_break_description,
      SUM(universe_estimate) AS universe_estimate
    FROM 
      ue_table_renamed 
    WHERE 1=1 
      AND market_break_description in ('{break_desc}') 
      AND market_break = '{minority_br}'
    GROUP BY 
      geography_code, market_break

    UNION ALL 
    """

for minority in market_break_list_min:
    for market_break,market_break_description in ue_to_agg_per.items():
        ue_table_agg_query += sql_min_part.format(m_break=market_break,break_desc="', '".join(market_break_description),
                                                  minority_br=minority)
    
ue_table_agg_query = ue_table_agg_query.rstrip('UNION ALL\n ')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Run Aggregation Query

# COMMAND ----------

ue_table_agg = spark.sql(ue_table_agg_query).coalesce(1)
ue_table_agg.createOrReplaceTempView('ue_table_agg')
ue_table_agg.cache().count()

cached_tables.append('ue_table_agg')

# COMMAND ----------

# MAGIC %md # Part 4: Collect PPM Metro 1 & 2 and PPM Remainder UEs from MDL

# COMMAND ----------

# MAGIC %md
# MAGIC ### PPM Metro UEs

# COMMAND ----------

#########################################################################################################################
##### Pull up UEs from the dsci_tam.universe_estimates table for PPM Metro 1, 2 and Remainder                      ###### 
##### Keep only UEs with the highest load_id.                                                                      ######
#########################################################################################################################

sql_ppm = """
SELECT 
    geography_code,
    'PPM_METRO_CODE' AS market_break,
    CASE
        WHEN market_break = 'PPM Metro 1' THEN 'PPM_METRO_1'
        WHEN market_break = 'PPM Metro 2' THEN 'PPM_METRO_2'
        WHEN market_break = 'PPM Non-Metro' THEN 'PPM_REMAINDER'
    END AS market_break_description,
    universe_estimate, load_id
FROM 
      dsci_tam.universe_estimates 
WHERE 1=1 
    AND measurement_period = '{period}' 
    AND (market_break = 'PPM Metro 1' OR market_break = 'PPM Metro 2' OR market_break = 'PPM Non-Metro') 
    AND market_break_description = 'TV Household'
    AND geography_code IN ({dmas})
    AND geography = 'DMA'
""".format(period=ue_table_measurement_period, dmas=', '.join(str(x) for x in sorted(dma_list)))

ue_table_ppm = spark.sql(sql_ppm)

# Filter the result to the MAX load_id present in the table, which represents the latest data available
max_load_id_ppm = max([x.load_id for x in ue_table_ppm.select('load_id').distinct().collect()])
ue_table_ppm = ue_table_ppm.filter(ue_table_ppm.load_id == max_load_id_ppm).drop('load_id').coalesce(1)

ue_table_ppm.createOrReplaceTempView('ue_table_ppm')
ue_table_ppm.cache()

cached_tables.append('ue_table_ppm')

# COMMAND ----------

######################################################################
##### Create UE for PPM MEtro break when there are two PPM Metros
######################################################################

s = """
SELECT
    A.geography_code, 
    A.market_break,
    "PPM_METRO" AS market_break_description,
    A.universe_estimate + B.universe_estimate as universe_estimate
FROM 
    ue_table_ppm A
JOIN 
  (SELECT
       *
   FROM
       ue_table_ppm
   WHERE
       market_break_description = "PPM_METRO_2") B
ON
    A.geography_code = B.geography_code
    AND A.market_break = B.market_break
WHERE 
    A.market_break_description = "PPM_METRO_1"
"""

ue_table_ppm_metro = spark.sql(s).coalesce(1)
ue_table_ppm_metro.createOrReplaceTempView("ue_table_ppm_metro")
ue_table_ppm_metro.cache()

cached_tables.append('ue_table_ppm_metro')

# COMMAND ----------

# MAGIC %md # Part 5: Union UEs and Join UEs to Collapsed Weighting Controls

# COMMAND ----------

#Union all the required tables
ue_table_final = ue_table_renamed.union(ue_table_agg).union(ue_table_ppm).union(ue_table_ppm_metro).repartition(1)
ue_table_final.createOrReplaceTempView('ue_table_final')
ue_table_final.cache()#.count()

cached_tables.append('ue_table_final')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Join UEs with Weighting Controls

# COMMAND ----------

s = """
SELECT 
    A.*
    ,B.universe_estimate
FROM 
    collapsed_controls AS A
LEFT JOIN 
    ue_table_final AS B
ON
    A.dma = B.geography_code 
    AND A.weighting_control = B.market_break 
    AND A.level = B.market_break_description
"""

weighting_control_evaluation = spark.sql(s).coalesce(1)
weighting_control_evaluation.createOrReplaceTempView('weighting_control_evaluation')
weighting_control_evaluation.cache()

cached_tables.append('weighting_control_evaluation')

# COMMAND ----------

# MAGIC %md # Part 6: Create Geography UEs

# COMMAND ----------

geography_assignment = sqlContext.read.csv(geography_file_dir,header=True)
# Filter only for DMA present in the weighting controls
geography_assignment = geography_assignment.filter(geography_assignment.dma_code.isin(dma_list))
geography_assignment.createOrReplaceTempView("geography_assignment")

# Create County list
county_list = [str(x.county_code) for x in geography_assignment.select('county_code').distinct().collect()]

# COMMAND ----------

# MAGIC %md
# MAGIC ### SQL query to pull UEs from MDL

# COMMAND ----------

###################################################################################################################
#####    Pull up total UEs from the dsci_tam.universe_estimates table for Black Hispanic and Total DMA. 
#####    Keep only UEs for the households without BBO and the measurement_period of interest. 
#####    Keep only UEs with the highest load_id.
###################################################################################################################

s = """
SELECT 
    geography_code,market_break,
    market_break_description,
    universe_estimate,load_id
FROM
     dsci_tam.universe_estimates 
WHERE 1=1
    AND measurement_period='{date}' 
    AND ue_type='H'
    AND geography_code IN ('{counties}')
    AND market_break IN ('Total', 'Black', 'Hispanic') 
    AND market_break_description = "TV Household"
    AND geography = 'LC'
""".format(date=ue_table_measurement_period, counties="', '".join(county_list))

ue_table_mdl_lc = spark.sql(s)

# Filter the result to the MAX load_id present in the table, which represents the latest data available
max_load_id = max([x.load_id for x in ue_table_mdl_lc.select('load_id').distinct().collect()])
ue_table_mdl_lc = ue_table_mdl_lc.filter(ue_table_mdl_lc.load_id == max_load_id).drop('load_id')

ue_table_mdl_lc.createOrReplaceTempView('ue_table_mdl_lc')
ue_table_mdl_lc.cache()

cached_tables.append('ue_table_mdl_lc')

# COMMAND ----------

###################################################################################################################
#####    Compute UEs for complement minority (non-black and non-hispanic)
###################################################################################################################

s = """
SELECT
    A.geography_code,
    CONCAT('Non-', A.market_break) AS market_break,
    A.market_break_description,
    B.universe_estimate - A.universe_estimate AS universe_estimate  -- Computation
FROM
    ue_table_mdl_lc A
JOIN
    (SELECT 
         * 
     FROM 
         ue_table_mdl_lc 
     WHERE 1=1 
         AND market_break = 'Total') B
ON
    A.geography_code = B.geography_code
    AND A.market_break_description = B.market_break_description
WHERE 1=1
    AND A.market_break IN ('Black', 'Hispanic') 
"""

ue_table_mdl_lc_non = spark.sql(s)

ue_table_mdl_lc_non.createOrReplaceTempView('ue_table_mdl_lc_non')
ue_table_mdl_lc_non.cache()

cached_tables.append('ue_table_mdl_lc_non')

# COMMAND ----------

ue_table_mdl_lc_1 = ue_table_mdl_lc.union(ue_table_mdl_lc_non)
ue_table_mdl_lc_1.createOrReplaceTempView('ue_table_mdl_lc_1')
ue_table_mdl_lc_1.cache()

cached_tables.append('ue_table_mdl_lc_1')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Modify Geography Assignment Dataframe

# COMMAND ----------

###################################################################################################################
#####    Create a dataframe containing the county group of each county present in the geography assignment file. 
#####    This dataframe is used to compute UEs for county groups. 
###################################################################################################################

cg_df = geography_assignment.groupBy("dma_code", "county_group", "control")\
                            .agg(collect_list("county_code").alias("counties"))\
                            .withColumn('counties', concat_ws(', ', 'counties'))\
                            .withColumn("control", when(geography_assignment.control == "GEOGRAPHY", "Total").\
                                                   when(geography_assignment.control == "GEOGRAPHY_BLACK_RACE", "Black").\
                                                   when(geography_assignment.control == "GEOGRAPHY_NON-BLACK_RACE", "Non-Black").\
                                                   when(geography_assignment.control == "GEOGRAPHY_HISPANIC", "Hispanic").\
                                                   when(geography_assignment.control == "GEOGRAPHY_NON-HISPANIC", "Non-Hispanic"))
      
cg_df.createOrReplaceTempView("cg_df")
cg_df.cache().count()

cached_tables.append('cg_df')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create County Groups UE

# COMMAND ----------

###################################################################################################################
#####    Compute UEs for each county group present in the geography assignment file 
###################################################################################################################

sql_county_query_part='''
SELECT 
    '{dma_code}' AS DMA_CODE,
    CONCAT("GEOGRAPHY", "{cont_rim}") AS RIM,
    CONCAT("{cont_sub}", "{group_number}", " ", "{counties}") AS SUB_RIM,
    SUM(universe_estimate) AS UE
FROM 
    ue_table_mdl_lc_1
WHERE 1=1 
    AND geography_code IN ({counties})
    AND market_break = "{control}"
GROUP BY DMA_CODE,RIM, SUB_RIM
    
    UNION ALL
'''

sql_county_query = ''

for row in cg_df.rdd.collect():
    if row['control'] == 'Hispanic':
        cont_rim = " WITHIN HISPANIC/NON-HISPANIC"
        cont_sub = "HISPANIC CG "
    elif row['control'] == 'Non-Hispanic':
        cont_rim = " WITHIN HISPANIC/NON-HISPANIC"
        cont_sub = "NON-HISP CG "
    elif row['control'] == 'Black':
        cont_rim = " WITHIN BLACK/NON-BLACK"  
        cont_sub = "RACE=BLK CG "
    elif row['control'] == 'Non-Black':
        cont_rim = " WITHIN BLACK/NON-BLACK"  
        cont_sub = "RACE=NOBLK CG "
    elif row['control'] == 'Total':
        cont_rim = ""  
        cont_sub = "CG "

    sql_county_query += sql_county_query_part.format(control=row['control'],cont_rim=cont_rim,cont_sub=cont_sub,dma_code=row['dma_code'],group_number=row['county_group'], counties=row['counties'])
    
sql_county_query = sql_county_query.rstrip('UNION ALL\n    ')

ue_table_cg = spark.sql(sql_county_query).coalesce(1)
ue_table_cg = ue_table_cg.withColumn("SUB_RIM", regexp_replace("SUB_RIM", ',', ''))
ue_table_cg.createOrReplaceTempView('ue_table_cg')
ue_table_cg.cache()

cached_tables.append('ue_table_cg')

# COMMAND ----------

# MAGIC %md # Part 7: Union Weight controls to Geography 

# COMMAND ----------

# MAGIC %md
# MAGIC ### Remove WC with no UEs and add Geography controls

# COMMAND ----------

###########################################################################################################################
#####     Create a csv file with all weighting controls that did not have any UEs (should be language weighting controls) 
#####     Save weighting_control file with UE
###########################################################################################################################

weighting_control_evaluation_no_ues = weighting_control_evaluation.filter(weighting_control_evaluation.universe_estimate.isNull()).orderBy('dma', 'weighting_control', 'level')
weighting_control_evaluation_no_ues.write.parquet(missing_ue_output_dir, mode='overwrite')


weighting_control_evaluation = weighting_control_evaluation.filter(weighting_control_evaluation.universe_estimate.isNotNull())
weighting_control_evaluation = weighting_control_evaluation.union(ue_table_cg).coalesce(1)
weighting_control_evaluation.cache()

cached_tables.append('weighting_control_evaluation')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Save final output

# COMMAND ----------

sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")
weighting_control_evaluation.write.parquet(final_output_module4_dir, mode='overwrite')

# COMMAND ----------

# for table in cached_tables:
#     uncache_tbl_sql = "UNCACHE TABLE {}".format(table)
#     spark.sql(uncache_tbl_sql)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Notebook Caller Output

# COMMAND ----------

dbutils.notebook.exit("Output of UE Computation Module path: " + final_output_module4_dir)
