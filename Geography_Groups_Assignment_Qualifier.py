# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Geography Groups Assignment Qualifier
# MAGIC 
# MAGIC >This notebook has to be executed prior to running the Weighting Control Evaluation (WCE) software. The geography group assignment CSV file which is qualified by this notebook is needed in the **UE Computation** module of WCE tool.
# MAGIC 
# MAGIC **PURPOSE:**
# MAGIC >* To examine the geography group CSV file which is fed into this software and investigate if the file could be used without any adjustments or not. If changes in some geography groups are required, the software flags them for the user. 
# MAGIC 
# MAGIC **LOW-LEVEL EXPLANATION OF WHAT NOTEBOOK DOES:**
# MAGIC >This software can be broken into 4 sub-modules.
# MAGIC 
# MAGIC > **Part 1: Read Household Characteristics and Geography Group Assignment CSV File**
# MAGIC 
# MAGIC >>* Purpose of this part is to read household characteristics data from the TGB2 views (dsci_tam.household_characteristics) and geography group assignment CSV file that is meant to be investigated by this notebook.  
# MAGIC >>* for TGB2 views BBO homes are removed. Also, only keep TAM intab homes passing the set edit (LOCAL_DAILY_STATUS_IND in (1,3)) and intab PPM homes (LOCAL_DAILY_STATUS_IND in (4))
# MAGIC 
# MAGIC >>* Filteration used for the set_meter markets is 
# MAGIC 
# MAGIC >><pre>  
# MAGIC           (
# MAGIC       (npm_sample_ind='Y' and smm_sample_ind='N' and national_daily_status_ind in (1,3))
# MAGIC       or ((smm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4)))
# MAGIC           )</pre>
# MAGIC           
# MAGIC >>* Filteration used for the lpm markets is 
# MAGIC 
# MAGIC >><pre> ((lpm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4))) </pre>
# MAGIC 
# MAGIC 
# MAGIC > **Part 2: Compute Design Weights**
# MAGIC >>* Purpose of this part is to compute intab design weights for the households. 
# MAGIC >>* Aggregate data from TGB2 view and compute sample size per intab_date/dma_code/ppm_metro_code
# MAGIC >>* Pull up UEs from the dsci_tam.universe_estimates table for dma_code/ppm_metro_code combinations. Keep only UEs for the households without BBO and the measurement_period of interest. Also, keep only UEs with the highest load_id
# MAGIC >>* Join sample size table with the UE table by dma_code and ppm_metro_code. Intab design weight is UE divided by sample size and it is computed per intab_date/dma_code/ppm_metro_code.
# MAGIC >>* The TGB2 table is then joined with the copmuted intab design weights by intab_date,dma_code, and ppm_metro_code 
# MAGIC 
# MAGIC > **Part 3: Define the Functions Used in Flagging **
# MAGIC 
# MAGIC >>* Purpose of this part is first, to flag the current geography groups that are no longer qualified, second, to flag the current geography groups that might qualify for more granularity and, third, to flag the geography controls that are dropped in the geography group CSV file but need to be reexamined as the potential controls.
# MAGIC >>* Three flags might be raised in this module:
# MAGIC >>>* Flag 1: <i>not_enough_sample_size_flag</i>
# MAGIC >>>>* This flag is raised for geography groups of a DMA or minorities/complements within a DMA, which are present in the geography group assignment CSV file, but have an effective sample size of less than 29.01. 
# MAGIC >>>>* The geography group CSV file has only group assignments for DMAs and minorities (Black and Hispanic) within DMAs. However, the software computes the effective sample size of those geography groups for DMAs, minorities and also their complements. The computed effective sample size is then compared to a threshold of 29.01. <i>not_enough_sample_size_flag </i>column for groups with an effective sample size of 29.01 or less is set to 1 and is set to 0 otherwise.  
# MAGIC >>>>* If the historical geography file lacked some counties (mistakenly) or the computed effective sample size of some counties for a DMA or minorities/complements within a DMA are  0 due to 0 sample size in those counties, the software will inform the user about the existence of those DMAs.
# MAGIC >>>* Flag 2: <i>potential_more_granular_assignment_flag</i>
# MAGIC >>>>* This flag is raised for geography groups of a DMA or minorities/complements within a DMA, which are present in the geography group assignment CSV file, and might be eligible for more granularity. 
# MAGIC >>>>* In every existing geography group, the county with the highest effective sample size is identified and excluded. Then the effective sample size of all other counties in that specific group combined together is computed. <i>potential_more_granular_assignment_flag</i> column for groups with an effective sample size of 29.01 or more is set to 1 and is set to 0 otherwise.  
# MAGIC >>>* Flag 3: <i>potential_new_controls_flag</i>
# MAGIC >>>>* This flag is raised for DMAs or minorities/complements within a DMA that are dropped in the current geography group CSV file, but might be eligible as the weighting control in the upcoming report.
# MAGIC >>>>* The effective sample size of all DMAs and minorities/complements within DMAs that are dropped in the current geography group CSV file are computed. In order to have the geography as a weighting control for each DMA or minorities/complements within a DMA, we need at least two qualified geography groups. So, this time the effective sample size of all counties combine together is computed for DMAs or minorities/complements within DMAs.<i>potential_new_controls_flag6</i> column for DMAs or minorities/complements within a DMA with an effective sample size of 58.02 or more is set to 1 and is set to 0 otherwise.  
# MAGIC >>>>* For minorities/complements, this flag is only raised when the effective sample size of both minority and its complement exceeds 58.02. 
# MAGIC >>>>* If the effective sample size of some counties for a DMA or minorities complements within a DMA is 0 due to 0 sample size in those counties, the software will inform the user about the existence of those DMAs.
# MAGIC 
# MAGIC > **Part 4: Implement the Functions and Create the Datasets **
# MAGIC >>* Purpose of this part is to present the discussed  flags in part 3 with an easy and informative format in four dataset mentioned below. 
# MAGIC >>>* <i>county_group_flag1_flag2</i>
# MAGIC >>>>* Each row in this dataset represents a county group. The dataset has <i>not_enough_sample_size_flag</i> and <i>potential_more_granular_assignment_flag</i> columns. It also has the effective sample size of the geography group called <i>ess</i> and the effective sample size of the group excluding the county with the highest effective sample size called <i>ess_nomax</i>. If the group has only one county, <i>ess_nomax</i> is set to "Group with 1 County" value.    
# MAGIC >>>* <i>historical_geography_flag1_flag2</i>
# MAGIC >>>>* This dataset is the same as geography group CSV file Except that it borrows  <i>not_enough_sample_size_flag</i> and <i>potential_more_granular_assignment_flag</i>  columns from <i>county_group_flag1_flag2</i> dataset. So, each row in this dataset represents a county. 
# MAGIC >>>* <i>total_dma_flag3</i>
# MAGIC >>>>* This dataset has <i>potential_new_controls_flag</i> column. Each row in this dataset represents a DMA or minorities/complements within a DMA. 
# MAGIC >>>>* Only minorities/complements within a DMA whose <i>potential_new_controls_flag</i> is equal to 1 for both minorities and their complements are shown in this dataset
# MAGIC >>>* <i>county_flag3</i>
# MAGIC >>>>* This dataset is the expansion of <i>total_dma_flag3</i> dataset by counties.  So, each row in this dataset represents a county within a DMA.  
# MAGIC 
# MAGIC **USER INPUTS:** 
# MAGIC > 1. **start_date**
# MAGIC >  - The beginning of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC > 2. **end_date**
# MAGIC >  - The ending of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC > 3. **sample_type** 
# MAGIC >  - Market type of the analysis, provided as a string, the acceptable market types are 'SET_METER+PPM' or 'LPM+PPM'. The software cannot be executed for LPM plus PPM and set meter plus ppm markets simultaneously.
# MAGIC > 4. **DMA codes**
# MAGIC >  - this should be a Python string of dmas, e.g. '501,602,703', '501'.
# MAGIC >  - lmp plus ppm market codes are '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC >  - set meter plus ppm market codes are '515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839'
# MAGIC > 5. **ue_table_measurement_period**
# MAGIC >  - The period for which UEs are pulled up from the MDL, provided as a string, in the following format **monthyy**
# MAGIC >  - The software only pulls up ues with the maximum load id.
# MAGIC >  - examples are 'Feb18' or 'May18'
# MAGIC > 6. **geography_file_directory**
# MAGIC >  - The directory of the geography group assignment file, with a csv format, provided as a string. 
# MAGIC > 7. **please_export**
# MAGIC >  - Provided as a string, set this varialbe to 'True' if you want to save the data in the Optional Overrides sections and 'False' otherwise. 
# MAGIC > 8. **override_dir_county_group_flag1_flag2**
# MAGIC >  - the directory in which the software saves the county_group_flag1_flag2 dataset with a parquet fomat.
# MAGIC > 9. **override_dir_historical_geography_flag1_flag2**
# MAGIC >  - the directory in which the software saves the historical_geography_flag1_flag2 dataset with a parquet fomat.
# MAGIC > 10. **override_dir_total_dma_flag3**
# MAGIC >  - the directory in which the software saves the total_dma_flag3 dataset with a parquet fomat.
# MAGIC > 11. **override_dir_county_flag3**
# MAGIC >  - the directory in which the software saves the county_flag3 dataset with a parquet fomat.
# MAGIC  
# MAGIC **THE GEOGRAPHY FILE FORMAT**
# MAGIC > | column                    | data_type | example                            |
# MAGIC   |---------------------------|-----------|------------------------------------|
# MAGIC   | county_group              | bigint    | 1                                  |
# MAGIC   | dma_code                  | bigint    | 501                                |
# MAGIC   | control                   | string    | GEOGRAPHY or GEOGRAPHY_BLACK_RACE  |
# MAGIC   | county_code               | bigint    | 31059                              |
# MAGIC   
# MAGIC  
# MAGIC 
# MAGIC **DATA SOURCES:**
# MAGIC > | schema               | table                                                        |
# MAGIC   |----------------------|--------------------------------------------------------------|
# MAGIC   | dsci_tam             | universe_estimates                                           |
# MAGIC   | dsci_tam             | household_characteristics                                    |
# MAGIC   | tam_lpm_mch_tv_prod  | local_county                                                 |             
# MAGIC   | --------             | a CSV file including county groups assignment                |  
# MAGIC   
# MAGIC **AN EXAMPLE RUN:**
# MAGIC > <pre>
# MAGIC start_date                                       = '2018-03-01'
# MAGIC end_date                                         = '2018-03-02'
# MAGIC sample_type                                      = 'LPM+PPM'
# MAGIC dma_codes                                        = '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC ue_table_measurement_period                      = 'May18' 
# MAGIC please_export                                    = 'True'
# MAGIC geography_file_dir                               = '/FileStore/tables/historical_geography_assignment_example.csv'
# MAGIC override_dir_county_group_flag1_flag2            = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/county_group_flag1_flag2.parquet'
# MAGIC override_dir_historical_geography_flag1_flag2    = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/historical_geography_flag1_flag2.parquet'
# MAGIC override_dir_total_dma_flag3                     = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/total_dma_flag3.parquet'
# MAGIC override_dir_county_flag3                        = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/county_flag3.parquet'
# MAGIC 
# MAGIC </pre>
# MAGIC 
# MAGIC **AN EXAMPLE OUTPUT:**
# MAGIC >* county_group_flag1_flag2 dataset
# MAGIC >> <pre>
# MAGIC <table class="confluenceTable">
# MAGIC <tbody>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">dma_code</td>
# MAGIC <td class="confluenceTd mceSelected">control</td>
# MAGIC <td class="confluenceTd mceSelected">parameter</td>
# MAGIC <td class="confluenceTd mceSelected">ess</td>
# MAGIC <td class="confluenceTd mceSelected">num_ppms</td>
# MAGIC <td class="confluenceTd mceSelected">county_group_names</td>
# MAGIC <td class="confluenceTd mceSelected">county_group_ess</td>
# MAGIC <td class="confluenceTd mceSelected">not_enough_sample_size_flag</td>
# MAGIC <td class="confluenceTd mceSelected">potential_more_granular_assignment_flag</td>
# MAGIC <td class="confluenceTd mceSelected">ess_nomax</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG 2 31119 31005</td>
# MAGIC <td class="confluenceTd mceSelected">132</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC <td class="confluenceTd mceSelected">CG 2: Westchester, Bronx</td>
# MAGIC <td class="confluenceTd mceSelected">CG 2: 22.0, 110.0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">22</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG 6 6001 31087 31027 31071 29031 29027 31105</td>
# MAGIC <td class="confluenceTd mceSelected">43.91</td>
# MAGIC <td class="confluenceTd mceSelected">2</td>
# MAGIC <td class="confluenceTd mceSelected">CG 6: Fairfield, Rockland, Dutchess, Orange, Passaic, Morris, Sullivan</td>
# MAGIC <td class="confluenceTd mceSelected">CG 6: 13.26, 9.0, 1.0, 1.5, 19.0, 5.0, 1.0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">27.59</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_NON-HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG 2 31119 31005</td>
# MAGIC <td class="confluenceTd mceSelected">163.5</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC <td class="confluenceTd mceSelected">CG 2: Westchester, Bronx</td>
# MAGIC <td class="confluenceTd mceSelected">CG 2: 94.0, 69.5</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC <td class="confluenceTd mceSelected">69.5</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_NON-HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG 6 6001 31087 31027 31071 31079 37103 31111 29031 29027 29037 31105</td>
# MAGIC <td class="confluenceTd mceSelected">198.48</td>
# MAGIC <td class="confluenceTd mceSelected">2</td>
# MAGIC <td class="confluenceTd mceSelected">CG 6: Fairfield, Rockland, Dutchess, Orange, Putnam, Pike, Ulster, Passaic, Morris, Sussex, Sullivan</td>
# MAGIC <td class="confluenceTd mceSelected">CG 6: 42.27, 28.5, 14.5, 19.5, 6.0, 6.0, 7.5, 38.0, 61.5, 8.0, 4.0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC <td class="confluenceTd mceSelected">150.52</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY</td>
# MAGIC <td class="confluenceTd mceSelected">CG 17 31071 31027 31079</td>
# MAGIC <td class="confluenceTd mceSelected">40.55</td>
# MAGIC <td class="confluenceTd mceSelected">2</td>
# MAGIC <td class="confluenceTd mceSelected">CG 17: Orange, Dutchess, Putnam</td>
# MAGIC <td class="confluenceTd mceSelected">CG 17: 21.0, 15.5, 6.0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">19.63</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY</td>
# MAGIC <td class="confluenceTd mceSelected">CG 18 29029</td>
# MAGIC <td class="confluenceTd mceSelected">39.5</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC <td class="confluenceTd mceSelected">CG 18: Ocean</td>
# MAGIC <td class="confluenceTd mceSelected">CG 18: 39.5</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">Group with 1 County</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY</td>
# MAGIC <td class="confluenceTd mceSelected">CG 16 29041 31111 6001 31105</td>
# MAGIC <td class="confluenceTd mceSelected">72.37</td>
# MAGIC <td class="confluenceTd mceSelected">2</td>
# MAGIC <td class="confluenceTd mceSelected">CG 16: Warren, Ulster, Fairfield, Sullivan</td>
# MAGIC <td class="confluenceTd mceSelected">CG 16: 5.0, 7.5, 55.45, 5.0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">17.5</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY</td>
# MAGIC <td class="confluenceTd mceSelected">CG 15 29025</td>
# MAGIC <td class="confluenceTd mceSelected">74</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC <td class="confluenceTd mceSelected">CG 15: Monmouth</td>
# MAGIC <td class="confluenceTd mceSelected">CG 15: 74.0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">Group with 1 County</td>
# MAGIC </tr>
# MAGIC </tbody>
# MAGIC </table>
# MAGIC 
# MAGIC </pre>
# MAGIC >* historical_geography_flag1_flag2 dataset
# MAGIC >> <pre>
# MAGIC <table class="confluenceTable">
# MAGIC <tbody>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">county_group</td>
# MAGIC <td class="confluenceTd mceSelected">dma_code</td>
# MAGIC <td class="confluenceTd mceSelected">control</td>
# MAGIC <td class="confluenceTd mceSelected">county_code</td>
# MAGIC <td class="confluenceTd mceSelected">county_name</td>
# MAGIC <td class="confluenceTd mceSelected">count</td>
# MAGIC <td class="confluenceTd mceSelected">not_enough_sample_size_flag</td>
# MAGIC <td class="confluenceTd mceSelected">potential_more_granular_assignment_flag</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">5</td>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">31085</td>
# MAGIC <td class="confluenceTd mceSelected">RICHMOND</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">5</td>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">31085</td>
# MAGIC <td class="confluenceTd mceSelected">RICHMOND</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY</td>
# MAGIC <td class="confluenceTd mceSelected">31047</td>
# MAGIC <td class="confluenceTd mceSelected">KINGS</td>
# MAGIC <td class="confluenceTd mceSelected">142</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC <td class="confluenceTd mceSelected">501</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY</td>
# MAGIC <td class="confluenceTd mceSelected">31047</td>
# MAGIC <td class="confluenceTd mceSelected">KINGS</td>
# MAGIC <td class="confluenceTd mceSelected">142</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC </tr>
# MAGIC </tbody>
# MAGIC </table>
# MAGIC </pre>
# MAGIC 
# MAGIC >* total_dma_flag3 dataset
# MAGIC >> <pre>
# MAGIC <table class="confluenceTable">
# MAGIC <tbody>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">dma_code</td>
# MAGIC <td class="confluenceTd mceSelected">control</td>
# MAGIC <td class="confluenceTd mceSelected">parameter</td>
# MAGIC <td class="confluenceTd mceSelected">ess</td>
# MAGIC <td class="confluenceTd mceSelected">num_ppms</td>
# MAGIC <td class="confluenceTd mceSelected">potential_new_controls_flag</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG All counties</td>
# MAGIC <td class="confluenceTd mceSelected">60.25</td>
# MAGIC <td class="confluenceTd mceSelected">2</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_NON-HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG All counties</td>
# MAGIC <td class="confluenceTd mceSelected">1187.35</td>
# MAGIC <td class="confluenceTd mceSelected">2</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC </tr>
# MAGIC </tbody>
# MAGIC </table>
# MAGIC 
# MAGIC </pre>
# MAGIC 
# MAGIC 
# MAGIC >* county_flag3 dataset
# MAGIC 
# MAGIC >> <pre>
# MAGIC <table class="confluenceTable">
# MAGIC <tbody>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">DMA_CODE</td>
# MAGIC <td class="confluenceTd mceSelected">DMA_SHORT_NAME</td>
# MAGIC <td class="confluenceTd mceSelected">STATE_LOCAL_COUNTY_CODE</td>
# MAGIC <td class="confluenceTd mceSelected">local_county_name</td>
# MAGIC <td class="confluenceTd mceSelected">STATE_ABBREVIATION</td>
# MAGIC <td class="confluenceTd mceSelected">control</td>
# MAGIC <td class="confluenceTd mceSelected">parameter</td>
# MAGIC <td class="confluenceTd mceSelected">ess</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">Detroit</td>
# MAGIC <td class="confluenceTd mceSelected">21147</td>
# MAGIC <td class="confluenceTd mceSelected">St Clair</td>
# MAGIC <td class="confluenceTd mceSelected">MI</td>
# MAGIC <td class="confluenceTd mceSelected">nan</td>
# MAGIC <td class="confluenceTd mceSelected">nan</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">Detroit</td>
# MAGIC <td class="confluenceTd mceSelected">21115</td>
# MAGIC <td class="confluenceTd mceSelected">Monroe</td>
# MAGIC <td class="confluenceTd mceSelected">MI</td>
# MAGIC <td class="confluenceTd mceSelected">nan</td>
# MAGIC <td class="confluenceTd mceSelected">nan</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">Detroit</td>
# MAGIC <td class="confluenceTd mceSelected">21151</td>
# MAGIC <td class="confluenceTd mceSelected">Sanilac</td>
# MAGIC <td class="confluenceTd mceSelected">MI</td>
# MAGIC <td class="confluenceTd mceSelected">nan</td>
# MAGIC <td class="confluenceTd mceSelected">nan</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">Detroit</td>
# MAGIC <td class="confluenceTd mceSelected">21125</td>
# MAGIC <td class="confluenceTd mceSelected">Oakland</td>
# MAGIC <td class="confluenceTd mceSelected">MI</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG 21125</td>
# MAGIC <td class="confluenceTd mceSelected">10</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">Detroit</td>
# MAGIC <td class="confluenceTd mceSelected">21093</td>
# MAGIC <td class="confluenceTd mceSelected">Livingston</td>
# MAGIC <td class="confluenceTd mceSelected">MI</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG 21093</td>
# MAGIC <td class="confluenceTd mceSelected">1</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">Detroit</td>
# MAGIC <td class="confluenceTd mceSelected">21161</td>
# MAGIC <td class="confluenceTd mceSelected">Washtenaw</td>
# MAGIC <td class="confluenceTd mceSelected">MI</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG 21161</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">Detroit</td>
# MAGIC <td class="confluenceTd mceSelected">21099</td>
# MAGIC <td class="confluenceTd mceSelected">Macomb</td>
# MAGIC <td class="confluenceTd mceSelected">MI</td>
# MAGIC <td class="confluenceTd mceSelected">GEOGRAPHY_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">CG 21099</td>
# MAGIC <td class="confluenceTd mceSelected">11</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">Detroit</td>
# MAGIC <td class="confluenceTd mceSelected">21087</td>
# MAGIC <td class="confluenceTd mceSelected">Lapeer</td>
# MAGIC <td class="confluenceTd mceSelected">MI</td>
# MAGIC <td class="confluenceTd mceSelected">nan</td>
# MAGIC <td class="confluenceTd mceSelected">nan</td>
# MAGIC <td class="confluenceTd mceSelected">0</td>
# MAGIC </tr>
# MAGIC </tbody>
# MAGIC </table>
# MAGIC 
# MAGIC </pre>
# MAGIC 
# MAGIC **CODE DEVELOPERS:**
# MAGIC >* MJ PAKDEL
# MAGIC >* JONATHAN OUEGNIN

# COMMAND ----------

from pyspark.sql.functions import lit, when,col
from pyspark.sql import functions as F
from collections import defaultdict
import pandas as pd
import re


# COMMAND ----------

# MAGIC %md ### Define Parameters

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################
start_date                                       = '2018-07-10'
end_date                                         = '2018-07-10'
sample_type                                      = 'LPM+PPM'
dma_codes                                        = '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# dma_codes                                        = '751'
ue_table_measurement_period                      = 'Jul18' 
please_export                                    = 'True'
geography_file_dir                               = 's3://useast1-nlsn-w-digital-dsci-dev/users/buarol01/Geography_assignment/LPM_PPM_Geog19_USE.csv'

override_dir_county_group_flag1_flag2            = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/county_group_flag1_flag2.parquet'
override_dir_historical_geography_flag1_flag2    = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/historical_geography_flag1_flag2.parquet'
override_dir_total_dma_flag3                     = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/total_dma_flag3.parquet'
override_dir_county_flag3                        = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/county_flag3.parquet'

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Validate Sample Type User Input

# COMMAND ----------

sample_type = sample_type.upper()

if (sample_type != "LPM+PPM") and (sample_type != "SET_METER+PPM"):
    dbutils.notebook.exit("Invalid Sample Type Input")

# COMMAND ----------

###################################################################################################################
#####    Depending on the sample_type, here some filters are created to pull data from TGB2 and B1010    #########
###################################################################################################################

if sample_type == 'SET_METER+PPM':
    sample_type='''(
      (npm_sample_ind='Y' and smm_sample_ind='N' and national_daily_status_ind in (1,3))
      or ((smm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4)))
      )'''
  
elif sample_type == 'LPM+PPM':
    sample_type="((lpm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4)))"

  
sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")
  
sql_minority_filter = ["AND BLACK = 'BLACK_RACE'", "AND BLACK = 'NON-BLACK_RACE'", "AND ORIGIN = 'HISPANIC'", "AND ORIGIN = 'NON-HISPANIC'" , ""] 

cached_tables = [] 

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 1: Read Household Characteristics and Geography Group Assignment CSV File

# COMMAND ----------

# MAGIC %md ### Pull TGB2 Data

# COMMAND ----------

###################################################################################################################
#####    Pull up data from TGB2 view (dsci_tam.household_characteristics)                                    ######
#####    BBO homes are removed                                                                               ###### 
###################################################################################################################

s = '''
SELECT 
    DMA_CODE,INTAB_DATE,HOUSEHOLD_ID,lpm_sample_ind,ppm_sample_ind,smm_sample_ind,NMR_STCTY,
    CASE 
        WHEN HOH_ORIGIN_CODE = '1' THEN 'NON-HISPANIC'
        ELSE 'HISPANIC' 
    END AS ORIGIN,
    CASE
        WHEN HH_BLACK_IND = 'Y' THEN 'BLACK_RACE'
        ELSE 'NON-BLACK_RACE'
    END AS BLACK,
    CASE 
        WHEN PPM_METRO_CODE=1 THEN 'PPM Metro 1'
        WHEN PPM_METRO_CODE=2 THEN 'PPM Metro 2'
    ELSE 'PPM Non-Metro' END AS PPM_METRO_CODE
FROM 
    dsci_tam.household_characteristics 
WHERE 1=1
    AND intab_date between '{start_date}' and '{end_date}'
    AND dma_code in ({dma_codes})
    AND {sample_type}
    AND HH_BROADBAND_ONLY_IND!='Y'
    '''.format(dma_codes=dma_codes,start_date=start_date,end_date=end_date,sample_type=sample_type)

tgb2_file = spark.sql(s)
tgb2_file.createOrReplaceTempView('tgb2_file')
tgb2_file.cache()
# tgb2_file.count()
#display(tgb2_file)
cached_tables.append('tgb2_file')

# COMMAND ----------

# MAGIC %md ### Load Geography Group Assignment CSV File

# COMMAND ----------

###################################################################################################################
#####    Read geography group assignment CSV file that is meant to be investigated by this notebook          ###### 
###################################################################################################################

historical_geography=sqlContext.read.csv(geography_file_dir,header=True)
historical_geography.createOrReplaceTempView("historical_geography")
historical_geography=historical_geography.filter(col('dma_code').isin(dma_codes.split(',')))
dma_list = [str(x.DMA_CODE) for x in historical_geography.select('DMA_CODE').distinct().collect()]
for dma in dma_codes.split(','):
  if dma not in dma_list:
    print('Caution: no row with dma_code '+dma+' is found is historical geography file')
#display(historical_geography)

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 2: Compute Design Weights

# COMMAND ----------

# MAGIC %md ### Computing Intab Counts per intab_date/dma_code/ppm_metro_code

# COMMAND ----------

###################################################################################################################
#####    Aggregate data from TGB2 view and compute sample size per intab_date/dma_code/ppm_metro_code
###################################################################################################################

s = '''
SELECT
    INTAB_DATE,
    DMA_CODE,
    PPM_METRO_CODE,
    count(*) as intab_count
FROM
    tgb2_file
GROUP BY
    INTAB_DATE,DMA_CODE,PPM_METRO_CODE
ORDER BY
    INTAB_DATE,DMA_CODE,PPM_METRO_CODE
'''
intab_counts = spark.sql(s)
intab_counts.createOrReplaceTempView('intab_counts')
intab_counts.cache()
# intab_counts.count()
#isplay(intab_counts)
cached_tables.append('intab_counts')

# COMMAND ----------

# MAGIC %md ### Pull up UE Data  from the MDL per dma_code/ppm_metro_code

# COMMAND ----------

###################################################################################################################
#####    Pull up UEs from the dsci_tam.universe_estimates table for all dma_code/ppm_metro_code combinations ###### 
#####    Keep only UEs for the households without BBO and the measurement_period of interest                 ######          
#####    Also, keep only UEs with the highest load_id.                                                       ######
###################################################################################################################

s = ''' 
SELECT 
    geography_code as dma_code,market_break,universe_estimate,load_id
FROM
    dsci_tam.universe_estimates 
WHERE 
    measurement_period='{ue_period}'
    AND ue_type='H'
    AND ((market_break="PPM Metro 1" AND market_break_description="TV Household") OR 
        (market_break="PPM Metro 2" AND market_break_description="TV Household") OR 
        (market_break="PPM Non-Metro" AND market_break_description="TV Household"))
ORDER BY
    dma_code,market_break
    '''.format(ue_period=ue_table_measurement_period)
ue_table = spark.sql(s)
ue_table = ue_table.filter(ue_table.load_id == max([x.load_id for x in ue_table.select('load_id').distinct().collect()])).drop('load_id')
ue_table.createOrReplaceTempView("ue_table")
ue_table.cache()
# display(ue_table)
cached_tables.append('ue_table')

# COMMAND ----------

# MAGIC %md ### Compute Intab Design Weights per intab_date/dma_code/ppm_metro_code

# COMMAND ----------

###################################################################################################################
#####    Join sample size table with the UE table by dma_code and ppm_metro_code. 
#####    Intab design weight is UE divided by sample size and it is computed per intab_date/dma_code/ppm_metro_code
###################################################################################################################

s = '''
SELECT --count(*)
    A.*,B.universe_estimate,round(B.universe_estimate/A.intab_count,4) as weight
FROM
    intab_counts A
JOIN
    ue_table B
WHERE
    A.DMA_CODE=B.dma_code
    AND A.ppm_metro_code=B.market_break
ORDER BY
    INTAB_DATE,A.DMA_CODE,A.PPM_METRO_CODE
'''

calculated_design_weight = spark.sql(s)
calculated_design_weight.createOrReplaceTempView('calculated_design_weight')
calculated_design_weight.cache()
#display(calculated_design_weight)
cached_tables.append('calculated_design_weight')

# COMMAND ----------

# MAGIC %md ### Bring Weights to the TGB2 Table

# COMMAND ----------

###################################################################################################################
#####    The TGB2 table is then joined with the copmuted intab design weights by 
#####        intab_date,dma_code, and ppm_metro_code
###################################################################################################################

s = '''
SELECT 
    a.*,b.weight    
FROM
    tgb2_file AS a  
JOIN 
    calculated_design_weight AS b
ON 
    a.dma_code=b.dma_code
    AND a.PPM_METRO_CODE=b.PPM_METRO_CODE
    AND a.intab_date=b.intab_date
'''

tgb2_file_w = spark.sql(s).repartition(16)
sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")
# tgb2_file_w = tgb2_file_w.drop('NMR_STCTY','LPM_SAMPLE_IND','SMM_SAMPLE_IND','PPM_SAMPLE_IND')
tgb2_file_w.createOrReplaceTempView('tgb2_file_w')
tgb2_file_w.cache()
# tgb2_file_w.count()
# display(tgb2_file_w)
cached_tables.append('tgb2_file_w')

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 3: Define the Functions Used in Flagging

# COMMAND ----------

# MAGIC %md ### Function to Filter Data by DMA and Minority of Interest

# COMMAND ----------

###################################################################################################################
#####    Read TGB2 data fro DMA or minority/complement of interest
###################################################################################################################

def filter_data_func(dma,min_filter):
    """This function filters the data to a specific DMA and minority."""
    s = """
    SELECT * FROM tgb2_file_w WHERE DMA_CODE = {dma_num} {minority_filter}
    """.format(dma_num = dma, minority_filter = min_filter)

    filtered_dt = spark.sql(s)
    return(filtered_dt)

# COMMAND ----------

# MAGIC %md ### Function to Pull County Codes and County Names for DMA of Interest

# COMMAND ----------

###################################################################################################################
#####    Read county names for dma of interest from tam_lpm_mch_tv_prod.local_county
###################################################################################################################

def pull_data_local_county(dma_code):
    s='''SELECT 
             DMA_CODE,DMA_SHORT_NAME,STATE_LOCAL_COUNTY_CODE ,local_county_name,STATE_ABBREVIATION
         FROM 
             tam_lpm_mch_tv_prod.local_county
         WHERE 1=1
             AND DMA_CODE = {dma_code}
             --AND effective_end_date='3000-12-31'
             AND effective_end_date>='{end_date}'
             '''.format(dma_code=dma,end_date=end_date)
    local_county=spark.sql(s)
    local_county.createOrReplaceTempView("local_county")
    local_county=local_county.toPandas()
    local_county['STATE_LOCAL_COUNTY_CODE']=local_county['STATE_LOCAL_COUNTY_CODE'].astype(str)
    return(local_county)

# COMMAND ----------

# MAGIC %md ### Function to Join Group Number to the TGB2 File

# COMMAND ----------

###################################################################################################################
#####    TGB2 file joined with the geography group csv file to borrow the geography group
#####    the historical file only has the minorities
#####    group number of minorities in geography file is assigned to both minorities and their complements 
###################################################################################################################

def join_county_group():
    if min_filter == sql_minority_filter[0] or min_filter == sql_minority_filter[1]:
      join_county = 'GEOGRAPHY_BLACK_RACE'

    elif min_filter == sql_minority_filter[2] or min_filter == sql_minority_filter[3]:
      join_county = 'GEOGRAPHY_HISPANIC'

    else:
      join_county = 'GEOGRAPHY'

    # Maybe use a Broadcast join here to be more efficient

    raw_filtered_data_gr = raw_filtered_data\
                     .join(historical_geography, raw_filtered_data.NMR_STCTY == historical_geography.county_code, 'left')\
                     .filter(historical_geography['control'] == join_county)\
                     .select(col('county_group'),raw_filtered_data['*'])


    return(raw_filtered_data_gr)

# COMMAND ----------

# MAGIC %md ### Function to Create a Dictionary of All Counties withing a Group

# COMMAND ----------

###################################################################################################################
#####    All counties in the same geography group are stored in lists within a dictionary 
###################################################################################################################

def create_county_group_dict():
  
    if min_filter == sql_minority_filter[0] or min_filter == sql_minority_filter[1]:
      join_county = 'GEOGRAPHY_BLACK_RACE'

    elif min_filter == sql_minority_filter[2] or min_filter == sql_minority_filter[3]:
      join_county = 'GEOGRAPHY_HISPANIC'

    else:
      join_county = 'GEOGRAPHY'
      
    county_li=historical_geography.filter(historical_geography['control']== join_county).filter(historical_geography['dma_code']== dma).select('county_group','county_code').distinct().collect()

    county_list = []

    for county_gr,county_code in county_li:
      gr_code = ('CG ' + county_gr,county_code)
      county_list.append(gr_code)

    county_group_dict = defaultdict(list)

    for group, code in county_list:
        county_group_dict[group].append(code)

    county_group_dict = {group:list(code) for group, code in county_group_dict.items()}

    return(county_group_dict)

# COMMAND ----------

# MAGIC %md ### Function to Compute the Effective Sample Size

# COMMAND ----------

###################################################################################################################
#####    compute the effective sample size for the table of interest, DMA or minorities/complements within a DMA 
#####         of interest
###################################################################################################################

def calculate_ess(table_name,min_filter):
    """This function calculate the effective sample size given a DMA Number"""

    if min_filter == sql_minority_filter[0]:
      control_min = 'GEOGRAPHY_BLACK_RACE'

    elif min_filter == sql_minority_filter[1]:
      control_min = 'GEOGRAPHY_NON-BLACK_RACE'

    elif min_filter == sql_minority_filter[2]:
      control_min = 'GEOGRAPHY_HISPANIC'

    elif min_filter == sql_minority_filter[3]:
      control_min = 'GEOGRAPHY_NON-HISPANIC'

    else:
      control_min = 'GEOGRAPHY'


    s = """
        SELECT 
            dma_code,
            '{minority}' AS control,
            CONCAT("CG ", county_group) AS parameter,
            ROUND(MEAN(PESS),2) AS ess,
            num_ppms
        FROM(
            SELECT
                INTAB_DATE,
                dma_code,
                county_group,
                COUNT(DISTINCT PPM_METRO_CODE) AS num_ppms,
                ((sum(weight)*sum(weight))/sum(weight*weight)) AS PESS
            FROM 
                {table} 
            GROUP BY
                INTAB_DATE,dma_code,county_group
            )
        GROUP BY
             dma_code,control,parameter,num_ppms
         """.format(minority = control_min,table=table_name)

    filtered_dt_ess = spark.sql(s).repartition(1)
    filtered_dt_ess_pd = filtered_dt_ess.toPandas()
    return(filtered_dt_ess_pd)

# COMMAND ----------

# MAGIC %md ### Function to Create a Dic of County Names and ESS of All Counties withing a Group

# COMMAND ----------

###################################################################################################################
#####    county names and ess of all counties in the same geography group are stored in lists within two
#####            seperate dictionaries 
###################################################################################################################

def collect_county_names_and_county_ess():
    raw_filtered_data_county = raw_filtered_data.withColumn('county_group',raw_filtered_data['NMR_STCTY'])
    raw_filtered_data_county.createOrReplaceTempView("raw_filtered_data_county")
    aggregated_data_county = calculate_ess('raw_filtered_data_county',min_filter)
    aggregated_data_county['STATE_LOCAL_COUNTY_CODE'] = aggregated_data_county['parameter'].astype(str).str[3:]
    aggregated_data_county['STATE_LOCAL_COUNTY_CODE'] = aggregated_data_county['STATE_LOCAL_COUNTY_CODE'].astype(str)
    # print(aggregated_data_county)
    aggregated_data_county_with_name = aggregated_data_county.merge(local_county_table,how='right',left_on=['STATE_LOCAL_COUNTY_CODE'],right_on=  ['STATE_LOCAL_COUNTY_CODE'])
    # print(aggregated_data_county_with_name)
    
    if True in (aggregated_data_county_with_name['ess'].isnull().tolist()):
        print('For DMA '+ str(dma)+ ' at '+con_filter+' level '+'either the historical geography file lacked some counties or the effective sample size of those counties this year are 0')
        
    aggregated_data_county_with_name['ess'].fillna(0,inplace=True)
    
    county_name_group_dict = {}
    county_ess_group_dict = {}
    
    for key in county_group_dict:
        county_name_group_dict[key] = []
        county_ess_group_dict[key] = []
        for county_code in county_group_dict[key]:
            reduced_data = aggregated_data_county_with_name[aggregated_data_county_with_name['STATE_LOCAL_COUNTY_CODE'] == county_code].reset_index()
            county_name_group_dict[key].append(reduced_data.loc[0,'local_county_name'])
            county_ess_group_dict[key].append(reduced_data.loc[0,'ess'])
            
    return county_name_group_dict,county_ess_group_dict


# COMMAND ----------

# MAGIC %md ### Helper Function that Attaches County Group and County Lists

# COMMAND ----------

def replace_val(x):
    if str(x) in county_group_dict:
        county_groups_str = ' '.join(county_group_dict[str(x)])
        return(x + ' ' + county_groups_str)

# COMMAND ----------

# MAGIC %md ### Helper Function that Attaches County Group and County Names

# COMMAND ----------

def replace_val1(x):
    if str(x) in county_name_group_dict:
      county_name_groups_str = ', '.join(county_name_group_dict[str(x)])
      return(x + ': ' + county_name_groups_str)

# COMMAND ----------

# MAGIC %md ### Helper Function that Attaches County Group and County ESS

# COMMAND ----------

def replace_val2(x):
    if str(x) in county_ess_group_dict:
      county_ess_groups_str=', '.join([str(i) for i in county_ess_group_dict[str(x)]])
      return(x + ': ' + county_ess_groups_str)

# COMMAND ----------

# MAGIC %md ### Function to Check the Sufficiency of Effective Sample Size

# COMMAND ----------

###################################################################################################################
#####   This flag is raised for geography groups of a DMA or minorities/complements within a DMA, which 
#####         are present in the geography group assignment CSV file, but have an effective sample size 
#####         of less than 29.01.
###################################################################################################################

def not_enough_sample_size(x):
    if x <= 29.01:
      return('1')
    else:
      return('0')

# COMMAND ----------

# MAGIC %md ### Function to Check the Availability of More Granular Assignment

# COMMAND ----------

###################################################################################################################
#####   In every existing geography group, the county with the highest effective sample size is 
#####        identified and excluded. Then the effective sample size of all other counties in that
#####        specific group combined together is computed. potential_more_granular_assignment_flag column
#####        for groups with an effective sample size of 29.01 or more is set to 1 and is set to 0 otherwise.
###################################################################################################################

def check_granularity(list_county):
  """This function checks if the county group can be changed to a more granular level."""
  key=re.search('\w{2,2} \d+',list_county).group()
  counties_ess_list=county_ess_group_dict[key]
  max_county_position=counties_ess_list.index(max(counties_ess_list))
  list_county=county_group_dict[key]
  if len(list_county) > 1:
      list_county.pop(max_county_position)
      # Compute Effective Sample Size for all the counties combined in the new list
      query2 = """
          SELECT 
              dma_code,
              grouped_counties,
              ROUND(MEAN(PESS),2) AS ess
          FROM(
              SELECT
                  INTAB_DATE,
                  dma_code,
                  'CG {county_codes1}' AS grouped_counties,
                  ((sum(weight)*sum(weight))/sum(weight*weight)) AS PESS
              FROM 
                  raw_filtered_data_gr
              WHERE 
                  1=1 AND NMR_STCTY IN ({county_codes2})
              GROUP BY
                  INTAB_DATE,dma_code,grouped_counties
              )
          GROUP BY
               dma_code,grouped_counties      
           """.format(county_codes1 = ' '.join([str(x) for x in list_county]), county_codes2 = ', '.join([str(x) for x in list_county]))
    
      counties_ess_nomax = spark.sql(query2).repartition(1)

      if counties_ess_nomax.count()!=0 :
          ess_nomax = counties_ess_nomax.select('ess').collect()[0].ess
      else:
          ess_nomax = 0

      # Check if the Effective Sample Size all the remaining counties is greater or smaller than 29.01 and raise flag if necessary
      if ess_nomax < 29.01:
        return('0', ess_nomax)
      else:
        return('1', ess_nomax)
  
  else: 
      return('0', 'Group with 1 County')
  
  
  
  
  

# COMMAND ----------

# MAGIC %md ### Function to Check the Possibility of Adding Missing Controls

# COMMAND ----------

###################################################################################################################
#####   The effective sample size of all DMAs and minorities/complements within DMAs that are dropped 
#####        in the current geography group CSV file are computed. In order to have the geography as a 
#####        weighting control for each DMA or minorities/complements within a DMA, we need at least two 
#####        qualified geography groups. So, this time the effective sample size of all counties combine 
#####        together is computed for DMAs or minorities/complements within DMAs.potential_new_controls_flag6 column 
#####        for DMAs or minorities/complements within a DMA with an effective sample size of 58.02 or more is set 
#####        to 1 and is set to 0 otherwise.
###################################################################################################################


def potential_new_controls(dataset):
    # if dataset.effective_sample_size.sum().round(2) > 58.02: 
    if dataset.ess.sum() > 58.02:
        return('1')
    else:
        return('0')

# COMMAND ----------

# MAGIC %md ### Function to Check if both Minorities/Complements of Missing Controls are Qualified 

# COMMAND ----------

###################################################################################################################
#####  in total_dma_flag3 dataset, only minorities/complements within a DMA whose potential_new_controls_flag 
#####        is equal to 1 for both  minorities and their complements are shown in this dataset. 
###################################################################################################################

def total_dma_flag3_dataset_qualifier(total_dma_flag3):
    """ 
    Making sure that both minority controls passed the potential new control flag
    if not they are removed from the df_missing dataframe. 
    """

    dma_missing_list = total_dma_flag3.dma_code.unique()
    minorities = ["BLACK_RACE", "HISPANIC"]

    for dma in dma_missing_list:
      for minority in minorities:
        if len(total_dma_flag3.loc[(total_dma_flag3.dma_code == dma) & 
        (total_dma_flag3.control.str.contains(minority)),"potential_new_controls_flag"].unique()) == 2:
          print(dma,minority)
          total_dma_flag3 = total_dma_flag3[~((total_dma_flag3.dma_code==dma) & (total_dma_flag3.control.str.contains(minority)))]
    return total_dma_flag3

# COMMAND ----------

# MAGIC %md ### Function to Expand the Dataset with Missing controls

# COMMAND ----------

###################################################################################################################
#####  Function that creates county_flag3 from the total_dma_flag3 by exapnding it county by county
###################################################################################################################

def expand_aggregated_data_flag3_all():
    county_flag3 = pd.DataFrame()
    
    for index,row in total_dma_flag3.iterrows():
      dma = row['dma_code']
      minority = row['control']
      if minority == 'GEOGRAPHY_BLACK_RACE':
          min_filter = sql_minority_filter[0]
      elif minority == 'GEOGRAPHY_NON-BLACK_RACE':
          min_filter = sql_minority_filter[1]
      elif minority == 'GEOGRAPHY_HISPANIC':
          min_filter = sql_minority_filter[2]
      elif minority == 'GEOGRAPHY_NON-HISPANIC':
          min_filter = sql_minority_filter[3]
      else:
          min_filter = ''
        
      raw_filtered_data = filter_data_func(dma,min_filter)
      raw_filtered_data_county = raw_filtered_data.withColumn('county_group',raw_filtered_data['NMR_STCTY'])
      raw_filtered_data_county.createOrReplaceTempView("raw_filtered_data_county")
      aggregated_data_county = calculate_ess('raw_filtered_data_county',min_filter)
      aggregated_data_county['STATE_LOCAL_COUNTY_CODE'] = aggregated_data_county['parameter'].astype(str).str[3:]
      aggregated_data_county['STATE_LOCAL_COUNTY_CODE'] = aggregated_data_county['STATE_LOCAL_COUNTY_CODE'].astype(str)
      aggregated_data_county1 = aggregated_data_county[['STATE_LOCAL_COUNTY_CODE','control','parameter','ess']]
      aggregated_data_county_with_name = local_county_table.merge(aggregated_data_county1,how='left',left_on=['STATE_LOCAL_COUNTY_CODE'],right_on=                 ['STATE_LOCAL_COUNTY_CODE'])

      if True in (aggregated_data_county_with_name['ess'].isnull().tolist()):
          print('For DMA '+ str(dma)+ ' at '+minority+' level, '+'flag 3 was raised. However, the effective sample size of some counties this year are 0')
      
      aggregated_data_county_with_name['ess'] = aggregated_data_county_with_name['ess'].fillna(0)
      county_flag3 = pd.concat([county_flag3,aggregated_data_county_with_name])
    county_flag3 = county_flag3.reset_index(drop=True)
    return county_flag3

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 4: Implement the Functions and Create the Datasets

# COMMAND ----------

# MAGIC %md ### The Algorithm to Implement All Coded Functions

# COMMAND ----------

###################################################################################################################
#####  In the following loop, all functions are applied and datasets county_group_flag1_flag2,total_dma_flag3
#####       and county_flag3 are created. total_dma_flag3 and county_flag3 might be empty
###################################################################################################################

county_group_flag1_flag2 = pd.DataFrame()
total_dma_flag3 = pd.DataFrame()
county_flag3 = pd.DataFrame()

for min_filter in sql_minority_filter:
    
    if min_filter == sql_minority_filter[0] or min_filter == sql_minority_filter[1]:
        con_filter = 'GEOGRAPHY_BLACK_RACE'

    elif min_filter == sql_minority_filter[2] or min_filter == sql_minority_filter[3]:
        con_filter = 'GEOGRAPHY_HISPANIC'

    else:
        con_filter = 'GEOGRAPHY'

    for dma in dma_codes.split(','):
      
        local_county_table = pull_data_local_county(dma)
        print(dma,min_filter)
        if historical_geography.filter((historical_geography['dma_code'] == dma) & (historical_geography['control'] == con_filter)).count() > 0:
            raw_filtered_data = filter_data_func(dma,min_filter)
            raw_filtered_data_gr = join_county_group()
            raw_filtered_data_gr.createOrReplaceTempView('raw_filtered_data_gr')
            raw_filtered_data_gr.cache()
            
            county_group_dict = create_county_group_dict()
            county_name_group_dict,county_ess_group_dict = collect_county_names_and_county_ess()
            
            aggregated_data_flag1_flag2 = calculate_ess('raw_filtered_data_gr',min_filter)
            aggregated_data_flag1_flag2['county_group_names'] = aggregated_data_flag1_flag2.parameter.map(replace_val1)
            aggregated_data_flag1_flag2['county_group_ess'] = aggregated_data_flag1_flag2.parameter.map(replace_val2)
            aggregated_data_flag1_flag2.parameter = aggregated_data_flag1_flag2.parameter.map(replace_val)
            aggregated_data_flag1_flag2['not_enough_sample_size_flag'] = aggregated_data_flag1_flag2.ess.map(not_enough_sample_size)
            aggregated_data_flag1_flag2['potential_more_granular_assignment_flag'], aggregated_data_flag1_flag2['ess_nomax'] =zip(*aggregated_data_flag1_flag2['parameter'].apply(lambda x: check_granularity(x)))
    
            county_group_flag1_flag2 = pd.concat([county_group_flag1_flag2, aggregated_data_flag1_flag2])
            
        else:
            raw_filtered_data = filter_data_func(dma,min_filter)
            raw_filtered_data_gr=raw_filtered_data.withColumn('county_group',lit('All counties'))#.withColumn('PPM_METRO_CODE',lit('All PPM'))
            raw_filtered_data_gr.createOrReplaceTempView("raw_filtered_data_gr")
            aggregated_data_flag3 = calculate_ess('raw_filtered_data_gr',min_filter)
            
            aggregated_data_flag3['potential_new_controls_flag'] = potential_new_controls(aggregated_data_flag3)
            total_dma_flag3 = pd.concat([total_dma_flag3,aggregated_data_flag3])
            continue

# COMMAND ----------

# MAGIC %md ### The Algorithm to Check if both Minorities and Complements of Total_dma_flag3 Exceed 29.01

# COMMAND ----------

county_group_flag1_flag2 = county_group_flag1_flag2.reset_index(drop=True)
if len(total_dma_flag3) != 0:
    total_dma_flag3 = total_dma_flag3.reset_index(drop=True)
    total_dma_flag3 = total_dma_flag3_dataset_qualifier(total_dma_flag3)
    if len(total_dma_flag3) != 0:
        county_flag3 = expand_aggregated_data_flag3_all()

# COMMAND ----------

# MAGIC %md ### Converting Pandas to Spark Dataframes

# COMMAND ----------

county_group_flag1_flag2.ess_nomax = county_group_flag1_flag2.ess_nomax.astype(str)
county_group_flag1_flag2 = sqlContext.createDataFrame(county_group_flag1_flag2)
county_group_flag1_flag2.createOrReplaceTempView('county_group_flag1_flag2')

if len(total_dma_flag3) != 0:
    total_dma_flag3 = sqlContext.createDataFrame(total_dma_flag3)
    total_dma_flag3.createOrReplaceTempView('total_dma_flag3')
    
if len(county_flag3) != 0:
    county_flag3.control = county_flag3.control.astype(str)
    county_flag3.parameter = county_flag3.parameter.astype(str)
    county_flag3 = sqlContext.createDataFrame(county_flag3)
    county_flag3.createOrReplaceTempView('county_flag3')

# COMMAND ----------

display(county_group_flag1_flag2)

# COMMAND ----------

display(total_dma_flag3)

# COMMAND ----------

display(county_flag3)

# COMMAND ----------

# MAGIC %md ### Create historical_geography_flag1_flag2 Dataset

# COMMAND ----------

###################################################################################################################
#####  Function that creates Create historical_geography_flag1_flag2 Dataset through
#####         joining county_group_flag1_flag2 table with the geography group CSV file 
###################################################################################################################

s='''
SELECT 
    a.*,b.not_enough_sample_size_flag,b.potential_more_granular_assignment_flag
FROM
    historical_geography a
LEFT JOIN
    county_group_flag1_flag2 b
ON
    1=1
    AND a.dma_code = b.dma_code
    AND a.control = b.control
'''
historical_geography_flag1_flag2 = spark.sql(s)
historical_geography_flag1_flag2.createOrReplaceTempView("historical_geography_flag1_flag2")
# display(historical_geography_flag1_flag2)

# COMMAND ----------

display(historical_geography_flag1_flag2)

# COMMAND ----------

if please_export == True:
    sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")
    county_group_flag1_flag2.write.parquet(override_dir_county_group_flag1_flag2, mode='overwrite')
    
    historical_geography_flag1_flag2.write.parquet(override_dir_historical_geography_flag1_flag2, mode='overwrite')
    if len(total_dma_flag3) != 0:
        total_dma_flag3.write.parquet(override_dir_total_dma_flag3, mode='overwrite')
    if len(county_flag3) != 0:
        county_flag3.write.parquet(override_dir_county_flag3, mode='overwrite')
      

# COMMAND ----------

# MAGIC %md ### Clean Work Space

# COMMAND ----------

for table in cached_tables:
    uncache_tbl_sql = "UNCACHE TABLE {}".format(table)
    spark.sql(uncache_tbl_sql)
