# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Geography Effective Sample Size Calculator
# MAGIC 
# MAGIC ----------
# MAGIC 
# MAGIC **PURPOSE:**
# MAGIC > To compute the geography groups effective sample size, using tables from the media data lake, for a supplied date range, a selected DMAs and sample type. This notebook is used along with the **Geography Groups Assignment Qualifier** to computed the effective sample size of proposed county groups.
# MAGIC 
# MAGIC **USER INPUTS:** 
# MAGIC 
# MAGIC >* Cell #1 Inputs (in command 2)
# MAGIC 
# MAGIC >> 1. **start_date**
# MAGIC >>  - The beginning of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC >> 2. **end_date**
# MAGIC >>  - The ending of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC >> 3. **sample_type** 
# MAGIC >>  - Market type of the analysis, provided as a string, the acceptable market types are 'SET_METER+PPM' or 'LPM+PPM'. The software cannot be executed for LPM plus PPM and set meter plus ppm markets simultaneously.
# MAGIC >> 4. **dma_code**
# MAGIC >  - this should be a Python string of one and only one dma, e.g. '501'.
# MAGIC >  - lmp plus ppm market codes are '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC >  - set meter plus ppm market codes are '515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839'
# MAGIC >> 5. **ue_table_measurement_period**
# MAGIC >>  - The period for which UEs are pulled up from the MDL, provided as a string, in the following format **monthyy**
# MAGIC >>  - The software only pulls up ues with the maximum load id.
# MAGIC >>  - examples are 'Feb18' or 'May18'
# MAGIC 
# MAGIC >* Cell #2 Inputs (in command 9)
# MAGIC 
# MAGIC >> 1. **target_population**
# MAGIC >>  - Specify the demographic of interest (it could be one of the levels 'BLACK', 'NON-BLACK', 'HISPANIC', 'NON-HISPANIC', and 'DMA')
# MAGIC >> 2. **cg_list**
# MAGIC >>  - Specify NMR counties of interest (an example would be [[29013,31071,29023,31005],[31119,31027]])
# MAGIC 
# MAGIC **DATA SOURCES:**
# MAGIC > | schema               | table                                                        |
# MAGIC   |----------------------|--------------------------------------------------------------|
# MAGIC   | dsci_tam             | universe_estimates                                           |
# MAGIC   | dsci_tam             | household_characteristics                                    |
# MAGIC 
# MAGIC **AN EXAMPLE RUN:**
# MAGIC >* Input Cell #1
# MAGIC >><pre>
# MAGIC start_date                    = '2018-03-01'
# MAGIC end_date                      = '2018-03-02'
# MAGIC sample_type                   = 'LPM+PPM'
# MAGIC dma_code                      = '501'
# MAGIC ue_table_measurement_period   = 'May18' 
# MAGIC </pre>
# MAGIC >* Input Cell #2
# MAGIC >><pre>
# MAGIC target_population             = 'NON-BLACK'
# MAGIC cg_list                       = [[29013,31071,29023,31005],[31119,31027]] 
# MAGIC </pre>
# MAGIC 
# MAGIC **CODE DEVELOPERS:**
# MAGIC >* MJ PAKDEL
# MAGIC >* JONATHAN OUEGNIN
# MAGIC  

# COMMAND ----------

# MAGIC %md
# MAGIC # Input Cell #1

# COMMAND ----------

start_date                    = '2018-03-01'
end_date                      = '2018-03-01'
sample_type                   = 'SET_METER+PPM'
dma_code                      = '515'
ue_table_measurement_period   = 'May18' 

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Validate Sample Type User Input

# COMMAND ----------

sample_type = sample_type.upper()

if (sample_type != "LPM+PPM") and (sample_type != "SET_METER+PPM"):
    dbutils.notebook.exit("Invalid Sample Type Input")

# COMMAND ----------

# MAGIC %md ### Variable Definition

# COMMAND ----------

if sample_type == 'SET_METER+PPM':
    sample_type='''(
      (npm_sample_ind='Y' and smm_sample_ind='N' and national_daily_status_ind in (1,3))
      or ((smm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4)))
      )'''
  
elif sample_type == 'LPM+PPM':
    sample_type="((lpm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4)))"
  
from pyspark.sql.types import StructField, StringType, StructType

schemaString = "dma_code control parameter effective_sample_size"
fields = [StructField(field_name, StringType(), True) for field_name in schemaString.split()]
schema = StructType(fields)

# COMMAND ----------

# MAGIC %md ### Pull TGB2 Data

# COMMAND ----------

s = '''
SELECT 
    DMA_CODE,INTAB_DATE,HOUSEHOLD_ID,NMR_STCTY,
    CASE 
          WHEN HOH_ORIGIN_CODE = '1' THEN 'NON-HISPANIC'
          ELSE 'HISPANIC' 
   END AS ORIGIN,
   CASE
          WHEN HH_BLACK_IND = 'Y' THEN 'BLACK_RACE'
          ELSE 'NON-BLACK_RACE'
        END AS BLACK,
   CASE WHEN PPM_METRO_CODE=1 THEN 'PPM Metro 1'
        WHEN PPM_METRO_CODE=2 THEN 'PPM Metro 2'
        ELSE 'PPM Non-Metro' END AS PPM_METRO_CODE
FROM 
    dsci_tam.household_characteristics 
WHERE 
    1=1
    AND intab_date between '{start_date}' and '{end_date}'
    AND dma_code in ({dma_codes})
    AND {sample_type}
    AND HH_BROADBAND_ONLY_IND!='Y'
'''.format(dma_codes=dma_code,start_date=start_date,end_date=end_date,sample_type=sample_type)

tgb2_file = spark.sql(s)
tgb2_file.createOrReplaceTempView('tgb2_file')
tgb2_file.cache()
tgb2_file.count()
# display(tgb2_file)

# COMMAND ----------

# MAGIC %md ### Computing Intab Counts per intab_date/dma_code/ppm_metro_code

# COMMAND ----------

s = '''
SELECT
    INTAB_DATE,
    DMA_CODE,
    PPM_METRO_CODE,
    count(*) as intab_count
FROM
    tgb2_file
GROUP BY
    INTAB_DATE,DMA_CODE,PPM_METRO_CODE
ORDER BY
    INTAB_DATE,DMA_CODE,PPM_METRO_CODE
'''
intab_counts = spark.sql(s)
intab_counts.createOrReplaceTempView('intab_counts')
intab_counts.cache()
intab_counts.count()
# display(intab_counts)

# COMMAND ----------

# MAGIC %md ### Pull up UE Data  from the MDL per dma_code/ppm_metro_code

# COMMAND ----------

s = ''' 
SELECT 
    geography_code as dma_code,market_break,universe_estimate,load_id
FROM
    dsci_tam.universe_estimates 
WHERE 
    measurement_period='{ue_period}'
    AND ue_type='H'
    AND ((market_break="PPM Metro 1" AND market_break_description="TV Household") OR 
       (market_break="PPM Metro 2" AND market_break_description="TV Household") OR 
       (market_break="PPM Non-Metro" AND market_break_description="TV Household"))
ORDER BY
    dma_code,market_break
    '''.format(ue_period=ue_table_measurement_period)
ue_table = spark.sql(s)
ue_table = ue_table.filter(ue_table.load_id == max([x.load_id for x in ue_table.select('load_id').distinct().collect()])).drop('load_id')
ue_table.createOrReplaceTempView("ue_table")
ue_table.cache
ue_table.count()
# display(ue_table)

# COMMAND ----------

# MAGIC %md ### Compute Intab Design Weights per intab_date/dma_code/ppm_metro_code

# COMMAND ----------

s = '''
SELECT --count(*)
    A.*,B.universe_estimate,round(B.universe_estimate/A.intab_count,4) as weight
FROM
    intab_counts A
JOIN
    ue_table B
WHERE
    A.DMA_CODE=B.dma_code
    AND A.ppm_metro_code=B.market_break
ORDER BY
    INTAB_DATE,A.DMA_CODE,A.PPM_METRO_CODE
'''

calculated_design_weight = spark.sql(s)
calculated_design_weight.createOrReplaceTempView('calculated_design_weight')
calculated_design_weight.cache()
calculated_design_weight.cache()
display(calculated_design_weight)

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Join TGB2 and Weight Table

# COMMAND ----------

s = '''
  SELECT 
        a.*,b.weight
        
  FROM
      tgb2_file AS a
      
  JOIN 
      calculated_design_weight AS b
  ON 
      a.dma_code=b.dma_code
      AND a.PPM_METRO_CODE=b.PPM_METRO_CODE
      AND a.intab_date=b.intab_date
      
  
   '''

tgb2_file_w = spark.sql(s).repartition(16)
sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")
# tgb2_file_w = tgb2_file_w.drop('NMR_STCTY','LPM_SAMPLE_IND','SMM_SAMPLE_IND','PPM_SAMPLE_IND')
tgb2_file_w.createOrReplaceTempView('tgb2_file_w')
tgb2_file_w.cache()
tgb2_file_w.count()
# display(tgb2_file_w)

# COMMAND ----------

# MAGIC %md ### Function to Calculate Effective Sample Size

# COMMAND ----------

def effective_sample_size_calulator(target_population,cg_list):
    effective_sample_size_all=sqlContext.createDataFrame(sc.emptyRDD(),schema) 
    if target_population=='DMA':
        minority_filter='AND 1=1'
    elif target_population=='BLACK':
        minority_filter='AND BLACK="BLACK_RACE"'
    elif target_population=='NON-BLACK':
        minority_filter='AND BLACK="NON-BLACK_RACE"'
    elif target_population=='HISPANIC':
        minority_filter='AND ORIGIN="HISPANIC"'
    elif target_population=='NON-HISPANIC':
        minority_filter='AND ORIGIN="NON-HISPANIC"'
   
  s='''
      SELECT 
          dma_code,
          CONCAT("GEOGRAPHY_",'{target_pop}') AS control,
          CONCAT("CG ",'{cg_list}') AS parameter,
          ROUND(MEAN(PESS),2) AS effective_sample_size
      FROM (
         SELECT
              INTAB_DATE,
              dma_code,
              'group' as group,
              ((sum(weight)*sum(weight))/sum(weight*weight)) AS PESS
          FROM 
              tgb2_file_w 
          WHERE
              NMR_STCTY in ({cg_list})
              {minority_filter}
          GROUP BY
              INTAB_DATE,dma_code,group
              )
      GROUP BY
           dma_code,control,parameter'''  
  for list_county in cg_list:
      effective_sample_size=spark.sql(s.format(cg_list = ', '.join([str(x) 
                                   for x in list_county]),target_pop=target_population,minority_filter=minority_filter))
      effective_sample_size_all=effective_sample_size_all.union(effective_sample_size)
  return effective_sample_size_all

# COMMAND ----------

# MAGIC %md 
# MAGIC # Input Cell #2

# COMMAND ----------

target_population           = 'HISPANIC'
cg_list                     = [[ 29041, 31111, 6001, 31105],[29025],[12233,33443]]

# COMMAND ----------

display(effective_sample_size_calulator(target_population,cg_list))

# COMMAND ----------


