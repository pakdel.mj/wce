# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Sufficent Sample Size Qualifier Engine
# MAGIC > This notebook has to be executed as the third module of Weighting Control Evaluation (WCE) software. The qualified controls created at the end of this module are used in the **UE Computation** module of WCE tool. For further information about the collapsing rules used in this notebook, refer to the <a href="https://docs.google.com/document/d/1hhF0-CkzZHeRzy7OEyteQb1B7mFLkcWka0U4vfmrAN0/edit">MWCFA_RSV_V1.6 </a> document.
# MAGIC 
# MAGIC **Purpose:**
# MAGIC 
# MAGIC >* To decide which potential **control/level combination** has the **sufficient sample size** of **29.01** or higher in order to qualify as a weighting control for a DMA, minorities/controls within a DMA and demo buckets within a DMA.
# MAGIC 
# MAGIC **LOW-LEVEL EXPLANATION OF WHAT NOTEBOOK DOES:** 
# MAGIC >This software can be broken into 5 sub-modules.
# MAGIC 
# MAGIC > **Part 1: Develop the Qualifier Functions**
# MAGIC 
# MAGIC >>* Purpose of this part is to develop functions that decide which level of granularity of a control is qualified for a DMA, minorities/complements within a DMA and demo buckets within a DMA. A control might also be completely dropped.
# MAGIC >>* Levels of all flowcharts in the specification document except the male and female flowcharts could be prioritized. For all flowcharts with known level priority, we developed the collapsed_level_determiner function. This function, for example, examines the most granular level for the age of householder (<25/25-34/35-44/45-54/55-64/65+), if that level is qualified, the collapsed_level_determiner stops and return that level as the selected level of granularity. Otherwise, it considers the second most granular level and so on until it finds the qualified level or drops the control completely.
# MAGIC >>* For male and female flowcharts, since level priority is not known, we had to code the whole flowchart. the class Node is developed to pick the qualified level for only those two flowcharts.
# MAGIC 
# MAGIC > **Part 2: Translate the Flowcharts into Codes**
# MAGIC >>* Purpose of this part is to translate the flowcharts mentioned in the specification documentation into Python codes.
# MAGIC >>* Age of Head Flowchart for DMA with the following level priorities:
# MAGIC           <25/25-34/35-44/45-54/55-64/65+ (Level 1 Added 09/25/2010)								
# MAGIC            <35/35-44/45-54/55-64/65+ (Level 2)								
# MAGIC            <35/35-44/45-54/55+ (Level 3)								
# MAGIC            <35/35-54/55-64/65+ (Level 4)								
# MAGIC            <35/35-54/55+ (Level 5)								
# MAGIC            <55/55-64/65+ (Level 6)								
# MAGIC            <35/35+ (Level 7)								
# MAGIC            <55/55+ (Level 8)								
# MAGIC >>* Age of Head Flowchart for Minorities/Complements according to the table 8.2.2 of MWCFA_RSV_V1.6 document. Please note the fact that the minority/complement must not collapse more finely than the total DMA collapses. This flowchart is different from every other flowcahrts since the start point in this flowchart is contingent on the end poin in the  Age of Head Flowchart for DMA.
# MAGIC >>* Household Size Flowchart for DMA with the following level priorities:
# MAGIC           1/2/3/4/5/6+ (Level 1 Updated to 10-way July 2012)								
# MAGIC            1/2/3/4/5+ (Level 2)								
# MAGIC            1/2/3-4/5/6+ (Level 3)								
# MAGIC            1/2/3-4/5+ (Level 4)								
# MAGIC            1-2/3/4/5/6+ (Level 5)								
# MAGIC            1-2/3/4/5+ (Level 6)								
# MAGIC            1-2/3-4/5/6+ (Level 7)								
# MAGIC            1/2/3+ (Level 8)								
# MAGIC            1-2/3-4/5+ (Level 9)								
# MAGIC            1-2/3+ (Level 10)	
# MAGIC >>* Household Size Flowchart for Minorities/Complements with the following level priorities:
# MAGIC           1/2/3-4/5/6+ (Level 1)
# MAGIC            1/2/3-4/5+ (Level 2)
# MAGIC            1-2/3-4/5/6+ (Level 3)
# MAGIC            1/2/3+ (Level 4)
# MAGIC            1-2/3-4/5+ (Level 5)
# MAGIC            1-2/3+ (Level 6)
# MAGIC >>* Enhanced Household Size Flowchart for Minorities/Complements was introduced at a later stage to prevent the houshold size 1 homes stand alone after the collapse. It only applied to some select DMAs with the following level priorities:
# MAGIC           1-2/3-4/5/6+ (Level 1)
# MAGIC            1-2/3-4/5+ (Level 2)
# MAGIC            1-2/3+ (Level 3)
# MAGIC >>* Cable Plus Flowchart for DMA according to the **8.2.7 Total: Cable Status / ADS Collapsing Rules** flowchart in MWCFA_RSV_V1.6 document
# MAGIC >>* Cable Plus Flowchart for Minorities/Complements according to the **8.2.8 Black/Non-Black: Cable Status / ADS Collapsing Rules** and **8.2.8.5 Hispanic/Non-Hispanic: Cable Status/ADS Collapsing Rules** flowcharts in MWCFA_RSV_V1.6 document
# MAGIC >>* ADS Flowchart for DMA and Minorities/Complements with only one **YES/NO** level
# MAGIC >>* Presence of Non Adults Flowchart for DMA with the following level priorities:
# MAGIC           None/Only 0-11/Any 12-17
# MAGIC            None/Any 0-17 (2-way)
# MAGIC >>* Language Class Flowchart for DMA with the following level priorities:    
# MAGIC 
# MAGIC >>           Language - Non-Hispanic/Spanish Only/Mostly Spanish/Spanish English Equal/Mostly English/Only English (Level 1)						
# MAGIC            Non-Hispanic/Spanish Only/Mostly Spanish/Spanish English Equal/English Dominant (Level 2)								
# MAGIC            Non-Hispanic/Spanish Dominant/Spanish English Equal/Mostly English/Only English (Level 3)								
# MAGIC            Non-Hispanic/Spanish Dominant/Spanish English Equal/English Dominant (Level 4)								
# MAGIC            Non-Hispanic/Spanish Dominant BiLingual/Mostly English/Only English (Level 5)								
# MAGIC            Non-Hispanic/Spanish Only/Mostly Spanish/English Dominant BiLingual (Level 6)								
# MAGIC            Non-Hispanic/Spanish Dominant BiLingual/English Dominant (Level 7)								
# MAGIC            Non-Hispanic/Spanish Dominant/English Dominant BiLingual (Level 8)								
# MAGIC            (Language based on Persons 2+ added December 30, 2002)								
# MAGIC            (Spanish Dominant BiLingual added May 2009)	
# MAGIC >>* PPM Metro Flowchart for DMA with the following level priorities: 
# MAGIC           ppm metro1/ppm metro2/ppm remainder
# MAGIC            ppm metro (ppm metro1 & 2 combined)/ppm remainder
# MAGIC            ppm metro1/ppm remainder
# MAGIC            ppm metro1
# MAGIC >>* Children Aged 2-17 Flowchart for Demo Buckets according to the **8.3.2 Non-Adult Collapsing Rules for Minorities and Complements** flowchart in MWCFA_RSV_V1.6 document
# MAGIC >>* Male aged 18+ Flowchart for Demo Buckets according to the **8.3.3 Male Collapsing Rules for Ethnicity = All** flowchart in MWCFA_RSV_V1.6 document
# MAGIC >>* Female aged 18+ Flowchart for Demo Buckets according to the **8.3.5 Female Collapsing Rules for Ethnicity = All** flowchart in MWCFA_RSV_V1.6 document
# MAGIC >>* Black Flowchart for DMA with only one **YES/NO** level
# MAGIC >>* Origin Flowchart for DMA with only one **YES/NO** level (Hispanic/Non-hispanic)
# MAGIC >>* Asian Flowchart for DMA with only one **YES/NO** level
# MAGIC >>* Native American Flowchart for DMA with only one **YES/NO** level
# MAGIC >>* DVR Flowchart for DMA with only one **YES/NO** level
# MAGIC >>* Operable Sets Flowchart for  DMA (Only Code Readers) with only one **YES/NO** level
# MAGIC >>* Measurable Sets Flowchart for  DMA (Only LPM/Set Meter) with the following level priorities:
# MAGIC           1/2/3/4+ (Level 1 Added September 2016)								
# MAGIC            1/2+ (Level 2)
# MAGIC            
# MAGIC > **Part 3: Apply the Flowcharts to DMAs and Minorities/Complements within a DMA**
# MAGIC >>* Purpose of this part is to identify the granularity level of potential controls for DMAs and minorities/complements within a DMA.
# MAGIC >>* dma_controls dictionary includes all the controls that we should examine for total DMA
# MAGIC >>* minority_controls dictionary includes all the controls that we should examine for minorities/complements within a DMA
# MAGIC >>* Read the Output of Effective_Sample_Size_Computation Module
# MAGIC >>* Apply Required Flowcharts to DMA controls. The chosen level for each control is added to a dictionary named collapsed_controls. keys of this dictionary are dma_codes
# MAGIC >>* Apply Required Flowcharts to Minorities/Complements controls. The chosen level for each control is added to the collapsed_controls dictionary
# MAGIC >>* Since a different level might have been picked for a minority and its complement, there is a piece of code that picks the least granular level between the minority and complement for every control and then assigns both the minority and complement the chosen least granular level.
# MAGIC 
# MAGIC > **Part 4: Apply the Flowcharts to Demo Buckets within a DMA**
# MAGIC >>* Purpose of this part is to identify the granularity level of demo buckets within a DMA. 
# MAGIC >>* Flowchart for Children aged 2 to 17, Males aged 18+ and Females aged 18+ are applied to demo buckets within a DMA.
# MAGIC >>* Unlike the controls for a minority and its complement within a DMA which are separately evaluated, demo buckets of minorities and their complements are examined simultaneously within a DMA. Consequently, there is no need to pick the least granular level later between the minority and its complement. 
# MAGIC >>* The chosen level for each control is added to the collapsed_controls dictionary
# MAGIC 
# MAGIC > **Part 5: Implementing the Required Adjustments**
# MAGIC >>* Purpose of this part is to implement all the required adjustments that are easier to be done here rather where we coded the flowcharts.
# MAGIC >>* For the code reader markets, only operable sets stays as a control while the measurable set is dropped. For the set meter and LPM markets, the vice versa is true. 
# MAGIC >>* ADS is dropped as a control when cable plus is CABLE_PLUS_YES/BROADCAST_ONLY for minorities/complements within a DMA.
# MAGIC >>* Demo buckets are present at the most granular level for all DMAs automatically without any investigation. 
# MAGIC >>* For the minorities and complements mentioned in the list below, the selected level based on the **Enhanced Household Size Flowchart for Minorities/Complements** remains and the level based on **Household Size Flowchart for Minorities/Complements** is dropped. For every other minorities and complements, the vice versa is true. 
# MAGIC           DMA	RIM 
# MAGIC            501	HOUSEHOLD SIZE WITHIN HISPANIC/NON-HISPANIC
# MAGIC            501	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            602	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            504	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            623	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            511	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            524	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            618	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            505	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            609	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            517	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            512	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            544	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            560	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            640	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            556	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            622	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK
# MAGIC            630	HOUSEHOLD SIZE WITHIN BLACK/NON-BLACK	
# MAGIC            
# MAGIC > **Part 6: Change the Output Format and Write the Output to S3**
# MAGIC >>* Purpose of this part it to implement some changes in the fomat of the qualified weighting controls and then save the data frame as a parquet file to S3
# MAGIC 
# MAGIC **USER INPUTS:** 
# MAGIC > 1. **input_controls_with_ess_dir**
# MAGIC >  - The directory of the file with effective sample size of all potential weighting controls, saved to S3 with a Parquet format, provided as a string.
# MAGIC >  - This file is the outputed by the Effective Sample Size Computation module of WCE tool.
# MAGIC > 2. **please_export**
# MAGIC >  - Provided as a string, set this varialbe to 'True' if you want to save the data in the Optional Overrides sections and 'False' otherwise. 
# MAGIC > 3. **override_dir**
# MAGIC >  - the directory in which the software saves the output with a parquet fomat
# MAGIC  
# MAGIC **THE OUTPUT FORMAT:**
# MAGIC > | col_name                   | data_type | ex_results                            |
# MAGIC   |----------------------------|-----------|---------------------------------------|
# MAGIC   | dma_code                   | bigint    | 501                                   |
# MAGIC   | weightin_control           | string    | ADS                                   |
# MAGIC   | level                      | string    | ADS_YES/ADS_NO                        |
# MAGIC   
# MAGIC 
# MAGIC **DATA SOURCES:**
# MAGIC > | schema               | table                                                                                   |
# MAGIC   |----------------------|-----------------------------------------------------------------------------------------|
# MAGIC   | --------             | a Parquet file that is created by Effective Sample Size Computation Module of WCE tool  |
# MAGIC 
# MAGIC   
# MAGIC **AN EXAMPLE RUN:**
# MAGIC > <pre>
# MAGIC input_controls_with_ess_dir        = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/oujo8001/WCE/final.output.module1.parquet'
# MAGIC please_export                      = 'True'
# MAGIC override_dir                       = 's3://useast1-nlsn-w-digital-dsci-dev/data/users/oujo8001/WCE/collapsed.controls.parquet'
# MAGIC </pre>
# MAGIC 
# MAGIC **AN EXAMPLE OUTPUT:**
# MAGIC > <pre>
# MAGIC <table class="confluenceTable relative-table" style="width: 472px;" data-resize-percent="41.5785764622974">
# MAGIC <tbody>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">dma</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">weighting_control</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 247px;">level</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 10px;">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 10px;">&nbsp;</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">ADS</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 247px;">ADS_YES</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 10px;">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 10px;">&nbsp;</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">ADS</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 247px;">ADS_NO</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 10px;">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 10px;">&nbsp;</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_&lt;35</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_35-44</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_45-54</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_55-64</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_65+</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_&lt;35</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_35-44</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_45-54</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_55-64</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_65+</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_&lt;35</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_35-44</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_45-54</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_55-64</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected" style="width: 35px;">501</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 211px;">AGE_OF_HEAD_HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected" style="width: 267px;" colspan="3">AGE_OF_HOUSEHOLDER_65+</td>
# MAGIC </tr>
# MAGIC </tbody>
# MAGIC </table>
# MAGIC </pre>
# MAGIC 
# MAGIC **CODE DEVELOPERS:**
# MAGIC >* MJ PAKDEL
# MAGIC >* JONATHAN OUEGNIN

# COMMAND ----------

# MAGIC %md
# MAGIC ### Input

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################

# start_date = '2018-03-01'
# end_date = '2018-03-02'
# sample_type = 'LPM+PPM'

# COMMAND ----------

##########################################################
#####    USER INPUTS (called in another notebook)    #####
##########################################################

start_date                        = str(dbutils.widgets.get('start_date'))
end_date                          = str(dbutils.widgets.get('end_date'))
sample_type                       = str(dbutils.widgets.get('sample_type'))

# COMMAND ----------

# Specify whcih DMAs belong to which market type. Market types are used down the road in adjustments
set_meter_markets_list = [515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839]
lpm_markets_list = [602,820,511,539,623,501,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751]
code_reader_markets_list = [513, 519, 532, 540, 563, 566, 632, 669, 686, 789, 810, 811, 855, 866, 744]

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Validate Sample Type User Input

# COMMAND ----------

sample_type = sample_type.upper()

if (sample_type != "LPM+PPM") and (sample_type != "SET_METER+PPM"):
    dbutils.notebook.exit("Invalid Sample Type Input")

# COMMAND ----------

# Directory path of module 2 dataframe result 
input_controls_with_ess_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/effective_sample_size_data_' + start_date.replace('-','_') + '_' + end_date.replace('-','_') + '_' + sample_type.replace('+','_') + '.parquet'

# Directory path for module output 
final_output_module3_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/collapsed_controls_' + start_date.replace('-','_') + '_' + end_date.replace('-','_') + '_' + sample_type.replace('+','_') + '.parquet'

# COMMAND ----------

# MAGIC %md 
# MAGIC # Part1: Develop the Qualifier Functions

# COMMAND ----------

# MAGIC %md
# MAGIC ### Qualifier Function for the Flowcharts with Known Levels Priority 

# COMMAND ----------

#######################################################################################################################
#####        which level of granularity should be picked for all weighting controls (except males and females)
#####           is decided by this function
#######################################################################################################################
def collapsed_level_determiner(option):
    """This function checks if each option dictionary's weighting control level has an effective sample size greater than the threshold (29.1)"""
    
    for i in sorted(list(option.keys())):
      
      if not reduced_df.empty:
        data_subset=(reduced_df[reduced_df['parameter'].isin(option[i])]['effective_sample_size']>home_threshold).values
        
        if False not in data_subset and len(data_subset)!=0:
          return i,option[i]
          break

# COMMAND ----------

# MAGIC %md
# MAGIC ### Qualifier Function for the Flowcharts with Unknown Levels Priority

# COMMAND ----------

#######################################################################################################################
#####        which level of granularity should be picked for male and female controls
#####           is decided by this function
#######################################################################################################################
class node():
    """
    The goal of this class is to store outputs for the cases when conditions are.t or.f along with an array of the data within 
    each node of the flowchart. Typically, most nodes point to other nodes which can be defined within the true/false statements.
    """
    
    def __init__(self):
        '''Rename true/false to t/f for brevity and ease of reading when manually enterning flowcharts'''
      
        self.t=None
        self.f=None
        self.data=None
        self.collapsed_primary_control=None

# COMMAND ----------

# MAGIC %md
# MAGIC #Part2: Translate the Flowcharts into Codes

# COMMAND ----------

# MAGIC %md
# MAGIC ### Age of Head Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####        Age of head for total DMA could be prioritized according to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def AGE_OF_HEAD_FLOWCHART():
    """
    This function define the rules of the Age of Head flowchart for Total DMA.
    The Age of Head flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
    
    level1= ['AGE_OF_HOUSEHOLDER_<25', 'AGE_OF_HOUSEHOLDER_25-34', 'AGE_OF_HOUSEHOLDER_35-44',
            'AGE_OF_HOUSEHOLDER_45-54','AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+']
    level2=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35-44',
                                  'AGE_OF_HOUSEHOLDER_45-54','AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+']
    level3=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35-44',
                                  'AGE_OF_HOUSEHOLDER_45-54','AGE_OF_HOUSEHOLDER_55+']
    level4=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35-54',
                                  'AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+']
    level5=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35-54','AGE_OF_HOUSEHOLDER_55+']
    level6=                      ['AGE_OF_HOUSEHOLDER_<55', 'AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+']
    level7=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35+']
    level8=                      ['AGE_OF_HOUSEHOLDER_<55', 'AGE_OF_HOUSEHOLDER_55+']
    level9=                      None
    option1={1:level1,2:level2,3:level3,4:level4,5:level5,6:level6,7:level7,8:level8}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Age of Head Flowchart for Minorities/Complements

# COMMAND ----------

######################################################################################################################
#####        Age of head for minorities/complements within a DMA could be prioritized according 
#####              to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#####        this flowchart is contingent on the end poin in the Age of Head Flowchart for DMA.
#######################################################################################################################

def AGE_OF_HEAD_MINORITY_FLOWCHART():
    """
    This function define the rules of the Age of Head flowchart for Minorities.
    The Age of Head flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
        
    level=primary_control_collapsed
    
  
    level1=['AGE_OF_HOUSEHOLDER_<25', 'AGE_OF_HOUSEHOLDER_25-34', 'AGE_OF_HOUSEHOLDER_35-44',
            'AGE_OF_HOUSEHOLDER_45-54','AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+']
    level2=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35-44',
                                  'AGE_OF_HOUSEHOLDER_45-54','AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+']
    level3=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35-44',
                                  'AGE_OF_HOUSEHOLDER_45-54','AGE_OF_HOUSEHOLDER_55+']
    level4=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35-54',
                                  'AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+']
    level5=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35-54','AGE_OF_HOUSEHOLDER_55+']
    level6=                      ['AGE_OF_HOUSEHOLDER_<55', 'AGE_OF_HOUSEHOLDER_55-64','AGE_OF_HOUSEHOLDER_65+']
    level7=                      ['AGE_OF_HOUSEHOLDER_<35', 'AGE_OF_HOUSEHOLDER_35+']
    level8=                      ['AGE_OF_HOUSEHOLDER_<55', 'AGE_OF_HOUSEHOLDER_55+']
    level9=                      None

    option1={1:level1,2:level2,3:level3,4:level4,5:level5,6:level6,7:level7,8:level8}
    option2={         2:level2,3:level3,4:level4,5:level5,6:level6,7:level7,8:level8}
    option3={                  3:level3,         5:level5,6:level6,7:level7,8:level8}
    option4={                           4:level4,5:level5,6:level6,7:level7,8:level8}
    option5={                                    5:level5,         7:level7,8:level8}
    option6={                                             6:level6,         8:level8}
    option7={                                                      7:level7         }
    option8={                                                               8:level8}
    
    if level==level1 and collapsed_level_determiner(option1) is not None :
      no,collapsed_level=collapsed_level_determiner(option1)
    elif level==level2 and collapsed_level_determiner(option2) is not None :
      no,collapsed_level=collapsed_level_determiner(option2)
    elif level==level3 and collapsed_level_determiner(option3) is not None :
      no,collapsed_level=collapsed_level_determiner(option3)
    elif level==level4 and collapsed_level_determiner(option4) is not None :
      no,collapsed_level=collapsed_level_determiner(option4)
    elif level==level5 and collapsed_level_determiner(option5) is not None :
      no,collapsed_level=collapsed_level_determiner(option5)
    elif level==level6 and collapsed_level_determiner(option6) is not None :
      no,collapsed_level=collapsed_level_determiner(option6)
    elif level==level7 and collapsed_level_determiner(option7) is not None :
      no,collapsed_level=collapsed_level_determiner(option7)
    elif level==level8 and collapsed_level_determiner(option8) is not None :
      no,collapsed_level=collapsed_level_determiner(option8)
    elif level==level9:
      collapsed_level=None
      no=None
    else:
      collapsed_level=None
      no=None
    
    
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Household Size Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####        household size for total DMA could be prioritized according to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def HOUSEHOLD_SIZE_FLOWCHART():
    """
    This function define the rules of the Household Size flowchart for Total DMA.
    The Household Size flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
        
    level1=                      ['1_PERSON','2_PERSONS','3_PERSONS','4_PERSONS','5_PERSONS','6+_PERSONS']
    level2=                      ['1_PERSON','2_PERSONS','3_PERSONS','4_PERSONS','5+_PERSONS']
    level3=                      ['1_PERSON','2_PERSONS','3-4_PERSONS','5_PERSONS','6+_PERSONS']
    level4=                      ['1_PERSON','2_PERSONS','3-4_PERSONS','5+_PERSONS']
    level5=                      ['1-2_PERSONS','3_PERSONS','4_PERSONS','5_PERSONS','6+_PERSONS']
    level6=                      ['1-2_PERSONS','3_PERSONS','4_PERSONS','5+_PERSONS']
    level7=                      ['1-2_PERSONS','3-4_PERSONS','5_PERSONS','6+_PERSONS']
    level8=                      ['1_PERSON','2_PERSONS','3+_PERSONS']
    level9=                      ['1-2_PERSONS','3-4_PERSONS','5+_PERSONS']
    level10=                     ['1-2_PERSONS','3+_PERSONS']
    level11=                     None
    option1={1:level1,2:level2,3:level3,4:level4,5:level5,6:level6,7:level7,8:level8,9:level9,10:level10}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Household Size Flowchart for Minorities/Complements

# COMMAND ----------

######################################################################################################################
#####        household size for some minorities/complements within a DMA could be prioritized according 
#####              to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################
def HOUSEHOLD_SIZE_MINORITY_FLOWCHART():
    """
    This function define the rules of the Household Size flowchart for Minorities.
    The Household Size flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
  
    level1=                      ['1_PERSON','2_PERSONS','3-4_PERSONS','5_PERSONS','6+_PERSONS']
    level2=                      ['1_PERSON','2_PERSONS','3-4_PERSONS','5+_PERSONS']
    level3=                      ['1-2_PERSONS','3-4_PERSONS','5_PERSONS','6+_PERSONS']
    level4=                      ['1_PERSON','2_PERSONS','3+_PERSONS']
    level5=                      ['1-2_PERSONS','3-4_PERSONS','5+_PERSONS']
    level6=                      ['1-2_PERSONS','3+_PERSONS']
    level7=                      None
    option1={1:level1,2:level2,3:level3,4:level4,5:level5,6:level6}
    
  
        
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Enhanced Household Size Flowchart for Minorities/Complements

# COMMAND ----------

######################################################################################################################
#####        Enhanced Household Size Flowchart for Minorities/Complements was introduced at a later stage to prevent 
#####              the houshold size 1 homes stand alone after the collapse. It only applied to some select DMAs
#####        Enhanced household size for minorities/complements within a DMA could be prioritized according 
#####              to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def ENHANCED_HOUSEHOLD_SIZE_MINORITY_FLOWCHART():
    """
    This function define the rules of the Household Size flowchart for Total DMA.
    The Household Size flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
        
    level1=                      ['1-2_PERSONS','3-4_PERSONS','5_PERSONS','6+_PERSONS']
    level2=                      ['1-2_PERSONS','3-4_PERSONS','5+_PERSONS']
    level3=                     ['1-2_PERSONS','3+_PERSONS']
    level4=                     None
    option1={1:level1,2:level2,3:level3}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Cable Plus Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####        cable plus for total DMA could be prioritized according to the levels in the following function
#####        this is the only control whose level of granularity is determined by 
#####             two variables instead of one(cable plus variable and cable variable)
#####        this control does not use the collapsed_level_determiner function
#######################################################################################################################

def CABLE_PLUS_FLOWCHART():
    """
    This function define the rules of the Cable Plus flowchart for Total DMA.
    The Cable Plus flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
  
    reduced_df_cable_plus=input_df[(input_df.dma_code==dma) & (input_df.control=='CABLE_PLUS')]['effective_sample_size'].tolist()
    reduced_df_cable=input_df[(input_df.dma_code==dma) & (input_df.control=='CABLE')]['effective_sample_size'].tolist()
#     print(reduced_df_cable_plus)
    reduced_df_cable_plus_status=[i>home_threshold for i in reduced_df_cable_plus]
    reduced_df_cable_status=[i>home_threshold for i in reduced_df_cable]
    
    if False not in reduced_df_cable_plus_status:
      return 1,['CABLE_PLUS_YES','BROADCAST_ONLY']
    elif False in reduced_df_cable_plus_status and False not in reduced_df_cable_status:
      return 2,['CABLE_YES','CABLE_NO']
    else:
      return None, None   

# COMMAND ----------

# MAGIC %md
# MAGIC ### Cable Plus Flowchart for Minorities/Complements

# COMMAND ----------

#######################################################################################################################
#####        cable plus for minorities/complements within a DMA could be prioritized according to the levels in the following function
#####        this is the only control whose level of granularity is determined by 
#####             two variables instead of one(cable plus variable and cable variable)
#####        this control does not use the collapsed_level_determiner function
#######################################################################################################################

def CABLE_PLUS_MINORITY_FLOWCHART():
    """
    This function define the rules of the Cable Plus flowchart for Minorities.
    The Cable Plus flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
  
#     print(minority_dict[control][1])
    reduced_df_cable_plus=input_df[(input_df.dma_code==dma) & (input_df.control=='CABLE_PLUS'+minority_controls[control][1])]\
                                  ['effective_sample_size'].tolist()
    reduced_df_cable=input_df[(input_df.dma_code==dma) & (input_df.control=='CABLE'+minority_controls[control][1])]\
                             ['effective_sample_size'].tolist()
#     print(reduced_df_cable_plus)
    reduced_df_cable_plus_status=[i>home_threshold for i in reduced_df_cable_plus]
    reduced_df_cable_status=[i>home_threshold for i in reduced_df_cable]
    
    if (control=='CABLE_PLUS_BLACK_RACE' or control=='CABLE_PLUS_NON-BLACK_RACE'):
      if False not in reduced_df_cable_plus_status:
        return 1,['CABLE_PLUS_YES','BROADCAST_ONLY']
      elif False in reduced_df_cable_plus_status and False not in reduced_df_cable_status:
        return 2,['CABLE_YES','CABLE_NO']
      else:
        return None, None 
    else:
      if False not in reduced_df_cable_plus_status:
        return 1,['CABLE_PLUS_YES','BROADCAST_ONLY']
      else:
        return None, None 
      

# COMMAND ----------

# MAGIC %md
# MAGIC ### ADS Flowchart for DMA and Minorities/Complements

# COMMAND ----------

#######################################################################################################################
#####        ADS for total DMA and for minorities/complements within a DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def ADS_FLOWCHART():
    """
    This function define the rules of the ADS flowchart.
    The ADS flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
    
    level1=                      ['ADS_YES','ADS_NO']
    level2=                      None
    option1={1:level1}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Presence of Non Adults Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####        Presence of Non Adults Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def PRESENCE_OF_NONADULTS_FLOWCHART():
    """
    This function define the rules of the Presence of Non Adults flowchart.
    The Presence of Non Adults flowchart rules are applied to a subset of the 
    data using the collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """ 
    
    level1=                      ['PRESENCE_OF_NONADULTS_NONE_<18','PRESENCE_OF_NONADULTS_ONLY_<12','PRESENCE_OF_NONADULTS_ANY_12-17']
    level2=                      ['PRESENCE_OF_NONADULTS_NONE_<18','ANY_CHILD']
    level3=                      None
    option1={1:level1,2:level2}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Language Class Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####        Language Class Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def LANGUAGE_FLOWCHART():
    """
    This function define the rules of the Language flowchart.
    The Language flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
        
    level1=                      ['NON-HISPANIC','SPANISH_ONLY','MOSTLY_SPANISH','SPANISH_ENGLISH_EQUAL','MOSTLY_ENGLISH','ENGLISH_ONLY']
    level2=                      ['NON-HISPANIC','SPANISH_ONLY','MOSTLY_SPANISH','SPANISH_ENGLISH_EQUAL','ENGLISH_DOMINANT']
    level3=                      ['NON-HISPANIC','SPANISH_DOMINANT','SPANISH_ENGLISH_EQUAL','MOSTLY_ENGLISH','ENGLISH_ONLY']
    level4=                      ['NON-HISPANIC','SPANISH_DOMINANT','SPANISH_ENGLISH_EQUAL','ENGLISH_DOMINANT']
    level5=                      ['NON-HISPANIC','SPANISH_DOMINANT_BILINGUAL','MOSTLY_ENGLISH','ENGLISH_ONLY']
    level6=                      ['NON-HISPANIC','SPANISH_ONLY','MOSTLY_SPANISH','ENGLISH_DOMINANT_BILINGUAL']
    level7=                      ['NON-HISPANIC','SPANISH_DOMINANT_BILINGUAL','ENGLISH_DOMINANT']
    level8=                      ['NON-HISPANIC','SPANISH_DOMINANT','ENGLISH_DOMINANT_BILINGUAL']
    level9=                      None
    option1={1:level1,2:level2,3:level3,4:level4,5:level5,6:level6,7:level7,8:level8}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### PPM Metro Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####        PPM Metro Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level of granularity is picked depends on how many ppm metro codes the dma has 
#####        this control does not use the collapsed_level_determiner function
#######################################################################################################################

def PPM_METRO_CODE_FLOWCHART():
    """
    This function define the rules of the ppm metro flowchart for Total DMA.
    The ppm metro flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
    if control=='PPM_METRO_CODE':
      if reduced_df[reduced_df['parameter']=='PPM Non-Metro'].reset_index().ix[0,'effective_sample_size']==0:
        if reduced_df[reduced_df['parameter']=='PPM Metro 1']        .reset_index().ix[0,'effective_sample_size']>home_threshold:
            return 1,['PPM METRO 1']
        else:
            return None, None
      elif reduced_df[reduced_df['parameter']=='PPM Metro 2'].reset_index().ix[0,'effective_sample_size']>0:
        if (reduced_df[reduced_df['parameter']=='PPM Metro 1'].reset_index().ix[0,'effective_sample_size']>home_threshold and
           reduced_df[reduced_df['parameter']=='PPM Metro 2'].reset_index().ix[0,'effective_sample_size']>home_threshold and
           reduced_df[reduced_df['parameter']=='PPM Non-Metro'].reset_index().ix[0,'effective_sample_size']>home_threshold):
             return 1,['PPM METRO 1','PPM METRO 2','PPM REMAINDER']
        elif (reduced_df[reduced_df['parameter']=='PPM Metro'].reset_index().ix[0,'effective_sample_size']>home_threshold and
             reduced_df[reduced_df['parameter']=='PPM Non-Metro'].reset_index().ix[0,'effective_sample_size']>home_threshold):
          return 2,['PPM METRO','PPM REMAINDER']
        else:
          return None, None
      else:
        if (reduced_df[reduced_df['parameter']=='PPM Metro 1'].reset_index()
            .ix[0,'effective_sample_size']>home_threshold and
            reduced_df[reduced_df['parameter']=='PPM Non-Metro'].reset_index().ix[0,'effective_sample_size']>home_threshold):
          return 1,['PPM METRO 1','PPM REMAINDER']
        else:
          return None, None  

# COMMAND ----------

# MAGIC %md
# MAGIC ### Children Aged 2-17 Flowchart for Demo Buckets

# COMMAND ----------

#######################################################################################################################
#####        children Aged 2-17 Flowchart for Demo Buckets could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def CHILDREN_MINORITY_FLOWCHART():
    """
    This function define the rules of the Children Demographics flowchart for Minorities.
    The Children Demographics flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """  
  
    level1=                      ['MINORITY_C2_5','MINORITY_C6_11','MINORITY_M12_17','MINORITY_F12_17',
                                  'COMPLEMENT_C2_5','COMPLEMENT_C6_11','COMPLEMENT_M12_17','COMPLEMENT_F12_17']
    level2=                      ['MINORITY_C2_11','MINORITY_M12_17','MINORITY_F12_17',
                                 'COMPLEMENT_C2_11','COMPLEMENT_M12_17','COMPLEMENT_F12_17']
    level3=                      ['MINORITY_C2_5','MINORITY_C6_11','MINORITY_P12_17',
                                 'COMPLEMENT_C2_5','COMPLEMENT_C6_11','COMPLEMENT_P12_17']
    level4=                      ['MINORITY_C2_11','MINORITY_P12_17',
                                 'COMPLEMENT_C2_11','COMPLEMENT_P12_17']
    level5=                      ['MINORITY_P2_17',
                                 'COMPLEMENT_P2_17']
    level6=                      None
    option1={1:level1,2:level2,3:level3,4:level4,5:level5}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Male aged 18+ Flowchart for Demo Buckets

# COMMAND ----------

#######################################################################################################################
#####        male Aged 18-999 Flowchart for Demo Buckets can not be prioritized like other controls
#####        the flowchart is coded here with a tree
#####        which level is qualified is decided by the node class
#######################################################################################################################

def MALE_FLOWCHART():
    """
    This function define the rules of the Male Demographics flowchart for Minorities.
    The Male Demographics flowchart rules are applied to a subset of the data 
    creating a decision tree using the node class. 
    It returns the most granular level that meets the threshold criteria.
    """  
  
    root=node()
    root.data=['MINORITY_M18_24','MINORITY_M25_34',   'COMPLEMENT_M18_24','COMPLEMENT_M25_34']
    root.f=node()
    root.f.data=['MINORITY_M18_34', 'COMPLEMENT_M18_34']
    root.f.f=node()
    root.f.f.data=['MINORITY_M18_49', 'COMPLEMENT_M18_49']
    root.f.f.f=node()
    root.f.f.f.data=['MINORITY_M18_999', 'COMPLEMENT_M18_999']
    root.f.f.f.f=None
    root.f.f.f.t=['MINORITY_M18_999', 'COMPLEMENT_M18_999']
    root.f.f.t=node()
    root.f.f.t.data=['MINORITY_M50_54','MINORITY_M55_64', 'COMPLEMENT_M50_54','COMPLEMENT_M55_64']
    root.f.f.t.f=node()
    root.f.f.t.f.data=['MINORITY_M50_64','COMPLEMENT_M50_64']
    root.f.f.t.f.f=node()
    root.f.f.t.f.f.data=['MINORITY_M50_999', 'COMPLEMENT_M50_999']
    root.f.f.t.f.f.t=['MINORITY_M18_49','MINORITY_M50_999', 'COMPLEMENT_M18_49','COMPLEMENT_M50_999']
    root.f.f.t.f.f.f=None
    root.f.f.t.f.t=node()
    root.f.f.t.f.t.data=['MINORITY_M65_999',    'COMPLEMENT_M65_999']
    root.f.f.t.f.t.t=['MINORITY_M18_49','MINORITY_M50_64','MINORITY_M65_999',    'COMPLEMENT_M18_49','COMPLEMENT_M50_64','COMPLEMENT_M65_999']
    root.f.f.t.f.t.f=None
    root.f.f.t.t=node()
    root.f.f.t.t.data=['MINORITY_M65_999',    'COMPLEMENT_M65_999']
    root.f.f.t.t.f=None
    root.f.f.t.t.t=['MINORITY_M18_49','MINORITY_M50_54','MINORITY_M55_64',
                    'MINORITY_M65_999','COMPLEMENT_M18_49','COMPLEMENT_M50_54','COMPLEMENT_M55_64','COMPLEMENT_M65_999'] 
    root.t=node()
    root.t.data=['MINORITY_M35_49',    'COMPLEMENT_M35_49']
    root.t.t=node()
    root.t.f=None
    root.t.t.data=['MINORITY_M50_54','MINORITY_M55_64',    'COMPLEMENT_M50_54','COMPLEMENT_M55_64']
    root.t.t.t=node()
    root.t.t.f=node()
    #THIS IS NOT LEGIBLE IN THE DOCUMENTATION ASSUMING ITS 50-64
    root.t.t.f.data=['MINORITY_M50_64',    'COMPLEMENT_M50_64']
    root.t.t.f.f=node()
    root.t.t.f.f.data=['MINORITY_M50_999',    'COMPLEMENT_M50_999']
    root.t.t.f.f.t=['MINORITY_M18_24','MINORITY_M25_34','MINORITY_M35_49',
                    'MINORITY_M50_999','COMPLEMENT_M18_24','COMPLEMENT_M25_34','COMPLEMENT_M35_49','COMPLEMENT_M50_999']
    root.t.t.f.f.f=None
    root.t.t.f.t=node()
    root.t.t.f.t.data=['MINORITY_M65_999','COMPLEMENT_M65_999']
    #NOT LEGIBLE DOUBLE CHECK
    root.t.t.f.t.t=['MINORITY_M18_24','MINORITY_M25_34','MINORITY_M35_49',
                    'MINORITY_M50_64','MINORITY_M65_999','COMPLEMENT_M18_24','COMPLEMENT_M25_34',
                    'COMPLEMENT_M35_49','COMPLEMENT_M50_64','COMPLEMENT_M65_999']
    root.t.t.f.t.f=None
    root.t.t.t.data=['MINORITY_M65_999',   'COMPLEMENT_M65_999']
    root.t.t.t.t=['MINORITY_M18_24','MINORITY_M25_34','MINORITY_M35_49','MINORITY_M50_54',
                  'MINORITY_M55_64','MINORITY_M65_999','COMPLEMENT_M18_24','COMPLEMENT_M25_34','COMPLEMENT_M35_49',
                  'COMPLEMENT_M50_54','COMPLEMENT_M55_64','COMPLEMENT_M65_999']
    root.t.t.t.f=None
    root.f.t=node()
    root.f.t.data=['MINORITY_M35_49',    'COMPLEMENT_M35_49']
    root.f.t.t=node()
    root.f.t.t.data=['MINORITY_M50_54','MINORITY_M55_64',    'COMPLEMENT_M50_54','COMPLEMENT_M55_64']
    root.f.t.t.t=node()
    root.f.t.t.t.data=['MINORITY_M65_999',    'COMPLEMENT_M65_999']
    root.f.t.t.t.t=['MINORITY_M18_34','MINORITY_M35_49','MINORITY_M50_54','MINORITY_M55_64',
                    'MINORITY_M65_999','COMPLEMENT_M18_34','COMPLEMENT_M35_49','COMPLEMENT_M50_54','COMPLEMENT_M55_64','COMPLEMENT_M65_999']
    root.f.t.t.t.f=None
    root.f.t.t.f=node()
    root.f.t.t.f.data=['MINORITY_M50_64',   'COMPLEMENT_M50_64']
    root.f.t.t.f.t=node()
    root.f.t.t.f.t.data=['MINORITY_M65_999',    'COMPLEMENT_M65_999']
    root.f.t.t.f.t.t=['MINORITY_M18_34','MINORITY_M35_49','MINORITY_M50_64',
                      'MINORITY_M65_999','COMPLEMENT_M18_34','COMPLEMENT_M35_49','COMPLEMENT_M50_64','COMPLEMENT_M65_999']
    root.f.t.t.f.f=node()
    root.f.t.t.f.f.data=['MINORITY_M50_999',    'COMPLEMENT_M50_999']
    root.f.t.t.f.f.t=['MINORITY_M18_34','MINORITY_M35_49','MINORITY_M50_999', 'COMPLEMENT_M18_34','COMPLEMENT_M35_49','COMPLEMENT_M50_999']
    root.f.t.t.f.f.f=None
    root.f.t.t.f.t.f=node()
    root.f.t.t.f.t.f.data=['MINORITY_M50_999',   'COMPLEMENT_M50_999']
    root.f.t.t.f.t.f.t=['MINORITY_M18_34','MINORITY_M35_49','MINORITY_M50_999', 'COMPLEMENT_M18_34','COMPLEMENT_M35_49','COMPLEMENT_M50_999']
    root.f.t.t.f.t.f.f=None
    root.f.t.f=None
        
    return root



# COMMAND ----------

# MAGIC %md
# MAGIC ### Female aged 18+ Flowchart for Demo Buckets

# COMMAND ----------

#######################################################################################################################
#####        female Aged 18-999 Flowchart for Demo Buckets can not be prioritized like other controls
#####        the flowchart is coded here with a tree
#####        which level is qualified is decided by the node class
#######################################################################################################################

def FEMALE_FLOWCHART():
    """
    This function define the rules of the Male Demographics flowchart for Minorities.
    The FeMale Demographics flowchart rules are applied to a subset of the data 
    creating a decision tree using the node class. 
    It returns the most granular level that meets the threshold criteria.
    """  
  
    root=node()
    root.data=['MINORITY_F18_24','MINORITY_F25_34',   'COMPLEMENT_F18_24','COMPLEMENT_F25_34']
    root.f=node()
    root.f.data=['MINORITY_F18_34', 'COMPLEMENT_F18_34']
    root.f.f=node()
    root.f.f.data=['MINORITY_F18_49', 'COMPLEMENT_F18_49']
    root.f.f.f=node()
    root.f.f.f.data=['MINORITY_F18_999', 'COMPLEMENT_F18_999']
    root.f.f.f.f=None
    root.f.f.f.t=['MINORITY_F18_999', 'COMPLEMENT_F18_999']
    root.f.f.t=node()
    root.f.f.t.data=['MINORITY_F50_54','MINORITY_F55_64', 'COMPLEMENT_F50_54','COMPLEMENT_F55_64']
    root.f.f.t.f=node()
    root.f.f.t.f.data=['MINORITY_F50_64','COMPLEMENT_F50_64']
    root.f.f.t.f.f=node()
    root.f.f.t.f.f.data=['MINORITY_F50_999', 'COMPLEMENT_F50_999']
    root.f.f.t.f.f.t=['MINORITY_F18_49','MINORITY_F50_999', 'COMPLEMENT_F18_49','COMPLEMENT_F50_999']
    root.f.f.t.f.f.f=None
    root.f.f.t.f.t=node()
    root.f.f.t.f.t.data=['MINORITY_F65_999',    'COMPLEMENT_F65_999']
    root.f.f.t.f.t.t=['MINORITY_F18_49','MINORITY_F50_64','MINORITY_F65_999',    'COMPLEMENT_F18_49','COMPLEMENT_F50_64','COMPLEMENT_F65_999']
    root.f.f.t.f.t.f=None
    root.f.f.t.t=node()
    root.f.f.t.t.data=['MINORITY_F65_999',    'COMPLEMENT_F65_999']
    root.f.f.t.t.f=None
    root.f.f.t.t.t=['MINORITY_F18_49','MINORITY_F50_54','MINORITY_F55_64',
                    'MINORITY_F65_999','COMPLEMENT_F18_49','COMPLEMENT_F50_54','COMPLEMENT_F55_64','COMPLEMENT_F65_999'] 
    root.t=node()
    root.t.data=['MINORITY_F35_49',    'COMPLEMENT_F35_49']
    root.t.t=node()
    root.t.f=None
    root.t.t.data=['MINORITY_F50_54','MINORITY_F55_64',    'COMPLEMENT_F50_54','COMPLEMENT_F55_64']
    root.t.t.t=node()
    root.t.t.f=node()
    #THIS IS NOT LEGIBLE IN THE DOCUMENTATION ASSUMING ITS 50-64
    root.t.t.f.data=['MINORITY_F50_64',    'COMPLEMENT_F50_64']
    root.t.t.f.f=node()
    root.t.t.f.f.data=['MINORITY_F50_999',    'COMPLEMENT_F50_999']
    root.t.t.f.f.t=['MINORITY_F18_24','MINORITY_F25_34','MINORITY_F35_49',
                    'MINORITY_F50_999','COMPLEMENT_F18_24','COMPLEMENT_F25_34','COMPLEMENT_F35_49','COMPLEMENT_F50_999']
    root.t.t.f.f.f=None
    root.t.t.f.t=node()
    root.t.t.f.t.data=['MINORITY_F65_999','COMPLEMENT_F65_999']
    #NOT LEGIBLE DOUBLE CHECK
    root.t.t.f.t.t=['MINORITY_F18_24','MINORITY_F25_34','MINORITY_F35_49',
                    'MINORITY_F50_64','MINORITY_F65_999','COMPLEMENT_F18_24','COMPLEMENT_F25_34',
                    'COMPLEMENT_F35_49','COMPLEMENT_F50_64','COMPLEMENT_F65_999']
    root.t.t.f.t.f=None
    root.t.t.t.data=['MINORITY_F65_999',   'COMPLEMENT_F65_999']
    root.t.t.t.t=['MINORITY_F18_24','MINORITY_F25_34','MINORITY_F35_49','MINORITY_F50_54',
                  'MINORITY_F55_64','MINORITY_F65_999','COMPLEMENT_F18_24','COMPLEMENT_F25_34','COMPLEMENT_F35_49',
                  'COMPLEMENT_F50_54','COMPLEMENT_F55_64','COMPLEMENT_F65_999']
    root.t.t.t.f=None
    root.f.t=node()
    root.f.t.data=['MINORITY_F35_49',    'COMPLEMENT_F35_49']
    root.f.t.t=node()
    root.f.t.t.data=['MINORITY_F50_54','MINORITY_F55_64',    'COMPLEMENT_F50_54','COMPLEMENT_F55_64']
    root.f.t.t.t=node()
    root.f.t.t.t.data=['MINORITY_F65_999',    'COMPLEMENT_F65_999']
    root.f.t.t.t.t=['MINORITY_F18_34','MINORITY_F35_49','MINORITY_F50_54','MINORITY_F55_64',
                    'MINORITY_F65_999','COMPLEMENT_F18_34','COMPLEMENT_F35_49','COMPLEMENT_F50_54','COMPLEMENT_F55_64','COMPLEMENT_F65_999']
    root.f.t.t.t.f=None
    root.f.t.t.f=node()
    root.f.t.t.f.data=['MINORITY_F50_64',   'COMPLEMENT_F50_64']
    root.f.t.t.f.t=node()
    root.f.t.t.f.t.data=['MINORITY_F65_999',    'COMPLEMENT_F65_999']
    root.f.t.t.f.t.t=['MINORITY_F18_34','MINORITY_F35_49','MINORITY_F50_64',
                      'MINORITY_F65_999','COMPLEMENT_F18_34','COMPLEMENT_F35_49','COMPLEMENT_F50_64','COMPLEMENT_F65_999']
    root.f.t.t.f.f=node()
    root.f.t.t.f.f.data=['MINORITY_F50_999',    'COMPLEMENT_F50_999']
    root.f.t.t.f.f.t=['MINORITY_F18_34','MINORITY_F35_49','MINORITY_F50_999', 'COMPLEMENT_F18_34','COMPLEMENT_F35_49','COMPLEMENT_F50_999']
    root.f.t.t.f.f.f=None
    root.f.t.t.f.t.f=node()
    root.f.t.t.f.t.f.data=['MINORITY_F50_999',   'COMPLEMENT_F50_999']
    root.f.t.t.f.t.f.t=['MINORITY_F18_34','MINORITY_F35_49','MINORITY_F50_999', 'COMPLEMENT_F18_34','COMPLEMENT_F35_49','COMPLEMENT_F50_999']
    root.f.t.t.f.t.f.f=None
    root.f.t.f=None
        
    return root




# COMMAND ----------

# MAGIC %md 
# MAGIC ### Black Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####       Black Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def BLACK_FLOWCHART():
    """
    This function define the rules of the Black Demographics flowchart.
    The Black Demographics flowchart rules are applied to a subset of the data using the 
    collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """  
    
    level1=                      ['BLACK_RACE','NON-BLACK_RACE']
    level2=                      None
    option1={1:level1}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Origin Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####       Origin Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def ORIGIN_FLOWCHART():
    """
    This function define the rules of the Origin flowchart.
    The Origin flowchart rules are applied to a subset of the 
    data using the collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """      
    
    level1=                      ['HISPANIC','NON-HISPANIC']
    level2=                      None
    option1={1:level1}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Asian Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####       Asian Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def ASIAN_FLOWCHART():
    """
    This function define the rules of the Asian flowchart.
    The Asian flowchart rules are applied to a subset of the 
    data using the collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """       
    
    level1=                      ['ASIAN_RACE','NON-ASIAN_RACE']
    level2=                      None
    option1={1:level1}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Native American Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####       Native American Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def AIAN_FLOWCHART():
    """
    This function define the rules of the Native American flowchart.
    The Native American flowchart rules are applied to a subset of the 
    data using the collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
  
  
    level1=                      ['AIAN_RACE','NON-AIAN_RACE']
    level2=                      None
    option1={1:level1}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### DVR Flowchart for DMA

# COMMAND ----------

#######################################################################################################################
#####       DVR Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def DVR_FLOWCHART():
    """
    This function define the rules of the DVR flowchart.
    The DVR flowchart rules are applied to a subset of the 
    data using the collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
    
    level1=                      ['DVR_YES','DVR_NO']
    level2=                      None
    option1={1:level1}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Operable Sets Flowchart for  DMA (Only Code Readers)

# COMMAND ----------

#######################################################################################################################
#####       Operable Sets Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def OPERABLE_SETS_FLOWCHART():
    """
    This function define the rules of the Operable Sets flowchart.
    The Operable Sets flowchart rules are applied to a subset of the 
    data using the collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
    
    level1=                      ['1_SET','2+_SETS']
    level2=                      None
    option1={1:level1}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC ### Measurable Sets Flowchart for  DMA (Only LPM/Set Meter)

# COMMAND ----------

#######################################################################################################################
#####       Measurable Sets Flowchart for DMA could be prioritized according 
#####               to the levels in the following function
#####        which level is qualified is decided by collapsed_level_determiner function
#######################################################################################################################

def MEASURABLE_SETS_FLOWCHART():
    """
    This function define the rules of the Measurable Sets flowchart.
    The Measurable Sets flowchart rules are applied to a subset of the 
    data using the collapsed_level_determiner function. 
    It returns the most granular level that meets the threshold criteria.
    """
    
    level1=                      ['1_SET','2_SETS','3_SETS','4+_SETS']
    level2=                      ['1_SET','2+_SETS']
    level3=                      None
    option1={1:level1,2:level2}
    
    if collapsed_level_determiner(option1) is not None:
      no,collapsed_level=collapsed_level_determiner(option1)
    else:
      collapsed_level=None
      no=None
    return no,collapsed_level

# COMMAND ----------

# MAGIC %md
# MAGIC #Part3: Apply the Flowcharts to DMAs and Minorities/Complements within a DMA

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Create DMA and Minority Controls Dictionary

# COMMAND ----------

#######################################################################################################################
#####       dma_controls dictionary includes all the controls that we should examine for total DMA
#####       minority_controls dictionary includes all the controls that we should examine for minorities/complements within a DMA
#######################################################################################################################

dma_controls={
         'AGE_OF_HEAD': 'AGE_OF_HEAD',
         'HOUSEHOLD_SIZE':'HOUSEHOLD_SIZE',
         'LANGUAGE':'LANGUAGE',
         'CABLE_PLUS':'CABLE_PLUS',
         'ADS':'ADS',
         'PRESENCE_OF_NONADULTS':'PRESENCE_OF_NONADULTS',
         'PPM_METRO_CODE':'PPM_METRO_CODE',
         
         'BLACK':'BLACK',
         'ORIGIN':'ORIGIN',
         'ASIAN':'ASIAN',
         'AIAN':'AIAN',
         'DVR':'DVR',
         'OPERABLE_SETS':'OPERABLE_SETS',
         'MEASURABLE_SETS':'MEASURABLE_SETS'
         }


minority_controls={'AGE_OF_HEAD_BLACK_RACE': ['AGE_OF_HEAD_BLACK_RACE','_BLACK_RACE'],
                   'AGE_OF_HEAD_NON-BLACK_RACE':['AGE_OF_HEAD_NON-BLACK_RACE','_NON-BLACK_RACE'],
                   'AGE_OF_HEAD_HISPANIC': ['AGE_OF_HEAD_HISPANIC','_HISPANIC'],
                   'AGE_OF_HEAD_NON-HISPANIC': ['AGE_OF_HEAD_NON-HISPANIC','_NON-HISPANIC']  ,
                   
                   'HOUSEHOLD_SIZE_BLACK_RACE': ['HOUSEHOLD_SIZE_BLACK_RACE','_BLACK_RACE'],
                   'HOUSEHOLD_SIZE_NON-BLACK_RACE':['HOUSEHOLD_SIZE_NON-BLACK_RACE','_NON-BLACK_RACE'],
                   'HOUSEHOLD_SIZE_HISPANIC': ['HOUSEHOLD_SIZE_HISPANIC','_HISPANIC'],
                   'HOUSEHOLD_SIZE_NON-HISPANIC': ['HOUSEHOLD_SIZE_NON-HISPANIC','_NON-HISPANIC'],
                   
                   'ENHANCED_HOUSEHOLD_SIZE_BLACK_RACE': ['HOUSEHOLD_SIZE_BLACK_RACE','_BLACK_RACE'],
                   'ENHANCED_HOUSEHOLD_SIZE_NON-BLACK_RACE':['HOUSEHOLD_SIZE_NON-BLACK_RACE','_NON-BLACK_RACE'],
                   'ENHANCED_HOUSEHOLD_SIZE_HISPANIC': ['HOUSEHOLD_SIZE_HISPANIC','_HISPANIC'],
                   'ENHANCED_HOUSEHOLD_SIZE_NON-HISPANIC': ['HOUSEHOLD_SIZE_NON-HISPANIC','_NON-HISPANIC'],
               
                   'CABLE_PLUS_BLACK_RACE': ['CABLE_PLUS_BLACK_RACE','_BLACK_RACE'],
                   'CABLE_PLUS_NON-BLACK_RACE':['CABLE_PLUS_NON-BLACK_RACE','_NON-BLACK_RACE'],
                   'CABLE_PLUS_HISPANIC': ['CABLE_PLUS_HISPANIC','_HISPANIC'],
                   'CABLE_PLUS_NON-HISPANIC': ['CABLE_PLUS_NON-HISPANIC','_NON-HISPANIC'],
                   
                   'ADS_BLACK_RACE': ['ADS_BLACK_RACE','_BLACK_RACE'],
                   'ADS_NON-BLACK_RACE':['ADS_NON-BLACK_RACE','_NON-BLACK_RACE'],
                   'ADS_HISPANIC': ['ADS_HISPANIC','_HISPANIC'],
                   'ADS_NON-HISPANIC': ['ADS_NON-HISPANIC','_NON-HISPANIC']
                  }

minority_or_complement_dict={'AGE_OF_HEAD': 'AGE_OF_HEAD','ENHANCED_HOUSEHOLD_SIZE':'ENHANCED_HOUSEHOLD_SIZE','HOUSEHOLD_SIZE':'HOUSEHOLD_SIZE','CABLE_PLUS':'CABLE_PLUS','ADS':'ADS'}

# COMMAND ----------

# MAGIC %md
# MAGIC ### Read the Output of Effective_Sample_Size_Computation Module

# COMMAND ----------

#######################################################################################################################
#####       Read the Output of Effective_Sample_Size_Computation Module
#####       Convert the ouput to a Pandas dataframe
#######################################################################################################################

input_df0=sqlContext.read.parquet(input_controls_with_ess_dir)
input_df0.createOrReplaceTempView('input_df0')
# display(input_df0)

import pandas as pd
import numpy as np

home_threshold=29.1

input_df=input_df0.toPandas()
input_df['dma_code']=input_df['dma_code'].astype(int)
input_df['effective_sample_size']=input_df['effective_sample_size'].astype(float)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Apply Required Flowcharts to DMA Controls

# COMMAND ----------

#######################################################################################################################
#####       Apply Required Flowcharts to DMA controls. 
#####       The chosen level for each control is added to a dictionary named collapsed_controls. 
#####       keys of this dictionary are dma_codes
#######################################################################################################################

collapsed_controls=dict()

# Loop over DMAs and Control dictionaries using Sufficient Sample Size Qualifier Engine rules to fill the collapsed_control dictionary 
for dma in input_df.dma_code.unique():
  
  collapsed_controls[dma]=dict()
  
  for control in dma_controls.keys():
    reduced_df=input_df[(input_df.dma_code==dma) & (input_df.control==dma_controls[control])][['parameter','effective_sample_size']]

    flowcharts={'AGE_OF_HEAD':AGE_OF_HEAD_FLOWCHART(),
                'HOUSEHOLD_SIZE':HOUSEHOLD_SIZE_FLOWCHART(),
                'LANGUAGE':LANGUAGE_FLOWCHART(),
                'CABLE_PLUS': CABLE_PLUS_FLOWCHART(),
                'ADS':ADS_FLOWCHART(),
                'PRESENCE_OF_NONADULTS':PRESENCE_OF_NONADULTS_FLOWCHART(),
                'PPM_METRO_CODE':PPM_METRO_CODE_FLOWCHART(),


                'BLACK':BLACK_FLOWCHART(),
                'ORIGIN':ORIGIN_FLOWCHART(),
                'ASIAN':ASIAN_FLOWCHART(),
                'AIAN':AIAN_FLOWCHART(),
                'DVR':DVR_FLOWCHART(),
                'OPERABLE_SETS':OPERABLE_SETS_FLOWCHART(),
                'MEASURABLE_SETS':MEASURABLE_SETS_FLOWCHART()
               }
  
    level_no,dma_control_collapsed=flowcharts[control]
    collapsed_controls[dma][control]=dma_control_collapsed
    
collapsed_controls

# COMMAND ----------

# MAGIC %md
# MAGIC ### Apply Required Flowcharts to Minorities/Complements Controls

# COMMAND ----------

#######################################################################################################################
#####       Apply Required Flowcharts to Minorities/Complements controls. 
#####       The chosen level for each control is added to the collapsed_controls dictionary
#######################################################################################################################

for dma in input_df.dma_code.unique():
  
  for control in minority_controls.keys():
    
    reduced_df=input_df[(input_df.dma_code==dma) & (input_df.control==minority_controls[control][0])][['parameter','effective_sample_size']]
    primary_control=control.rsplit(minority_controls[control][1])[0]
    if primary_control in dma_controls.keys():
       primary_control_collapsed=collapsed_controls[dma][primary_control]
    else:
       primary_control_collapsed=None
          
    flowcharts={'AGE_OF_HEAD':AGE_OF_HEAD_MINORITY_FLOWCHART(),
                'HOUSEHOLD_SIZE':HOUSEHOLD_SIZE_MINORITY_FLOWCHART(),
                'ENHANCED_HOUSEHOLD_SIZE':ENHANCED_HOUSEHOLD_SIZE_MINORITY_FLOWCHART(),
                'CABLE_PLUS':CABLE_PLUS_MINORITY_FLOWCHART(),
                'ADS':ADS_FLOWCHART()
               }
    
    level_no,minority_control_collapsed=flowcharts[primary_control]
    
    collapsed_controls[dma][control]=minority_control_collapsed
    
    collapsed_controls[dma][control+'_NUMBER']=level_no
    
collapsed_controls

# COMMAND ----------

# MAGIC %md
# MAGIC ### Pick the Least Granular Level between Minority and Complement

# COMMAND ----------

#######################################################################################################################
#####       Since a different level might have been picked for a minority and its complement, there is a piece of 
#####            code that picks the least granular level between the minority and complement for every control 
#####            and then assigns both the minority and complement the chosen least granular level
#######################################################################################################################

for key in collapsed_controls.keys():
  
  M_or_C=minority_or_complement_dict
  
  for control in minority_or_complement_dict.keys():
    
    black_dic={collapsed_controls[key][M_or_C[control]+'_BLACK_RACE_NUMBER']:collapsed_controls[key][M_or_C[control]+'_BLACK_RACE'],
               collapsed_controls[key][M_or_C[control]+'_NON-BLACK_RACE_NUMBER']:collapsed_controls[key][M_or_C[control]+'_NON-BLACK_RACE']}
    
    if None not in black_dic.keys():
      winner_black_level_no=max(black_dic.keys())
      winner_black_level=black_dic[winner_black_level_no]
    else:
      winner_black_level_no=None
      winner_black_level=None
      
    collapsed_controls[key][M_or_C[control]+'_BLACK_RACE_NUMBER']=winner_black_level_no
    collapsed_controls[key][M_or_C[control]+'_BLACK_RACE']=winner_black_level
    collapsed_controls[key][M_or_C[control]+'_NON-BLACK_RACE_NUMBER']=winner_black_level_no
    collapsed_controls[key][M_or_C[control]+'_NON-BLACK_RACE']=winner_black_level
    
    hispanic_dic={collapsed_controls[key][M_or_C[control]+'_HISPANIC_NUMBER']:collapsed_controls[key][M_or_C[control]+'_HISPANIC'],
                  collapsed_controls[key][M_or_C[control]+'_NON-HISPANIC_NUMBER']:collapsed_controls[key][M_or_C[control]+'_NON-HISPANIC']}
    
    if None not in hispanic_dic.keys():
      winner_hispanic_level_no=max(hispanic_dic.keys())
      winner_hispanic_level=hispanic_dic[winner_hispanic_level_no]
    else:
      winner_hispanic_level_no=None
      winner_hispanic_level=None
      
    collapsed_controls[key][M_or_C[control]+'_HISPANIC_NUMBER']=winner_hispanic_level_no
    collapsed_controls[key][M_or_C[control]+'_HISPANIC']=winner_hispanic_level
    collapsed_controls[key][M_or_C[control]+'_NON-HISPANIC_NUMBER']=winner_hispanic_level_no
    collapsed_controls[key][M_or_C[control]+'_NON-HISPANIC']=winner_hispanic_level
    
collapsed_controls

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 4: Apply the Flowcharts to Demo Buckets within a DMA

# COMMAND ----------

# MAGIC %md
# MAGIC ### Change Labels

# COMMAND ----------

#######################################################################################################################
############ Replace data labels in order to process both the minority and complement at the same time
#######################################################################################################################

input_df['parameter']=np.where(input_df['control']=='PERSONS_BLACK_RACE','MINORITY_'+input_df['parameter'].astype(str),input_df['parameter'])
input_df['parameter']=np.where(input_df['control']=='PERSONS_NON-BLACK_RACE','COMPLEMENT_'+input_df['parameter'].astype(str),input_df['parameter'])
input_df['parameter']=np.where(input_df['control']=='PERSONS_HISPANIC','MINORITY_'+input_df['parameter'].astype(str),input_df['parameter'])
input_df['parameter']=np.where(input_df['control']=='PERSONS_NON-HISPANIC','COMPLEMENT_'+input_df['parameter'].astype(str),input_df['parameter'])
input_df['control']=np.where(input_df['control']=='PERSONS_BLACK_RACE','PERSONS WITHIN BLACK/NON-BLACK',input_df['control'])
input_df['control']=np.where(input_df['control']=='PERSONS_NON-BLACK_RACE','PERSONS WITHIN BLACK/NON-BLACK',input_df['control'])
input_df['control']=np.where(input_df['control']=='PERSONS_HISPANIC','PERSONS WITHIN HISPANIC/NON-HISPANIC',input_df['control'])
input_df['control']=np.where(input_df['control']=='PERSONS_NON-HISPANIC','PERSONS WITHIN HISPANIC/NON-HISPANIC',input_df['control'])

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create Demographics Controls Dictionary

# COMMAND ----------

#######################################################################################################################
############ Create a dictionary that is needed in order to process both minorities and complements at the same time
############ Create a dictionary which dictates which flowchart to run for each control
#######################################################################################################################

sex_minority_controls={
          'PERSONS WITHIN BLACK/NON-BLACK_MALE': ['PERSONS WITHIN BLACK/NON-BLACK_MALE','_MALE'],
          'PERSONS WITHIN BLACK/NON-BLACK_FEMALE': ['PERSONS WITHIN BLACK/NON-BLACK_FEMALE','_FEMALE'],
          'PERSONS WITHIN HISPANIC/NON-HISPANIC_MALE': ['PERSONS WITHIN HISPANIC/NON-HISPANIC_MALE','_MALE'],
          'PERSONS WITHIN HISPANIC/NON-HISPANIC_FEMALE': ['PERSONS WITHIN HISPANIC/NON-HISPANIC_FEMALE','_FEMALE'],
          'PERSONS WITHIN BLACK/NON-BLACK_CHILDREN': ['PERSONS WITHIN BLACK/NON-BLACK_CHILDREN','_CHILDREN'],
          'PERSONS WITHIN HISPANIC/NON-HISPANIC_CHILDREN': ['PERSONS WITHIN HISPANIC/NON-HISPANIC_CHILDREN','_CHILDREN']
                       }

flowcharts={
           'PERSONS WITHIN BLACK/NON-BLACK_MALE': MALE_FLOWCHART(),
           'PERSONS WITHIN BLACK/NON-BLACK_FEMALE': FEMALE_FLOWCHART(),
           'PERSONS WITHIN HISPANIC/NON-HISPANIC_MALE': MALE_FLOWCHART(),
           'PERSONS WITHIN HISPANIC/NON-HISPANIC_FEMALE': FEMALE_FLOWCHART(),
           'PERSONS WITHIN HISPANIC/NON-HISPANIC_CHILDREN': CHILDREN_MINORITY_FLOWCHART(),
           'PERSONS WITHIN BLACK/NON-BLACK_CHILDREN': CHILDREN_MINORITY_FLOWCHART()
                }

# COMMAND ----------

# MAGIC %md
# MAGIC ### Apply Required Flowcharts to Demo Buckets within a DMA

# COMMAND ----------

#######################################################################################################################
############ Flowchart for Children aged 2 to 17, Males aged 18+ and Females aged 18+ are applied to demo buckets within a DMA.
#######################################################################################################################

for dma in input_df.dma_code.unique():
  
  for control in sex_minority_controls.keys():
    
    primary_control=control.rsplit(sex_minority_controls[control][1])[0]
    reduced_df=input_df[(input_df.dma_code==dma) & (input_df.control==primary_control)][['parameter','effective_sample_size']]
    data=reduced_df
    
    if sex_minority_controls[control][1]!='_CHILDREN':
      root=flowcharts[control]
      otp=root
      while type(otp) is type(root):
      
        data_subset=(data[data['parameter'].isin(otp.data)]['effective_sample_size']>home_threshold).values
        if False in data_subset:
          otp=otp.f 
        else:
          otp=otp.t
      collapsed_controls[dma][control]=otp
    
    else:
      level_no,minority_control_collapsed=CHILDREN_MINORITY_FLOWCHART()
      collapsed_controls[dma][control]=minority_control_collapsed
      collapsed_controls[dma][control+'_NUMBER']=level_no
    
collapsed_controls

# COMMAND ----------

# MAGIC %md 
# MAGIC # Part 5: Implementing the Required Adjustments

# COMMAND ----------

# MAGIC %md
# MAGIC ### Define Dictionaries that are Used in the Adjustment

# COMMAND ----------

#######################################################################################################################
############   Define the dictionaries and the lists that are required in the next cell. The next cell
############         implements some required adjustments
#######################################################################################################################

cable_plust_list_to_check={'CABLE_PLUS_BLACK_RACE':'ADS_BLACK_RACE',
                 'CABLE_PLUS_NON-BLACK_RACE':'ADS_NON-BLACK_RACE',
                 'CABLE_PLUS_HISPANIC':'ADS_HISPANIC',
                 'CABLE_PLUS_NON-HISPANIC':'ADS_NON-HISPANIC'}

demo_controls={'CHILDREN':['C2_5','C6_11','M12_17','F12_17'],
               'PERSONS_MALE':['M18_24','M25_34','M35_49','M50_54','M55_64','M65_999'],
               'PERSONS_FEMALE' :['F18_24','F25_34','F35_49','F50_54','F55_64','F65_999']}


enhanced_household_size_list={501:['BLACK','HISPANIC'],
                              602:['BLACK'],
                              504:['BLACK'],
                              623:['BLACK'],
                              511:['BLACK'],
                              524:['BLACK'],
                              618:['BLACK'],
                              505:['BLACK'],
                              609:['BLACK'],
                              517:['BLACK'],
                              512:['BLACK'],
                              544:['BLACK'],
                              560:['BLACK'],
                              640:['BLACK'],
                              556:['BLACK'],
                              622:['BLACK'],
                              630:['BLACK']
                                           }

minority_and_complement_dict={'BLACK':
                              ['HOUSEHOLD_SIZE_BLACK_RACE','HOUSEHOLD_SIZE_NON-BLACK_RACE','HOUSEHOLD_SIZE_BLACK_RACE_NUMBER','HOUSEHOLD_SIZE_NON-BLACK_RACE_NUMBER'],
                             'HISPANIC':
                              ['HOUSEHOLD_SIZE_HISPANIC','HOUSEHOLD_SIZE_NON-HISPANIC','HOUSEHOLD_SIZE_HISPANIC_NUMBER','HOUSEHOLD_SIZE_NON-HISPANIC_NUMBER']}

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Implement the Required Adjustments

# COMMAND ----------

#######################################################################################################################
############   For the code reader markets, only operable sets stays as a control while the measurable set is dropped. 
############          For the set meter and LPM markets, the vice versa is true.
############   ADS is dropped as a control when cable plus is CABLE_PLUS_YES/BROADCAST_ONLY for minorities/complements within a DMA.
############   Demo buckets are present at the most granular level for all DMAs automatically without any investigation.
############   For the minorities and complements mentioned in the list below, the selected level based on the 
############         Enhanced Household Size Flowchart for Minorities/Complements remains and the level based on 
############         Household Size Flowchart for Minorities/Complements is dropped. For every other minorities 
############         and complements, the vice versa is true.
#######################################################################################################################

for dma in input_df.dma_code.unique():
  
  # Operable set adjustment is only done for the code reader markets. In the following code, operable set control is dropped for the lpm markets
  if (dma in lpm_markets_list or dma in set_meter_markets_list):
    collapsed_controls[dma]['OPERABLE_SETS']=None
  elif (dma in code_reader_markets_list):
    collapsed_controls[dma]['MEASURABLE_SETS']=None
  

  # ADS is not a control when cable plus is ['CABLE_PLUS_YES', 'BROADCAST_ONLY'] for both hispanic and black. In the following code, we implement this rule. 
  for key in cable_plust_list_to_check:
    if collapsed_controls[dma][key]==['CABLE_PLUS_YES', 'BROADCAST_ONLY']:
      collapsed_controls[dma][cable_plust_list_to_check[key]]=None
      collapsed_controls[dma][cable_plust_list_to_check[key]+'_NUMBER']=None
      
  # Demos are available at the most granular level for all dmas without any flowcharts    
  for key,value in demo_controls.items():
    collapsed_controls[dma][key]=value
  
  # Enhanced household size flowchart is applied to only a few minority and markets, the following code removes for all non-related mnority/dma
  if dma in enhanced_household_size_list.keys():
    
    if 'BLACK' in enhanced_household_size_list[dma]:
      for value in minority_and_complement_dict['BLACK']:
        collapsed_controls[dma][value]=None
    else:
      for value in minority_and_complement_dict['BLACK']:
        collapsed_controls[dma]['ENHANCED_'+value]=None
    if 'HISPANIC' in enhanced_household_size_list[dma]:
      for value in minority_and_complement_dict['HISPANIC']:
        collapsed_controls[dma][value]=None
    else:
      for value in minority_and_complement_dict['HISPANIC']:
        collapsed_controls[dma]['ENHANCED_'+value]=None
        
  else:
    for value in minority_and_complement_dict['BLACK']:
        collapsed_controls[dma]['ENHANCED_'+value]=None
    for value in minority_and_complement_dict['HISPANIC']:
        collapsed_controls[dma]['ENHANCED_'+value]=None


# COMMAND ----------

# MAGIC %md
# MAGIC # Part 6: Change the Output Format and Write the Output to S3

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Change the Output Format

# COMMAND ----------

#######################################################################################################################
############   Convert collapsed_controls dictionary to Pandas dataframe 
#######################################################################################################################

df=pd.DataFrame.from_dict(collapsed_controls,orient='index')
df=df.stack(dropna=True)
df=df.apply(pd.Series).stack(dropna=True).reset_index(level=2, drop=True).to_frame('level')
df=df.reset_index()
df.columns=[u'dma', u'weighting_control', u'level']
df=df.sort_values(by=['dma','weighting_control'])
df=df[df['weighting_control'].str.contains('_NUMBER')==False]

# Replace Control and Level names  
df['weighting_control']= df['weighting_control'].str.replace('CHILDREN','PERSONS')
df['weighting_control']= df['weighting_control'].str.replace('ENHANCED_','')
df['weighting_control']= df['weighting_control'].str.replace('PERSONS_MALE','PERSONS')
df['weighting_control']= df['weighting_control'].str.replace('PERSONS_FEMALE','PERSONS')
df['weighting_control']=np.where((df['weighting_control'].str.contains('PERSONS WITHIN HISPANIC/NON-HISPANIC')==True)&\
                                 (df['level'].str.contains('MINORITY_')==True),'PERSONS_HISPANIC',df['weighting_control'])
df['weighting_control']=np.where((df['weighting_control'].str.contains('PERSONS WITHIN HISPANIC/NON-HISPANIC')==True)&\
                                 (df['level'].str.contains('COMPLEMENT_')==True),'PERSONS_NON-HISPANIC',df['weighting_control'])
df['weighting_control']=np.where((df['weighting_control'].str.contains('PERSONS WITHIN BLACK/NON-BLACK')==True)&\
                                 (df['level'].str.contains('MINORITY_')==True),'PERSONS_BLACK_RACE',df['weighting_control'])
df['weighting_control']=np.where((df['weighting_control'].str.contains('PERSONS WITHIN BLACK/NON-BLACK')==True)&\
                                 (df['level'].str.contains('COMPLEMENT_')==True),'PERSONS_NON-BLACK_RACE',df['weighting_control'])
df['level']= df['level'].str.replace('MINORITY_','')
df['level']= df['level'].str.replace('COMPLEMENT_','')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Convert to PySpark Dataframe and Write Output to s3

# COMMAND ----------

collapsed_controls_df=sqlContext.createDataFrame(df)
collapsed_controls_df.createOrReplaceTempView('collapsed_controls_df')

sqlContext.setConf('spark.sql.parquet.compression.codec', 'snappy')
collapsed_controls_df.write.parquet(final_output_module3_dir, mode='overwrite')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Notebook Caller Output

# COMMAND ----------

dbutils.notebook.exit("Output of Sufficient Sample Size Qualifier Engine Module path: " + final_output_module3_dir)
