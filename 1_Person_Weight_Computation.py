# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC # Person Weight Computation
# MAGIC 
# MAGIC >This notebook has to be executed as the first module of Weighting Control Evaluation (WCE) software. The person initial weights created at the end of this module are needed in the **Effective Sample Size Computation** module of WCE tool.
# MAGIC 
# MAGIC **PURPOSE:**
# MAGIC >* To compute reportable person initial weights data, using tables from the media data lake, for a supplied date range, selected DMAs and sample type. 
# MAGIC 
# MAGIC **LOW-LEVEL EXPLANATION OF WHAT NOTEBOOK DOES:**
# MAGIC >This software can be broken into 5 sub-modules.
# MAGIC 
# MAGIC > **Part 1: Calculate Set 2 and Set 3 Intab Design Weights**
# MAGIC 
# MAGIC >>* Purpose of this part is to compute set 2 and set 3 intab design weights for each dma_code/intab_date/weight_type_code/ppm_metro_code. (weight type code might be set 2 or set 3)
# MAGIC >>* The computed weights are used for households. Pull up data from TGB2 view (dsci_tam.household_characteristics)
# MAGIC >>* BBO homes are removed. 
# MAGIC >>* Also, only keep TAM intab homes passing set edit (LOCAL_DAILY_STATUS_IND in (1,3)) and PPM homes with at least 1 person aged 6+ who is intab (LOCAL_DAILY_STATUS_IND in (4,5))
# MAGIC >>* Filteration used for the set_meter markets is 
# MAGIC 
# MAGIC >><pre>  
# MAGIC           (
# MAGIC          (npm_sample_ind='Y' and smm_sample_ind='N' and national_daily_status_ind in (3))
# MAGIC           or ((smm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (3,4,5)))
# MAGIC           )</pre>
# MAGIC >>* Filteration used for the lpm markets is 
# MAGIC 
# MAGIC >><pre>  ((lpm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (3,4,5)))</pre>
# MAGIC >>* Pull up UEs from the dsci_tam.universe_estimates table for all dma_code/ppm_metro_code combinations. Keep only UEs for the households without BBO and the measurement_period of interest. Also, keep only UEs with the highest load_id.
# MAGIC >>* compute sample size of intab TAM home per DMA_CODE/INTAB_DATE/PPM_METRO_CODE and call it set 3 intab counts
# MAGIC >>* compute sample size of (intab TAM homes+PPM home with at least one intab person aged 6+) per DMA_CODE/INTAB_DATE/PPM_METRO_CODE and call it set 2 intab counts
# MAGIC >>* compute sample size of intab TAM home per DMA_CODE/INTAB_DATE/PPM_METRO_CODE and call it set 3 intab counts
# MAGIC >>* compute sample size of (intab TAM homes+PPM home with at least one intab person aged 6+) per DMA_CODE/INTAB_DATE/PPM_METRO_CODE and call it set 2 intab counts
# MAGIC >>* set 3 intab design weight: divide UEs of each ppm metro code by sample size of intab TAM homes.
# MAGIC >>* set 2 intab design weight: divide UEs of each ppm metro code by sample size of (intab TAM homes+PPM home with at least one intab person aged 6+).
# MAGIC 
# MAGIC > **Part 2: Process B1010 Data**
# MAGIC 
# MAGIC >>* Purpose of this part is to implement the required data preparation before computing Sum Of Weights (SOW) for each demo bucket at the third part of the cookbook.
# MAGIC >>* Pull up data from dsci_tam.person_characteristics
# MAGIC >>* LTV/STV/BBO homes are removed.
# MAGIC >>* Only intab persons aged 2+ from b1010 are included.
# MAGIC >>* Filteration used for the set_meter markets is 
# MAGIC 
# MAGIC >><pre>  
# MAGIC           (
# MAGIC          (npm_sample_ind='Y' and smm_sample_ind='N' and national_daily_status_ind in (3))
# MAGIC           or ((smm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (3,4,5)))
# MAGIC           )</pre>
# MAGIC >>* Filteration used for the lpm markets is 
# MAGIC 
# MAGIC >><pre>  ((lpm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (3,4,5)))</pre>
# MAGIC >>* Demo buckets are comprised of the following:
# MAGIC <pre>
# MAGIC >>      A gender (M = Males, F = Females, C = Children)
# MAGIC >>      An age range (e.g. 25-34, 2-5, 65+)
# MAGIC >>      16 primary demo buckets in this cookbook are:
# MAGIC 
# MAGIC >>      (C2-5, C6-11,M12-17,M18-24,M25-34,M35-49,M50-54,M55_64,M65+,F12-17,F18-24,F25-34,F35-49,F50-54,F55_64,F65+)
# MAGIC </pre>
# MAGIC >>* Which primary demo bucket the person falls into is decided with the added columns "demo". Any person meeting all criteria of a demo bucket, will be grouped in that bucket by setting the "demo" column equal to one of the 16 primary buckets for that person. For example, a male aged 20 will have the value of "M18_24" in the "demo" column.
# MAGIC 
# MAGIC >>* We also add 29 binary columns that are composed of 16 primary buckets and 13 derived buckets (C2_11,P12_17,P2_17,M18_34,M18_49,M50_64, M50+,M18+,F18_34,F18_49,F50_64,F50+,F18+). For example for a male aged 20, the columns M18_24, M18_34, M18+ will be equal to 'Y' and the rest of 26 binary columns will be 'N'. These 29 columns are not used in this notebook but will be used in the next module of WCE software (Effective Sample Size Computation).
# MAGIC >>* Since b1010 data lacks the variable ppm_metro_code, it is joined with TGB2 data by intab_date and housheold_id to borrow that column.
# MAGIC 
# MAGIC > **Part 3: Compute SOW for Each Demo**
# MAGIC 
# MAGIC >>* Purpose of this part is to compute the sum of weights for each demo bucket.
# MAGIC >>* Data from sub_module 2 is joined with the weights that are created in sub-module 1 by intab_date,dma_code and ppm_metro_code.
# MAGIC >>* Persons aged 2 to 5 use only set 3 intab design weights.
# MAGIC >>* Persons aged 6+ are use only set 2 intab design weights.
# MAGIC >>* Aggregate the data and compute SOW per intab_day/dma_code/demo
# MAGIC 
# MAGIC > **Part 4: Compute UEs for Demos**
# MAGIC 
# MAGIC >>* Purpose of this part is to compute UEs per dma_code/demo. demos used are 16 primary demo buckets.
# MAGIC >>* Pull up UEs from the dsci_tam.universe_estimates table for all dma_code/ppm_metro_code combinations. Keep only UEs for the households without BBO and the measurement_period of interest. Also, keep only UEs with the highest load_id.
# MAGIC 
# MAGIC > **Part 5: Compute DAFs and Person Initial Weights**
# MAGIC >>* Purpose of this part is to compute DAFs and person initial weights for each person.
# MAGIC >>* DAFs are calculated for each intab_date/dma_code/demo combinations.
# MAGIC >>* DAFs are calculated by joining the SOW (sub-module 3) and UEs (sub-module 4) by demo buckets and dma_code.
# MAGIC >>* A DAF is computed using the formula below (rounded to 4 decimals):
# MAGIC 
# MAGIC >> $$ DAF_D = \frac{\sum Universe\,Estimate_D}{\sum \left({Household\,Weight\,\bullet\,N_D}\right)} \quad,\, \text{where D is the demo and N is the number of persons in a household, meeting the demo criteria} $$
# MAGIC 
# MAGIC >>* After computing the DAFs for demo buckets, they are multiplied by household design weights (computed at sum-module 1) to compute person initial weights.
# MAGIC 
# MAGIC **USER INPUTS:** 
# MAGIC > 1. **start_date**
# MAGIC >  - The beginning of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC > 2. **end_date**
# MAGIC >  - The ending of the desired measurement period, provided as a string, in the following format: **yyyy-mm-dd**
# MAGIC > 3. **sample_type** 
# MAGIC >  - Market type of the analysis, provided as a string, the acceptable market types are 'SET_METER+PPM' or 'LPM+PPM'. The software cannot be executed for LPM plus PPM and set meter plus ppm markets simultaneously.
# MAGIC > 4. **DMA codes**
# MAGIC >  - this should be a Python string of dmas, e.g. '501,602,703', '501'.
# MAGIC >  - lmp plus ppm market codes are '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC >  - set meter plus ppm market codes are '515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839'
# MAGIC > 5. **ue_table_measurement_period**
# MAGIC >  - The period for which UEs are pulled up from the MDL, provided as a string, in the following format **monthyy**
# MAGIC >  - The software only pulls up ues with the maximum load id.
# MAGIC >  - examples are 'Feb18' or 'May18'
# MAGIC > 6. **please_export**
# MAGIC >  - Provided as a string, set this varialbe to 'True' if you want to save the data in the Optional Overrides sections and 'False' otherwise. 
# MAGIC > 7. **override_dir**
# MAGIC >  - the directory in which the software saves the output with a parquet fomat
# MAGIC  
# MAGIC **THE OUTPUT FORMAT:**
# MAGIC > | col_name                   | data_type | ex_results                            |
# MAGIC   |----------------------------|-----------|---------------------------------------|
# MAGIC   | INTAB_DATE                 | date      | 2018-03-01                            |
# MAGIC   | GLOBAL_HOUSEHOLD_ID	       | bigint    | 535262                                |
# MAGIC   | HOUSEHOLD_ID	           | bigint    | 535262                                |
# MAGIC   | PERSON_NUMBER              | bigint    | 1                                     |
# MAGIC   | DMA_CODE                   | bigint    | 501                                   |
# MAGIC   | AGE                        | int       | 70                                    |
# MAGIC   | ORIGIN                     | string    | HISPANIC/NON-HISPANIC                 |
# MAGIC   | BLACK                      | string    | BLACK/NON-BLACK                       |
# MAGIC   | C2_5                       | char      | Y/N                                   |
# MAGIC   | C2_5                       | char      | Y/N                                   |
# MAGIC   | ...                        | char      | Y/N                                   |
# MAGIC   | F65_999                    | char      | Y/N                                   |
# MAGIC   | demo                       | string    | F18_24                                |
# MAGIC   | PPM_METRO_CODE             | string    | PPM Metro 1/PPM Metro 1/PPM Non Metro |
# MAGIC   | weight_type_code           | bigint    | 2/3                                   |
# MAGIC   | household_weight           | bigint    | 3394.004                              |
# MAGIC   | daf                        | bigint    | 1.064                                 |
# MAGIC   | person_weight              | bigint    | 3611.22                               |
# MAGIC   
# MAGIC  
# MAGIC 
# MAGIC **DATA SOURCES:**
# MAGIC > | schema               | table                                                        |
# MAGIC   |----------------------|--------------------------------------------------------------|
# MAGIC   | dsci_tam             | universe_estimates                                           |
# MAGIC   | dsci_tam             | household_characteristics                                    |
# MAGIC   | dsci_tam             | person_characteristics                                       |             
# MAGIC 
# MAGIC   
# MAGIC **AN EXAMPLE RUN:**
# MAGIC > <pre>
# MAGIC start_date                    = '2018-03-01'
# MAGIC end_date                      = '2018-03-02'
# MAGIC sample_type                   = 'LPM+PPM'
# MAGIC dma_codes                     = '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# MAGIC ue_table_measurement_period   = 'May18' 
# MAGIC please_export                 = 'True'
# MAGIC override_dir                  = '''s3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/person_initial_weights.20180301_20180301.parquet'''
# MAGIC </pre>
# MAGIC 
# MAGIC **AN EXAMPLE OUTPUT:**
# MAGIC > <pre>
# MAGIC <table class="confluenceTable">
# MAGIC <tbody>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">INTAB_DATE</td>
# MAGIC <td class="confluenceTd mceSelected">GLOBAL_HOUSEHOLD_ID</td>
# MAGIC <td class="confluenceTd mceSelected">HOUSEHOLD_ID</td>
# MAGIC <td class="confluenceTd mceSelected">PERSON_NUMBER</td>
# MAGIC <td class="confluenceTd mceSelected">DMA_CODE</td>
# MAGIC <td class="confluenceTd mceSelected">age</td>
# MAGIC <td class="confluenceTd mceSelected">ORIGIN</td>
# MAGIC <td class="confluenceTd mceSelected">BLACK</td>
# MAGIC <td class="confluenceTd mceSelected">C2_5</td>
# MAGIC <td class="confluenceTd mceSelected">C6_11</td>
# MAGIC <td class="confluenceTd mceSelected">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected">F65_999</td>
# MAGIC <td class="confluenceTd mceSelected">demo</td>
# MAGIC <td class="confluenceTd mceSelected">PPM_METRO_CODE</td>
# MAGIC <td class="confluenceTd mceSelected">weight_type_code</td>
# MAGIC <td class="confluenceTd mceSelected">household_weight</td>
# MAGIC <td class="confluenceTd mceSelected">daf</td>
# MAGIC <td class="confluenceTd mceSelected">person_weight</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">3/2/2018</td>
# MAGIC <td class="confluenceTd mceSelected">6.87E+08</td>
# MAGIC <td class="confluenceTd mceSelected">6058866</td>
# MAGIC <td class="confluenceTd mceSelected">4</td>
# MAGIC <td class="confluenceTd mceSelected">504</td>
# MAGIC <td class="confluenceTd mceSelected">5</td>
# MAGIC <td class="confluenceTd mceSelected">HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">NON-BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">Y</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">C2_5</td>
# MAGIC <td class="confluenceTd mceSelected">PPM Metro 1</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">3394.004</td>
# MAGIC <td class="confluenceTd mceSelected">1.064</td>
# MAGIC <td class="confluenceTd mceSelected">3611.22</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">3/1/2018</td>
# MAGIC <td class="confluenceTd mceSelected">21201047</td>
# MAGIC <td class="confluenceTd mceSelected">6074539</td>
# MAGIC <td class="confluenceTd mceSelected">5</td>
# MAGIC <td class="confluenceTd mceSelected">511</td>
# MAGIC <td class="confluenceTd mceSelected">2</td>
# MAGIC <td class="confluenceTd mceSelected">HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">NON-BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">Y</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">C2_5</td>
# MAGIC <td class="confluenceTd mceSelected">PPM Metro 1</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">3302.679</td>
# MAGIC <td class="confluenceTd mceSelected">1.082</td>
# MAGIC <td class="confluenceTd mceSelected">3573.499</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">3/2/2018</td>
# MAGIC <td class="confluenceTd mceSelected">20612065</td>
# MAGIC <td class="confluenceTd mceSelected">6107056</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">505</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">NON-HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">NON-BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">Y</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">C2_5</td>
# MAGIC <td class="confluenceTd mceSelected">PPM Metro 1</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">2358.104</td>
# MAGIC <td class="confluenceTd mceSelected">0.821</td>
# MAGIC <td class="confluenceTd mceSelected">1936.004</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">3/1/2018</td>
# MAGIC <td class="confluenceTd mceSelected">6.87E+08</td>
# MAGIC <td class="confluenceTd mceSelected">6138573</td>
# MAGIC <td class="confluenceTd mceSelected">6</td>
# MAGIC <td class="confluenceTd mceSelected">623</td>
# MAGIC <td class="confluenceTd mceSelected">5</td>
# MAGIC <td class="confluenceTd mceSelected">HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">NON-BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">Y</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">C2_5</td>
# MAGIC <td class="confluenceTd mceSelected">PPM Metro 1</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">3394.711</td>
# MAGIC <td class="confluenceTd mceSelected">1.106</td>
# MAGIC <td class="confluenceTd mceSelected">3754.55</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">3/2/2018</td>
# MAGIC <td class="confluenceTd mceSelected">6.87E+08</td>
# MAGIC <td class="confluenceTd mceSelected">6139191</td>
# MAGIC <td class="confluenceTd mceSelected">4</td>
# MAGIC <td class="confluenceTd mceSelected">623</td>
# MAGIC <td class="confluenceTd mceSelected">5</td>
# MAGIC <td class="confluenceTd mceSelected">HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">NON-BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">Y</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">C2_5</td>
# MAGIC <td class="confluenceTd mceSelected">PPM Metro 1</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">3380.407</td>
# MAGIC <td class="confluenceTd mceSelected">1.101</td>
# MAGIC <td class="confluenceTd mceSelected">3721.828</td>
# MAGIC </tr>
# MAGIC <tr>
# MAGIC <td class="confluenceTd mceSelected">3/2/2018</td>
# MAGIC <td class="confluenceTd mceSelected">1.37E+09</td>
# MAGIC <td class="confluenceTd mceSelected">6163773</td>
# MAGIC <td class="confluenceTd mceSelected">4</td>
# MAGIC <td class="confluenceTd mceSelected">524</td>
# MAGIC <td class="confluenceTd mceSelected">2</td>
# MAGIC <td class="confluenceTd mceSelected">NON-HISPANIC</td>
# MAGIC <td class="confluenceTd mceSelected">NON-BLACK_RACE</td>
# MAGIC <td class="confluenceTd mceSelected">Y</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">&nbsp;</td>
# MAGIC <td class="confluenceTd mceSelected">N</td>
# MAGIC <td class="confluenceTd mceSelected">C2_5</td>
# MAGIC <td class="confluenceTd mceSelected">PPM Metro 1</td>
# MAGIC <td class="confluenceTd mceSelected">3</td>
# MAGIC <td class="confluenceTd mceSelected">3062.481</td>
# MAGIC <td class="confluenceTd mceSelected">0.939</td>
# MAGIC <td class="confluenceTd mceSelected">2875.67</td>
# MAGIC </tr>
# MAGIC </tbody>
# MAGIC </table>
# MAGIC </pre>
# MAGIC 
# MAGIC **CODE DEVELOPERS:**
# MAGIC >* MJ PAKDEL
# MAGIC >* JONATHAN OUEGNIN

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC ### Inputs

# COMMAND ----------

######################################
#####    USER INPUTS (manual)    #####
######################################

# start_date                    = '2018-03-01'
# end_date                      = '2018-03-01'
# dma_codes                     = '501,602,820,511,539,623,517,528,819,618,512,803,753,807,508,504,506,524,613,505,609,510,862,534,751'
# # dma_codes    =  '515, 518, 521, 527, 533, 535, 544, 548, 560, 561, 616, 617, 635, 640, 641, 659, 770, 825, 839' 
# # sample_type = 'SET_METER+PPM'
# sample_type                   = 'LPM+PPM'
# ue_table_measurement_period   = 'May18' # Set measurement period and end date
# please_export                 = True
# override_dir                  = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/person_initial_weights.20180301_20180302.parquet'

# COMMAND ----------

##########################################################
#####    USER INPUTS (called in another notebook)    #####
##########################################################

start_date = str(dbutils.widgets.get('start_date'))
end_date = str(dbutils.widgets.get('end_date'))
dma_codes = str(dbutils.widgets.get('dma_codes'))
sample_type = str(dbutils.widgets.get('sample_type'))
ue_table_measurement_period = str(dbutils.widgets.get('ue_table_measurement_period'))
please_export = True if 'T' == str(dbutils.widgets.get('please_export'))[0].upper() else False
override_dir = 's3://useast1-nlsn-w-digital-dsci-dev/projects/Weighting_Controls_Evaluation/person_initial_weights_' + start_date.replace('-','_') + '_' + end_date.replace('-','_') + '_' + sample_type.replace('+','_') +'.parquet' 

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ### Validate Sample Type User Input

# COMMAND ----------

sample_type = sample_type.upper()

if (sample_type != "LPM+PPM") and (sample_type != "SET_METER+PPM"):
    dbutils.notebook.exit("Invalid Sample Type Input")

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create the filters based on the sample type

# COMMAND ----------

###################################################################################################################
#####    Depending on the sample_type, here some filters are created to pull data from TGB2 and B1010    #########
###################################################################################################################

if sample_type == "SET_METER+PPM":
    sample_type1 =  '''(
      (npm_sample_ind='Y' and smm_sample_ind='N' and national_daily_status_ind in (1,3))
      or ((smm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4,5)))
      )'''
    sample_type2 = "(npm_sample_ind='Y')"
elif sample_type == "LPM+PPM":
    sample_type1 = "((lpm_sample_ind='Y' or ppm_sample_ind='Y') AND (LOCAL_DAILY_STATUS_IND in (1,3,4,5)))"
    sample_type2 = "(lpm_sample_ind='Y')"
    
cached_tables = []

# COMMAND ----------

# MAGIC %md 
# MAGIC # Part 1: Calculate Set 2 and Set 3 Intab Design Weights

# COMMAND ----------

# MAGIC %md ### Create Load ID Variable

# COMMAND ----------

max_load_id_query = '''
SELECT
    MAX(load_id) AS max_load_id
FROM
    dsci_tam.universe_estimates
WHERE 
    measurement_period='{0}'
'''.format(ue_table_measurement_period)

max_load_id = spark.sql(max_load_id_query).collect()[0][0]

# COMMAND ----------

# MAGIC %md ### Pull up UE table and Compute UEs by ppm metro codes

# COMMAND ----------

###################################################################################################################
#####    Pull up UEs from the dsci_tam.universe_estimates table for all dma_code/ppm_metro_code combinations ###### 
#####    Keep only UEs for the households without BBO and the measurement_period of interest                 ######          
#####    Also, keep only UEs with the highest load_id.                                                       ######
###################################################################################################################

s=''' 
SELECT 
    geography_code as dma_code,market_break,universe_estimate
FROM
    dsci_tam.universe_estimates 
WHERE 
    measurement_period='{ue_period}'
    AND ue_type='H'
    AND load_id={max_load_id}
    AND ((market_break="PPM Metro 1" AND market_break_description="TV Household") OR 
        (market_break="PPM Metro 2" AND market_break_description="TV Household") OR 
        (market_break="PPM Non-Metro" AND market_break_description="TV Household"))
ORDER BY
    dma_code,market_break
    '''.format(ue_period=ue_table_measurement_period, max_load_id=max_load_id)
ue_table=spark.sql(s)

# Filter load_id to the max
# ue_table=ue_table.filter(ue_table.load_id == max([x.load_id for x in ue_table.select('load_id').distinct().collect()])).drop('load_id')
ue_table.createOrReplaceTempView("ue_table")
ue_table.cache()
cached_tables.append('ue_table')

# COMMAND ----------

# MAGIC %md ### Pull up TGB2 View Table

# COMMAND ----------

###################################################################################################################
#####    Pull up data from TGB2 view (dsci_tam.household_characteristics)                                    ######
#####    BBO homes are removed                                                                               ###### 
###################################################################################################################

s = '''
SELECT 
    DMA_CODE,INTAB_DATE,HOUSEHOLD_ID,lpm_sample_ind,ppm_sample_ind,smm_sample_ind,npm_sample_ind,
    CASE WHEN PPM_METRO_CODE=1 THEN 'PPM Metro 1'
        WHEN PPM_METRO_CODE=2 THEN 'PPM Metro 2'
        ELSE 'PPM Non-Metro' END AS PPM_METRO_CODE
FROM 
    dsci_tam.household_characteristics 
WHERE 1=1
    AND intab_date between '{start_date}' and '{end_date}'
    AND dma_code in ({dma_codes})
    AND {sample_type}
    AND HH_BROADBAND_ONLY_IND!='Y'
    '''.format(dma_codes=dma_codes,start_date=start_date,end_date=end_date,sample_type=sample_type1)

tgb2_file=spark.sql(s)
tgb2_file.createOrReplaceTempView('tgb2_file')
tgb2_file.cache()
cached_tables.append('tgb2_file')

# COMMAND ----------

# MAGIC %md ### Compute intab counts for TGB2 by ppm metro code and weight_type_code

# COMMAND ----------

##########################################################################################################################
#####    compute sample size of intab TAM home per DMA_CODE/INTAB_DATE/PPM_METRO_CODE and call it set 3 intab counts  ####
#####    compute sample size of (intab TAM homes+PPM home with at least one intab person aged 6+) per                 ####
#####         DMA_CODE/INTAB_DATE/PPM_METRO_CODE and call it set 2 intab counts                                       ####
#####    union all two dataset created above                                                                          ####
###########################################################################################################################

s='''
(SELECT
    DMA_CODE,INTAB_DATE,'2' as weight_type_code,PPM_METRO_CODE,
    count(*) as intab_count
FROM
    tgb2_file
GROUP BY
    DMA_CODE,INTAB_DATE,PPM_METRO_CODE
ORDER BY
    DMA_CODE,INTAB_DATE,weight_type_code,PPM_METRO_CODE)
   
   
UNION ALL

(SELECT
    DMA_CODE,INTAB_DATE,'3' as weight_type_code,PPM_METRO_CODE,
    count(*) as intab_count
FROM
    tgb2_file
WHERE 1=1
    AND {sample_type}
   
GROUP BY
    DMA_CODE,PPM_METRO_CODE,INTAB_DATE
ORDER BY
    DMA_CODE,INTAB_DATE,weight_type_code,PPM_METRO_CODE)
'''.format(sample_type=sample_type2)

intab_counts=spark.sql(s)
intab_counts.createOrReplaceTempView("intab_counts")
intab_counts.cache()
cached_tables.append('intab_counts')

# COMMAND ----------

##########################################################################################################################
#####    set 3 intab design weight: divide UEs of each ppm metro code by sample size of intab TAM homes.               ###
#####    set 2 intab design weight: divide UEs of each ppm metro code by sample size of (intab TAM homes+PPM home with ###
#####    at least one intab person aged 6+).                                                                           ###  
###########################################################################################################################

s='''
SELECT --count(*)
    A.*,B.universe_estimate,round(B.universe_estimate/A.intab_count,4) as household_weight
FROM
    intab_counts A
JOIN
    ue_table B
WHERE
    A.DMA_CODE=B.dma_code
    AND A.ppm_metro_code=B.market_break
ORDER BY
    A.INTAB_DATE,A.DMA_CODE,A.weight_type_code,A.PPM_METRO_CODE
'''

calculated_design_weight=spark.sql(s)
calculated_design_weight.createOrReplaceTempView('calculated_design_weight')
calculated_design_weight.cache().count()
cached_tables.append('calculated_design_weight')

# COMMAND ----------

# MAGIC %md
# MAGIC # Part 2: Process B1010 Data

# COMMAND ----------

# MAGIC %md
# MAGIC ### Pull up B1010 view table

# COMMAND ----------

##########################################################################################################################
############      Pull up data from dsci_tam.person_characteristics
############      LTV/STV/BBO homes are removed.
############      Only intab persons aged 2+ from b1010 are included.  
###########################################################################################################################

s='''
SELECT 
    a.INTAB_DATE,a.GLOBAL_HOUSEHOLD_ID,a.HOUSEHOLD_ID,a.PERSON_NUMBER,a.DMA_CODE,a.gender,a.age ,a.HOH_ORIGIN_CODE,a.HH_BLACK_IND
FROM
    dsci_tam.person_characteristics a
WHERE 1=1
    AND intab_date between '{start_date}' and '{end_date}'
    AND dma_code in ({dma_codes})
    AND {sample_type}
    AND (a.RELATION_TO_HOH != 6 OR a.RELATION_TO_HOH != 7 OR a.RELATION_TO_HOH != 'A' OR a.RELATION_TO_HOH != 'B' OR a.RELATION_TO_HOH != 10) 
    AND short_term_visitor_flag='N'
    AND long_term_visitor_flag='N'
    AND PERSON_INTAB='Y'
    AND HH_BROADBAND_ONLY_IND != 'Y'
    AND age>=2

'''.format(dma_codes=dma_codes,start_date=start_date,end_date=end_date,sample_type=sample_type1)

b1010_file_pre=spark.sql(s)
b1010_file_pre.createOrReplaceTempView('b1010_file_pre')
b1010_file_pre.cache()
cached_tables.append('b1010_file_pre')

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Create new demographics columns for b1010

# COMMAND ----------

##########################################################################################################################
############      A column called demo is added to the dataset. Any person meeting all criteria of a demo bucket, 
############      will be grouped in that bucket by setting the "demo" column equal to one of the 16 primary buckets 
############      for that person.
############      We also add 29 binary columns that are composed of 16 primary buckets and 13 derived buckets  
###########################################################################################################################

s='''
SELECT 
    a.INTAB_DATE,a.GLOBAL_HOUSEHOLD_ID,a.HOUSEHOLD_ID,a.PERSON_NUMBER,a.DMA_CODE,a.age,
    CASE 
        WHEN a.HOH_ORIGIN_CODE = '1' THEN 'NON-HISPANIC'
        ELSE 'HISPANIC'
    END AS ORIGIN,
    CASE 
        WHEN HH_BLACK_IND = 'Y' THEN 'BLACK_RACE'
        ELSE 'NON-BLACK_RACE'
    END AS BLACK,
    CASE 
        WHEN CAST(a.AGE AS int) >= 2 AND CAST(a.AGE AS int) <= 5 THEN 'Y' ELSE 'N'
    END AS C2_5,
    CASE
        WHEN CAST(a.AGE AS int) >= 6 AND CAST(a.AGE AS int) <= 11 THEN 'Y' ELSE 'N' 
    END AS C6_11,
    CASE 
        WHEN CAST(a.AGE AS int) >= 2 AND CAST(a.AGE AS int) <= 11 THEN 'Y' ELSE 'N'
    END AS C2_11,
    CASE 
        WHEN CAST(a.AGE AS int) >= 12 AND CAST(a.AGE AS int) <= 17 THEN 'Y' ELSE 'N'
    END AS P12_17,
    CASE 
        WHEN CAST(a.AGE AS int) >= 2 AND CAST(a.AGE AS int) <= 17 THEN 'Y' ELSE 'N'
    END AS P2_17,
    CASE
        WHEN CAST(a.AGE AS int) >= 18 AND CAST(a.AGE AS int) <= 34 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M18_34,
    CASE
        WHEN CAST(a.AGE AS int) >= 18 AND CAST(a.AGE AS int) <= 34 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F18_34,
    CASE
        WHEN CAST(a.AGE AS int) >= 18 AND CAST(a.AGE AS int) <= 49 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M18_49,
    CASE
        WHEN CAST(a.AGE AS int) >= 18 AND CAST(a.AGE AS int) <= 49 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F18_49,
    CASE
        WHEN CAST(a.AGE AS int) >= 50 AND CAST(a.AGE AS int) <= 64 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M50_64,
    CASE
        WHEN CAST(a.AGE AS int) >= 50 AND CAST(a.AGE AS int) <= 64 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F50_64,
    CASE
        WHEN CAST(a.AGE AS int) >= 50 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M50_999,
    CASE
        WHEN CAST(a.AGE AS int) >= 50 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F50_999,
    CASE 
        WHEN CAST(a.AGE AS int) >= 18  AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M18_999,
    CASE 
        WHEN CAST(a.AGE AS int) >= 18  AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F18_999, 
    CASE
        WHEN CAST(a.AGE AS int) >= 12 AND CAST(a.AGE AS int) <= 17 AND a.GENDER = 'M' THEN 'Y' ELSE 'N'
    END AS M12_17,
    CASE
        WHEN CAST(a.AGE AS int) >= 18 AND CAST(a.AGE AS int) <= 24 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M18_24,
    CASE
        WHEN CAST(a.AGE AS int) >= 25 AND CAST(a.AGE AS int) <= 34 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M25_34,
    CASE
        WHEN CAST(a.AGE AS int) >= 35 AND CAST(a.AGE AS int) <= 49 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M35_49,
    CASE
        WHEN CAST(a.AGE AS int) >= 50 AND CAST(a.AGE AS int) <= 54 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M50_54,
    CASE
        WHEN CAST(a.AGE AS int) >= 55 AND CAST(a.AGE AS int) <= 64 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M55_64,
    CASE
        WHEN CAST(a.AGE AS int) >= 65 AND a.GENDER = 'M' THEN 'Y' ELSE 'N' 
    END AS M65_999,
    CASE
       WHEN CAST(a.AGE AS int) >= 12 AND CAST(a.AGE AS int) <= 17 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F12_17,
    CASE
        WHEN CAST(a.AGE AS int) >= 18 AND CAST(a.AGE AS int) <= 24 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F18_24,
    CASE
        WHEN CAST(a.AGE AS int) >= 25 AND CAST(a.AGE AS int) <= 34 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F25_34,
    CASE
        WHEN CAST(a.AGE AS int) >= 35 AND CAST(a.AGE AS int) <= 49 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F35_49,
    CASE
        WHEN CAST(a.AGE AS int) >= 50 AND CAST(a.AGE AS int) <= 54 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F50_54,
    CASE
        WHEN CAST(a.AGE AS int) >= 55 AND CAST(a.AGE AS int) <= 64 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F55_64,
    CASE
        WHEN CAST(a.AGE AS int) >= 65 AND a.GENDER = 'F' THEN 'Y' ELSE 'N' 
    END AS F65_999,
    CASE
        WHEN CAST(a.AGE AS int) >= 2 AND CAST(a.AGE AS int) <= 5 THEN 'C2_5'
        WHEN CAST(a.AGE AS int) >= 6 AND CAST(a.AGE AS int) <= 11 THEN 'C6_11'
        WHEN CAST(a.AGE AS int) >= 12 AND CAST(a.AGE AS int) <= 17 AND a.GENDER = 'M' THEN 'M12_17'
        WHEN CAST(a.AGE AS int) >= 18 AND CAST(a.AGE AS int) <= 24 AND a.GENDER = 'M' THEN 'M18_24'
        WHEN CAST(a.AGE AS int) >= 25 AND CAST(a.AGE AS int) <= 34 AND a.GENDER = 'M' THEN 'M25_34'
        WHEN CAST(a.AGE AS int) >= 35 AND CAST(a.AGE AS int) <= 49 AND a.GENDER = 'M' THEN 'M35_49'
        WHEN CAST(a.AGE AS int) >= 50 AND CAST(a.AGE AS int) <= 54 AND a.GENDER = 'M' THEN 'M50_54'
        WHEN CAST(a.AGE AS int) >= 55 AND CAST(a.AGE AS int) <= 64 AND a.GENDER = 'M' THEN 'M55_64'
        WHEN CAST(a.AGE AS int) >= 65 AND a.GENDER = 'M' THEN 'M65_999'
        WHEN CAST(a.AGE AS int) >= 12 AND CAST(a.AGE AS int) <= 17 AND a.GENDER = 'F' THEN 'F12_17'
        WHEN CAST(a.AGE AS int) >= 18 AND CAST(a.AGE AS int) <= 24 AND a.GENDER = 'F' THEN 'F18_24'
        WHEN CAST(a.AGE AS int) >= 25 AND CAST(a.AGE AS int) <= 34 AND a.GENDER = 'F' THEN 'F25_34'
        WHEN CAST(a.AGE AS int) >= 35 AND CAST(a.AGE AS int) <= 49 AND a.GENDER = 'F' THEN 'F35_49'
        WHEN CAST(a.AGE AS int) >= 50 AND CAST(a.AGE AS int) <= 54 AND a.GENDER = 'F' THEN 'F50_54'
        WHEN CAST(a.AGE AS int) >= 55 AND CAST(a.AGE AS int) <= 64 AND a.GENDER = 'F' THEN 'F55_64'
        WHEN CAST(a.AGE AS int) >= 65 AND a.GENDER = 'F' THEN 'F65_999'
    END AS demo
FROM 
    b1010_file_pre AS a
'''

b1010_file=spark.sql(s).repartition(1)
b1010_file.createOrReplaceTempView('b1010_file')
b1010_file.cache()
# b1010_file.count()
# display(b1010_file)
cached_tables.append('b1010_file')

# COMMAND ----------

# MAGIC %md
# MAGIC # Join B1010 with TGB2 in order to borrow ppm_metro_code

# COMMAND ----------

##########################################################################################################################
############      Since b1010 data lacks the variable ppm_metro_code, it is joined with TGB2 data by intab_date and 
############             housheold_id to borrow that column.
###########################################################################################################################

s='''
SELECT 
    A.*,
    CASE 
        WHEN PPM_METRO_CODE=1 THEN 'PPM Metro 1'
        WHEN PPM_METRO_CODE=2 THEN 'PPM Metro 2'
    ELSE 'PPM Non-Metro' END AS PPM_METRO_CODE
FROM 
    b1010_file A
JOIN
    dsci_tam.household_characteristics B
ON
    A.INTAB_DATE=B.intab_date
    AND A.household_id=B.household_id
'''
b1010_file_with_metro=spark.sql(s)
b1010_file_with_metro.createOrReplaceTempView("b1010_file_with_metro")
b1010_file_with_metro.cache().count()
cached_tables.append('b1010_file')

# COMMAND ----------

# MAGIC %md
# MAGIC #Part 3: Compute SOW for Each Demo

# COMMAND ----------

# MAGIC %md
# MAGIC ### Join B1010 with set 2 and  set 3 intab design weights

# COMMAND ----------

##########################################################################################################################
############      B1010 file is the joined with the computed weights by intab_date,dma_code and ppm_metro_code
############      Persons aged 2 to 5 use only set 3 intab design weights.
############      Persons aged 6+ are use only set 2 intab design weights.
###########################################################################################################################

s='''
SELECT 
    a.*,b.weight_type_code,b.household_weight
FROM
    b1010_file_with_metro a
JOIN
    calculated_design_weight b
ON 1=1
    AND a.intab_date=b.intab_date
    AND a.dma_code=b.dma_code
    AND a.ppm_metro_code=b.ppm_metro_code
WHERE 1=1
    AND b.weight_type_code==3
    AND a.age between 2 and 5
   
UNION ALL

SELECT 
    a.*,b.weight_type_code,b.household_weight
FROM
    b1010_file_with_metro a
JOIN
    calculated_design_weight b
ON 1=1
    AND a.intab_date=b.intab_date
    AND a.dma_code=b.dma_code
    AND a.ppm_metro_code=b.ppm_metro_code
WHERE 1=1
    AND b.weight_type_code==2
    AND a.age>=6
'''
b1010_file_with_weights=spark.sql(s)
b1010_file_with_weights.createOrReplaceTempView("b1010_file_with_weights")
b1010_file_with_weights.cache()
cached_tables.append('b1010_file_with_weights')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Compute SOW for Each Demo

# COMMAND ----------

##########################################################################################################################
############      Aggregate the data and compute SOW per intab_day/dma_code/demo
###########################################################################################################################

demo_subrims = {
  'C2_5': ['C2_5'],
  'C6_11': ['C6_11'],
  'M12_17': ['M12_17'],
  'M18_24': ['M18_24'],
  'M25_34': ['M25_34'],
  'M35_49': ['M35_49'],
  'M50_54': ['M50_54'],
  'M55_64': ['M55_64'],
  'M65_999': ['M65_999'],
  'F12_17': ['F12_17'],
  'F18_24': ['F18_24'],
  'F25_34': ['F25_34'],
  'F35_49': ['F35_49'],
  'F50_54': ['F50_54'],
  'F55_64': ['F55_64'],
  'F65_999': ['F65_999']
}

# Create string containing the SQL query used in the loop 
b1010_agg_query_part='''
SELECT
    DMA_CODE,INTAB_DATE,'{demo}' as demos,round(SUM(household_weight),4) as SOW 
FROM
    b1010_file_with_weights
WHERE
    demo in ('{list_demo}')
GROUP BY
    DMA_CODE,INTAB_DATE,demos
  
UNION ALL  
'''
# Create empty SQL query
b1010_agg_query=''

# Loop over the dictionary and consolidate query parts 
for demo_name,demo in demo_subrims.items():
  
  b1010_agg_query+=b1010_agg_query_part.format(demo=demo_name,list_demo="','".join(demo))      

b1010_agg_query = b1010_agg_query.rstrip('UNION ALL\n    ')
sow_table=spark.sql(b1010_agg_query).repartition(1)
sow_table.createOrReplaceTempView('sow_table')
sow_table.cache().count()
cached_tables.append('sow_table')

# COMMAND ----------

# MAGIC %md
# MAGIC #Part 4: Compute UEs for Demos

# COMMAND ----------

# MAGIC %md
# MAGIC ### Function to rename market_break_description to Demo

# COMMAND ----------

###################################################################################################################
#####    This function rename the market_break_description column of the UE table to match the demo column.  ######
#####    This function is used in the Cmd 19. The function is registered as a Pyspark UDF                    ###### 
###################################################################################################################

import re
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

def rename_demos(demo_name):
  
  demo_name_list = ['Children', 'Women', 'Men']

  if any(x in demo_name for x in demo_name_list):
    if demo_name != 'Working Women':
      replace_str = re.search(r'^\w{1}', demo_name).group()
      if replace_str == 'W':
        replace_str = 'F'
      demo_name = re.sub(r'^\w*\s', replace_str, demo_name).replace('-','_').replace('+','_999')
  
    return(demo_name)

sqlContext.udf.register("rename_demos_udf", rename_demos, StringType())

# Run next command if want to use function with Dataframes
# rename_demos_udf = udf(rename_demos,StringType())

# COMMAND ----------

# MAGIC %md
# MAGIC ### Pull up UE table from MDL Table

# COMMAND ----------

###################################################################################################################
#####    Pull up UEs from the dsci_tam.universe_estimates table for all dma_code/demo.                       ###### 
#####    Keep only UEs for the households without BBO and the measurement_period of interest.                ######          
#####    Keep only UEs with the highest load_id.                                                             ######
###################################################################################################################
s='''
SELECT 
    geography_code, market_break, rename_demos_udf(market_break_description) AS market_break_description, ue_type, universe_estimate, load_id
FROM 
    dsci_tam.universe_estimates
WHERE 1=1
    AND measurement_period='{ue_period}'
    AND ue_type='P'
    AND geography_code IN ({dma_codes})
    AND market_break = 'Total'
'''.format(ue_period=ue_table_measurement_period,dma_codes=dma_codes)

ue_table_per=spark.sql(s)

# Filter load_id to the max
ue_table_per=ue_table_per.filter(ue_table_per.load_id == max([x.load_id for x in ue_table_per.select('load_id').distinct().collect()])).drop('load_id')

ue_table_per.createOrReplaceTempView('ue_table_per')
ue_table_per.cache()
cached_tables.append('ue_table_per')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Create dict with demos and compute UEs for each demo

# COMMAND ----------

###################################################################################################################
#####    Create a dictionary with derived demos.                                                             ###### 
#####    Create a SQL statement to aggreagate the derived UEs.                                               ######          
#####    Computes derived UEs.                                                                               ######
###################################################################################################################

ue_to_agg={
        'C2_5': ['C2_5'],
        'C6_11': ['C6_11'],
        'M12_17': ['M12_14','M15_17'],
        'M18_24': ['M18_20','M21_24'],
        'M25_34': ['M25_34'],
        'M35_49': ['M35_44','M45_49'],
        'M50_54': ['M50_54'],
        'M55_64': ['M55_64'],
        'M65_999': ['M65_999'],
        'F12_17': ['F12_14','F15_17'],
        'F18_24': ['F18_20','F21_24'],
        'F25_34': ['F25_34'],
        'F35_49': ['F35_44','F45_49'],
        'F50_54': ['F50_54'],
        'F55_64': ['F55_64'],
        'F65_999': ['F65_999']
      }

ue_table_agg_query_part='''
SELECT 
    geography_code, 
    '{market_break_description}' AS market_break_description,
    sum(universe_estimate) AS sum_of_ues
FROM
    {table}
WHERE 
    1=1 
    AND market_break_description IN ('{all_levels}')
GROUP BY 
    geography_code
UNION ALL

'''

ue_table_agg_query=''

for new_market_break_description, level in ue_to_agg.items():

   ue_table_agg_query+=ue_table_agg_query_part.format(market_break_description=new_market_break_description,all_levels="', '".join(level),table='ue_table_per')

# Compute derived demos
ue_table_agg_query = ue_table_agg_query.rstrip('UNION ALL\n    ')
sum_of_ue_demo = spark.sql(ue_table_agg_query)
sum_of_ue_demo.createOrReplaceTempView('sum_of_ue_demo')
sum_of_ue_demo.cache().count()
cached_tables.append('sum_of_ue_demo')

# COMMAND ----------

# MAGIC %md
# MAGIC #Part 5: Compute DAFs and Person Initial Weights

# COMMAND ----------

# MAGIC %md
# MAGIC ### Join Sum of UEs and Sum of Weights and Compute DAF

# COMMAND ----------

###################################################################################################################
#####    DAFs are calculated for each intab_date/dma_code/demo combinations.
#####    DAFs are calculated by joining the SOW (sub-module 3) and UEs (sub-module 4) by demo buckets and dma_code
###################################################################################################################
s = """
SELECT 
    a.*,b.sum_of_ues,ROUND((b.sum_of_ues/SOW),4) AS DAF 
FROM 
    sow_table a 
JOIN 
    sum_of_ue_demo b 
ON 
    a.demos = b.market_break_description 
    AND a.DMA_CODE=b.geography_code
"""

daf_demos = spark.sql(s)
daf_demos.createOrReplaceTempView('daf_demos')
daf_demos.cache()
cached_tables.append('daf_demos')

# COMMAND ----------

# MAGIC %md
# MAGIC ### Join DAF to b1010 and compute person initial weights

# COMMAND ----------

###################################################################################################################
#####     After computing the DAFs for demo buckets, they are multiplied by household design weights 
#####     (computed at sum-module 1) to compute person initial weights.
###################################################################################################################
s="""
SELECT 
    a.*,b.DAF AS daf ,ROUND((household_weight * daf),4) AS person_weight
FROM
    b1010_file_with_weights a
JOIN 
    daf_demos b
ON 
    a.dma_code=b.dma_code
    AND a.demo=b.demos 
    AND a.INTAB_DATE = b.INTAB_DATE
"""

final_b1010 = spark.sql(s)
final_b1010.createOrReplaceTempView('final_b1010')
final_b1010.cache()
final_b1010.count()
cached_tables.append('final_b1010')

# COMMAND ----------

# MAGIC %md 
# MAGIC #Part 6: Write Output to S3

# COMMAND ----------

if please_export == True:
    sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy")
    final_b1010.write.parquet(override_dir, mode='overwrite')

# COMMAND ----------

# MAGIC %md 
# MAGIC ### Clean Work Space

# COMMAND ----------

for table in cached_tables:
    uncache_tbl_sql = "UNCACHE TABLE {}".format(table)
    spark.sql(uncache_tbl_sql)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Notebook Caller Output

# COMMAND ----------

dbutils.notebook.exit("Output of Person Weight Computation Module path: " + override_dir)
